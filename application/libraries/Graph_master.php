<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class Graph_master {

    function get_graph($mod, $params) {
    	
    	$data = $this->setting_module($params);

		return $data;
		
    }

    function setting_module($params) {
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		$db_mbr = $CI->load->database('db_mbr', TRUE);

		/*modul setting*/
		if($params['mod']==1){
			if($params['prefix']==1){
				$query = "SELECT MONTH(created_date) AS bulan, COUNT(id) AS total FROM log WHERE YEAR(created_date)=".date('Y')." GROUP BY MONTH(created_date)";	
				$fields = array('User_Activity'=>'total');
				$title = '<span style="font-size:13.5px">Log History Penggunaan Aplikasi Oleh User Tahun '.date('Y').'</span>';
				$subtitle = 'Source: RSSM - SIRS';
				/*excecute query*/
				$data = $db->query($query)->result_array();
			}

			if($params['prefix']==2){
				$query = "SELECT MONTH(log.created_date) AS bulan, tmp_mst_menu.name, COUNT(id) as total FROM log LEFT JOIN tmp_mst_menu ON tmp_mst_menu.menu_id=log.menu_id WHERE YEAR(log.created_date)=".date('Y')." AND log.modul_id !=0 AND log.menu_id!=0 GROUP BY log.menu_id, MONTH(log.created_date), tmp_mst_menu.name ORDER BY COUNT(id) DESC LIMIT 10";	
				$fields = array('name' => 'total');
				$title = '<span style="font-size:13.5px">5 Modul yang sering diakses oleh pengguna</span>';
				$subtitle = 'Source: RSSM - SIRS';
				/*excecute query*/
				$data = $db->query($query)->result_array();
			}

			if($params['prefix']==3){
				$query = "SELECT content,COUNT(id) AS total FROM log GROUP BY content ORDER BY COUNT(id) DESC LIMIT 10";	
				$fields = array('Activity' => 'content', 'Total' => 'total');
				$title = '<span style="font-size:13.5px">10 Fungsi yang sering diakses oleh pengguna</span>';
				$subtitle = 'Source: RSSM - SIRS';
				/*excecute query*/
				$data = $db->query($query)->result_array();
			}
		}

		/*modul supplier*/
		if($params['mod']==40){
			/*based query*/
			if($params['prefix']==351){
				$query = "SELECT MONTH(v_requestticket.trans_time) AS bulan, (SUM(total) - SUM(total_priceprincipal)) AS total FROM v_requestticket WHERE YEAR(v_requestticket.trans_time)=".date('Y')." GROUP BY MONTH(v_requestticket.trans_time)";	
				$fields = array('Total_Transaction'=>'total');
				$title = '<span style="font-size:13.5px"> Total Transaction per Month '.date('Y').' </span>';
				$subtitle = 'Source: Mobile Reseller - Liburania Agent';
				/*excecute query*/
				$data = $db_mbr->query($query)->result_array();
			}

			if($params['prefix']==352){
				$query = "SELECT MONTH(v_requestticket.trans_time) AS bulan, m_principals.principal_name as principal, (SUM(total) - SUM(total_priceprincipal)) AS total 
					FROM v_requestticket 
					LEFT JOIN m_principals ON m_principals.id=v_requestticket.principal_id 
					WHERE YEAR(v_requestticket.trans_time)=".date('Y')." ORDER BY trans_time DESC";	
				$fields = array('principal' => 'total');
				$title = '<span style="font-size:13.5px">Theme Park Transaction Total '.date('Y').'</span>';
				$subtitle = 'Source: Mobile Reseller - Liburania Agent';
				/*excecute query*/
				$data = $db_mbr->query($query)->result_array();
			}

			if($params['prefix']==353){
				$query = "SELECT MONTH(v_requestticket.trans_time) AS bulan, m_principals.principal_name as principal, COUNT(v_requestticket.id) AS total 
					FROM v_requestticket 
					LEFT JOIN m_principals ON m_principals.id=v_requestticket.principal_id 
					WHERE YEAR(v_requestticket.trans_time)=".date('Y')."
					GROUP BY m_principals.id ORDER BY COUNT(v_requestticket.id) DESC LIMIT 10";	
				$fields = array('Principal' => 'principal', 'Total' => 'total');
				$title = '<span style="font-size:13.5px">Ticket Transaction Total '.date('Y').'</span>';
				$subtitle = 'Source: Mobile Reseller - Liburania Agent';
				/*excecute query*/
				$data = $db_mbr->query($query)->result_array();
			}

			if($params['prefix']==354){
				$query = "SELECT MONTH(v_requestticket.trans_time) AS bulan, v_requestticket.`user_name` AS account_name, COUNT(v_requestticket.id) AS total 
					FROM v_requestticket 
					LEFT JOIN m_principals ON m_principals.id=v_requestticket.principal_id 
					WHERE YEAR(v_requestticket.trans_time)=".date('Y')."
					GROUP BY v_requestticket.`account_id` ORDER BY COUNT(v_requestticket.id) DESC";	
				$fields = array('account_name' => 'total');
				$title = '<span style="font-size:13.5px">Transaction Total '.date('Y').' By Reseller Account </span>';
				$subtitle = 'Source: Mobile Reseller - Liburania Agent';
				/*excecute query*/
				$data = $db_mbr->query($query)->result_array();
			}

			if($params['prefix']==355){
				$query = "SELECT MONTH(v_requestticket.trans_time) AS bulan, v_requestticket.`user_name` AS account_name, COUNT(v_requestticket.id) AS total 
					FROM v_requestticket 
					LEFT JOIN m_principals ON m_principals.id=v_requestticket.principal_id 
					WHERE YEAR(v_requestticket.trans_time)=".date('Y')."
					GROUP BY v_requestticket.`account_id` ORDER BY COUNT(v_requestticket.id) DESC LIMIT 10";	
				$fields = array('Account_Name' => 'account_name', 'Total' => 'total');
				$title = '<span style="font-size:13.5px">Transaction Total '.date('Y').' By Reseller Account </span>';
				$subtitle = 'Source: Mobile Reseller - Liburania Agent';
				/*excecute query*/
				$data = $db_mbr->query($query)->result_array();
			}

		}
			
		/*find and set type chart*/
		$chart = $this->chartTypeData($params['TypeChart'], $fields, $params, $data);
		$chart_data = array(
			'title' 	=> $title,
			'subtitle' 	=> $subtitle,
			'xAxis' 	=> isset($chart['xAxis'])?$chart['xAxis']:'',
			'series' 	=> isset($chart['series'])?$chart['series']:'',
			);

		return $chart_data;
		
    }


    public function chartTypeData($style, $fields, $params, $data){

    	switch ($style) {
    		case 'column':
    			/*lanjutkan buat function jika ada style yang lain*/
    			if ($params['style']==1) {
    				return $this->ColumnStyleOneData($fields, $params, $data);
    			}
    			break;
    		case 'pie':
    			if ($params['style']==1) {
    				return $this->PieStyleOneData($fields, $params, $data);
    			}
    			break;
    		case 'line':
    			if ($params['style']==1) {
    				return $this->LineStyleOneData($fields, $params, $data);
    			}
    			break;
    		case 'table':
    			if ($params['style']==1) {
    				return $this->TableStyleOneData($fields, $params, $data);
    			}
    			break;
    		
    		default:
    			# code...
    			break;
    	}
    }
    public function ColumnStyleOneData($fields, $params, $data){
    	$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
    	
        $getData = array();
		foreach($data as $key=>$row){
			foreach ($fields as $kf => $vf) {
				$getData[$kf][$row['bulan']-1] = (int)$row[$vf];
			}
		}
		
		for ($i=0; $i < 12; $i++) { 
			foreach ($fields as $kf2 => $vf2) {
				if(!isset($getData[$kf2][$i])){
					$getData[$kf2][$i] = 0;
				}
				ksort($getData[$kf2]);
			}
			$categories[] = $CI->tanggal->getBulan($i+1);
			
		}

		foreach ($getData as $k => $r) {
			$series[] = array('name' => $k, 'data' => $r );
		}
		
		$chart_data = array(
			'xAxis' 	=> array('categories' => $categories),
			'series' 	=> $series,
		);
		return $chart_data;
    }

    public function PieStyleOneData($fields, $params, $data){
    	$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
    	
        $getData = array();
		foreach($data as $key=>$row){
			foreach ($fields as $kf => $vf) {
				$getData[$row[$kf]][] = (int)$row[$vf];
			}
		}

		foreach ($getData as $k => $r) {
			$series[] = array($k, array_sum($r));
		}
		$chart_data = array(
			'series' 	=> $series,
		);
		return $chart_data;
    }

    public function LineStyleOneData($fields, $params, $data){
    	$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
    	
        $getData = array();
		foreach($data as $key=>$row){
			foreach ($fields as $kf => $vf) {
				$getData[$kf][$row['bulan']-1] = (int)$row[$vf];
			}
		}
		
		for ($i=0; $i < 12; $i++) { 
			foreach ($fields as $kf2 => $vf2) {
				if(!isset($getData[$kf2][$i])){
					$getData[$kf2][$i] = 0;
				}
				ksort($getData[$kf2]);
			}
			$categories[] = $CI->tanggal->getBulan($i+1);
			
		}

		foreach ($getData as $k => $r) {
			$series[] = array('name' => $k, 'data' => $r );
		}
		
		$chart_data = array(
			'xAxis' 	=> array('categories' => $categories),
			'series' 	=> $series,
		);
		return $chart_data;
    }

    public function TableStyleOneData($fields, $params, $data){
    	$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
    	
        $html = '';
        $html .='<table class="table table-bordered table-hover"><thead>
			        <tr><th width="20px" class="center">No</th>';
		        foreach ($fields as $kf => $vf) {
		        	$html .= '<th>'.ucfirst($kf).'</th>';
		        }
      	$html .='</thead>';
      	$html .='<tbody>';
      	$no=0;
      	foreach ($data as $key => $value) { $no++;
      		$html .='<tr>';
	      	$html .='<td align="center">'.$no.'</td>';
	      	foreach ($fields as $keyf => $valuef) {
	      		$html .='<td align="left">'.ucwords(strtolower($value[$valuef])).'</td>';
	      	}
	      	$html .='</tr>';
      	}
      	
      	$html .='</tbody>';
      	$html .='</table>';

        $chart_data = array(
			'xAxis' 	=> 0,
			'series' 	=> $html,
		);
		return $chart_data;
    }
	
}

?> 