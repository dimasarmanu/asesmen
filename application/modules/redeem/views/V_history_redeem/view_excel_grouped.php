<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>  
          <th width="30px" class="center"></th>
          <th>Kode Booking</th>
          <th>Kunjungan</th>
		  <th>Expired</th>
		  <th>Nama Pemesan</th>
          <th>Hari Kunjungan</th>
          <th>Group</th>
		  <th>Jumlah</th>
		  <th>Redeem</th>
		  <th>Userid</th>
		  <th>Time</th>
        </tr>
    </thead>

    <tbody>
        <?php 
            $count = count($value); 
            if( $count > 0 ) :
            $no=0; foreach($value as $row_dt) : $no++; 
        ?>
            <tr>
                <td align="center"><?php echo $no?></td>
                <td><?php echo $row_dt->booking_code?> </td>
                <td><?php echo $this->tanggal->formatDateFormDmy($row_dt->visit_date)?></td>
                <td><?php echo $this->tanggal->formatDateFormDmy($row_dt->expired_date)?></td>
                <td><?php echo $row_dt->name_to?> </td>
                <td><?php echo $row_dt->datepricetype_id?> </td>
                <td><?php echo $row_dt->is_group?> </td>
				<td><?php echo $row_dt->qty_request?> </td>
				<td><?php echo $row_dt->qty?> </td>
				<td><?php echo $row_dt->user_id?> </td>
                <td><?php echo $row_dt->use_time?> </td>
            </tr>
        <?php 
            endforeach; 
            else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
            endif;
        ?>
    </tbody>
</table>