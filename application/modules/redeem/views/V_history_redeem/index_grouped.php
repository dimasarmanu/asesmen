<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>

jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
  
});

$(document).ready(function(){

  oTable = $('#dynamic-table').DataTable({ 
          
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "ordering": false,
    "searching": true,
    "bPaginate": true,
    "bInfo": true,
    "pageLength": 50,
    "ajax": {
        "url": $('#dynamic-table').attr('base-url'),
        "type": "POST"
    },

  });

  $('#btn_search_data').click(function (e) {    
      e.preventDefault();
      $.ajax({
          url: 'redeem/C_history_redeem_grouped/find_data',
          type: "post",
          data: $('#form_history_redeem').serialize(),
          dataType: "json",
          beforeSend: function() {
            achtungShowLoader();  
          },
          success: function(data) {
            achtungHideLoader();
            find_data_reload(data);
          }
      });
  });
  $('#btn_export_excel').click(function (e) {
		e.preventDefault();
		$.ajax({
			url: 'redeem/C_history_redeem_grouped/find_data',
			type: "post",
			data: $('#form_search').serialize(),
			dataType: "json",
			beforeSend: function() {
				achtungShowLoader();  
			},
			success: function(data) {
				achtungHideLoader();
				export_excel(data);
			}
		});

	});

});

function export_excel(result){
	window.open('redeem/C_history_redeem_grouped/export_excel?'+result.data+'','_blank');  
}

function find_data_reload(result){

  oTable.ajax.url('redeem/C_history_redeem_grouped/get_data?'+result.data).load();

}

</script>

<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

    <form class="form-horizontal" method="post" id="form_history_redeem" action="<?php echo base_url().'redeem/C_history_redeem_grouped/process'?>">

      <!-- pencarian detail data -->
      <div id="accordion" class="accordion-style1 panel-group" style="margin-top: 10px">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <i class="fa fa-search bigger-120"></i> Pencarian detail
              </a>
            </h4>
          </div>
          <div class="panel-collapse collapse in" id="collapseOne">
            <div class="panel-body">
              <br>
              <div class="form-group">
                  <label class="control-label col-md-2">Kode Booking/Tiket</label>
                  <div class="col-md-2">
                    <input type="text" name="kode" class="form-control" id="kode">
                  </div>
              </div>

              <p><b>PENCARIAN BERDASARKAN PRINCIPAL</b></p>
              <div class="form-group">
                  <label class="control-label col-md-2">Principal</label>
                  <div class="col-md-4">
                    <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_principals', 'where'=>array('id' => $this->session->userdata('principal')->id), 'id'=>'id', 'name' => 'principal_name'),$this->session->userdata('principal')->id,'principal_id','principal_id','chosen-slect form-control','','', 'db_mbr');?>
                  </div>
              </div>

              <p><b>PENCARIAN BERDASARKAN WAKTU KUNJUNGAN</b></p>
              <div class="form-group">
                  <label class="control-label col-md-2">Kunjungan Bulan</label>
                  <div class="col-md-2">
                    <?php echo $this->master->get_bulan('' , 'from_month', 'from_month', 'form-control', '','') ?>
                  </div>
                  <label class="control-label col-md-1">s/d Bulan</label>
                  <div class="col-md-2">
                    <?php echo $this->master->get_bulan('' , 'to_month', 'to_month', 'form-control', '','') ?>
                  </div>
                  <label class="control-label col-md-1">Tahun</label>
                  <div class="col-md-2">
                    <?php echo $this->master->get_tahun('' , 'year', 'year', 'form-control', '', '') ?>
                  </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2">Tanggal</label>
                  <div class="col-md-2">
                    <div class="input-group">
                      <input class="form-control date-picker" name="from_tgl" id="from_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                      <span class="input-group-addon">
                        <i class="fa fa-calendar bigger-110"></i>
                      </span>
                    </div>
                  </div>

                  <label class="control-label col-md-1">s/d Tanggal</label>
                  <div class="col-md-2">
                    <div class="input-group">
                      <input class="form-control date-picker" name="to_tgl" id="to_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                      <span class="input-group-addon">
                        <i class="fa fa-calendar bigger-110"></i>
                      </span>
                    </div>
                  </div>
              </div>
              
              <div class="form-group">
                  <label class="col-md-2">&nbsp;</label>
                  <div class="col-md-4">
                    <button class="btn btn-sm btn-primary" type="button" id="btn_search_data" >
                      <i class="ace-icon fa fa-search bigger-110"></i>
                      Search
                    </button>
					<a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
							<i class="fa fa-file-word-o bigger-110"></i>
							Export Excel
						</a>
                  </div>
              </div>

              
              
            </div>
          </div>
        </div>

      </div>

      <hr class="separator">

      <!-- form redeem rombongan -->
      <table id="dynamic-table" base-url="redeem/C_history_redeem_grouped/get_data" url-detail="redeem/C_history_redeem_grouped/show_detail" class="table table-bordered table-hover">
          <thead>
          <tr>  
              <th width="30px" class="center">
              <label class="pos-rel">
                  <input type="checkbox" class="ace" name="select_all" onclick="checkAll(this)"/>
                  <span class="lbl"></span>
              </label>
              </th>
              <th>Kode Booking</th>
              <th>Kunjungan</th>
              <th>Expired</th>
              <th>Nama Pemesan</th>
              <th>Hari Kunjungan</th>
              <th>Group</th>
			  <th>Jumlah</th>
			  <th>Redeem</th>
			  <th>Userid</th>
              <th>Time</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
      </table>

    </form>

  </div><!-- /.col -->
</div><!-- /.row -->





