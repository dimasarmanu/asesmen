<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>

jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
  
});

$(document).ready(function(){

  $('#form_ticket_reverse_redeem').ajaxForm({
      beforeSend: function() {
        achtungShowLoader();  
      },
      uploadProgress: function(event, position, total, percentComplete) {
      },
      complete: function(xhr) {     
        var data=xhr.responseText;
        var jsonResponse = JSON.parse(data);

        if(jsonResponse.status === 200){
          $.achtung({message: jsonResponse.message, timeout:5});
          if( jsonResponse.btn_redeem == 'perorangan'){
            oTable.ajax.reload();
          }else{
            find_data_reload(jsonResponse.output)
          }

        }else{
          $.achtung({message: jsonResponse.message, timeout:5});
        }
        achtungHideLoader();
      }
    }); 

  oTable = $('#dynamic-table').DataTable({ 
          
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "ordering": false,
    "searching": false,
    "bPaginate": false,
    "bInfo": false,
    "pageLength": 50,
    "ajax": {
        "url": $('#dynamic-table').attr('base-url'),
        "type": "POST"
    },

  });

  $('#btn_search_data').click(function (e) {    
      e.preventDefault();
      if( $('#kode').val() != '' ){
        $.ajax({
          url: 'redeem/C_ticket_reverse_redeem/find_data',
          type: "post",
          data: $('#form_ticket_reverse_redeem').serialize(),
          dataType: "json",
          beforeSend: function() {
            achtungShowLoader();  
          },
          success: function(data) {
            achtungHideLoader();
            find_data_reload(data);
          }
        });
      }else{
        alert('Masukan Kode Booking/Tiket !'); return false;
      }
      
  });

});

function find_data_reload(result){

  $.getJSON('redeem/C_ticket_reverse_redeem/find_result_data?'+result.data, '', function (response) {
      var count_dt = response.count;
      var obj_dt = response.result;

      // jika lebih dari 0
      if( response.count > 0 ){
        // jika rombongan
        if( response.td_is_group == 'ROMBONGAN' ){
          
          // show detail data rombongan
          $('#form_rombongan').show();
          $('#form_perorangan').hide();
          showDetailFormRombongan(response);
          
        // jika perorangan
        }else{
          // jika perorangan lebih dari 1 maka tampilkan datatable
          $('#form_rombongan').hide();
          $('#form_perorangan').show();
          $('#kode_booking_perorangan').val(response.td_booking_code);
          oTable.ajax.url('redeem/C_ticket_reverse_redeem/get_data?'+result.data).load();
        }
      // jika tiket tidak ditemukan 
      }else{
        alert('Data tidak ditemukan'); 
        return $('#page-area-content').load('redeem/C_ticket_reverse_redeem');
      }
      
  });

}

function showDetailFormRombongan(obj){
 

  $('#dt_header_rombongan tbody').remove();
  var sisa_tiket = obj.available;  
  $('<tr><td>'+obj.td_booking_code+'</td><td>'+obj.td_visit_date+'</td><td>'+obj.td_expired_date+'</td><td>'+obj.td_datepricetype_id+'</td><td>'+obj.td_name_to+'</td><td align="center">'+obj.count+'</td><td>'+obj.td_principal_name+'</td><td align="center">'+obj.isused+'</td><td align="center">'+obj.available+'</td><td>'+obj.td_is_group+'</td></tr>').appendTo($('#dt_header_rombongan'));
  // auto insert value of sisa tiket
  $('#jml_rombongan').val(0);
  $('#kode_booking_rombongan').val(obj.td_booking_code);
  // cek sisa tiket, jika sudah kosong btn redeem set disabled
  if( sisa_tiket == 0 ){
    $('#btn_redeem_rombongan').attr('disabled', true);
  }else{
    $('#btn_redeem_rombongan').attr('disabled', false);
  }

}

function checkAll(elm) {

  if($(elm).prop("checked") == true){
    $('.checked').each(function(){
        $(this).prop("checked", true);
    });
  }else{
    $('.checked').prop("checked", false);
  }

}

function stayRedeem(elm) {

if($(elm).prop("checked") == true){
  $('#btn_redeem_rombongan').attr('disabled', false);
}else{
  $('#btn_redeem_rombongan').attr('disabled', true);
}

}

</script>

<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

    <form class="form-horizontal" method="post" id="form_ticket_reverse_redeem" action="<?php echo base_url().'redeem/C_ticket_reverse_redeem/process'?>">
      <!-- input kode booking -->
	  <br>
      <div style="margin-top: -10px">
        <label for="form-field-8" style="font-size: 14px">Masukan Kode Booking / Kode Tiket : </label>
        <div class="input-group">
          <input type="text" name="kode" class="form-control" id="kode" style="height: 50px !important; font-size: 28px">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-primary" type="button" id="btn_search_data" style="height: 49px !important;font-size: 28px;">
              <i class="ace-icon fa fa-check-circle bigger-110"></i>
              Go!
            </button>
          </span>
      </div>
      <hr class="separator">
      <!-- form redeem rombongan -->
      <div style="display:none" id="form_rombongan">
        <table class="table" id="dt_header_rombongan">
          <thead style="background-color: #f7f7f7">
          <tr>
              <th>Kode Booking</th>
              <th>Kunjungan</th>
              <th>Expired</th>
              <th>Hari Kunjungan</th>
              <th>Nama Pemesan</th>
			  <th>Jumlah</th>
              <th>Userid</th>
              <th>Redeem</th>
              <th>Sisa</th>
              <th>Group</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <!-- hidden form rombongan -->
        <input type="hidden" name="kode_booking_rombongan" id="kode_booking_rombongan">

        <!-- form input jumlah rombongan -->
        <div>
            <label for="form-field-8" style="font-size: 14px">Masukan jumlah rombongan yang akan di reverse : </label>
            <div class="input-group" style="width: 400px">
              <input type="text" name="jml_rombongan" class="form-control" id="jml_rombongan" style="height: 50px !important; font-size: 28px;">
            </div>
        </div>
        <!-- button redeem rombongan -->
        <div style="margin-left: -8px;padding-top: 7px;">
            <div class="checkbox">
            <button class="btn btn-sm btn-danger" type="submit" id="btn_redeem_rombongan" style="height: 49px !important;font-size: 28px;" name="btn_redeem" value="rombongan">
                <i class="ace-icon fa fa-ticket bigger-110"></i>
                Reverse Redeem
            </button>
            
        </div>
      </div>
      <!-- form redeem perorangan-->
      <div style="display:none" id="form_perorangan">
        <!-- hidden form perorangan -->
        <input type="hidden" name="kode_booking_perorangan" id="kode_booking_perorangan">
        <div style="margin-left: -8px;">
            <button class="btn btn-sm btn-danger" type="submit" name="btn_redeem" value="perorangan" id="btn_redeem_perorangan" style="height: 49px !important;font-size: 28px;" >
                <i class="ace-icon fa fa-ticket bigger-110"></i>
                Reverse Redeem
            </button>
        </div>

        <table id="dynamic-table" base-url="redeem/C_ticket_reverse_redeem/get_data" url-detail="redeem/C_ticket_reverse_redeem/show_detail" class="table table-bordered table-hover">
            <thead>
            <tr>  
                <th width="30px" class="center">
                <label class="pos-rel">
                    <input type="checkbox" class="ace" name="select_all" onclick="checkAll(this)"/>
                    <span class="lbl"></span>
                </label>
                </th>
                <th>Kode Booking</th>
                <th>No. Tiket</th>
                <th>Nama Agent</th>
                <th>Visit</th>
                <th>Expired</th>
                <th>Userid</th>
                <th>Nama Pemesan</th>
                <th>Hari Kunjungan</th>
                <th>Group</th>
                <th>Status</th>
                <th>Issued</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
      
      

    </form>

  </div><!-- /.col -->
</div><!-- /.row -->





