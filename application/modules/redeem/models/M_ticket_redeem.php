<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ticket_redeem extends CI_Model {

	var $table = 'v_barcodeuses';
	var $column = array('v_barcodeuses.barcode','v_barcodeuses.booking_code');
	var $select = 'v_barcodeuses.*, m_accounts.user_name';
	var $order = array('v_barcodeuses.tiket_isused' => 'ASC', 'v_barcodeuses.id' => 'DESC');

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
	}

	private function _main_query()
	{
		$this->db_mbr->select($this->select);
		$this->db_mbr->from($this->table);
		$this->db_mbr->join('m_accounts','m_accounts.id=v_barcodeuses.account_id','left');
		// filter by session principal
		$this->db_mbr->where('v_barcodeuses.principal_id', $this->session->userdata('principal')->id );
		
		if (isset($_GET['kode']) AND $_GET['kode'] != '') {
            $this->db_mbr->where('(booking_code LIKE '."'%".$_GET['kode']."%'".' or barcode LIKE '."'%".$_GET['kode']."%'".')');
		}

		/*search by month*/
		if (isset($_GET['from_month']) AND $_GET['from_month'] != 0 || isset($_GET['to_month']) AND $_GET['to_month'] != 0) {
            $this->db_mbr->where("MONTH(visit_date) between '".$_GET['from_month']."' and '".$_GET['to_month']."'");
        }
        /*search by year*/
        if (isset($_GET['year']) AND $_GET['year'] != 0) {
            $this->db_mbr->where('YEAR(visit_date)='.$_GET['year'].'');	
        }

		/*search by tgl*/
		if (isset($_GET['from_tgl']) AND $_GET['from_tgl'] != 0 || isset($_GET['to_tgl']) AND $_GET['to_tgl'] != 0) {
			$this->db_mbr->where("visit_date between '".$_GET['from_tgl']."' and '".$_GET['to_tgl']."'");
        }

        /*search by principal*/
        if (isset($_GET['principal_id']) AND $_GET['principal_id'] != 0) {
            $this->db_mbr->where('v_barcodeuses.principal_id='.$_GET['principal_id'].'');	
		}

	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();
		
		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db_mbr->like($item, $_POST['search']['value']) : $this->db_mbr->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_mbr->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_mbr->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_mbr->limit($_POST['length'], $_POST['start']);
		$query = $this->db_mbr->get();
		// print_r($this->db_mbr->last_query());die;
		return $query->result();
	}

	function get_data()
	{
		$this->_main_query();
		$query = $this->db_mbr->get();
		// print_r($this->db_mbr->last_query());die;
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_mbr->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db_mbr->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->_main_query();
		if(is_array($id)){
			$this->db_mbr->where_in(''.$this->table.'.id',$id);
			$query = $this->db_mbr->get();
			return $query->result();
		}else{
			$this->db_mbr->where(''.$this->table.'.id',$id);
			$query = $this->db_mbr->get();
			return $query->row();
		}
	}

	public function save($table, $data)
	{
		$this->db_mbr->insert($table, $data);
		return $this->db_mbr->insert_id();
	}

	public function update($table, $where, $data)
	{
		$this->db_mbr->update($table, $data, $where);
		return $this->db_mbr->affected_rows();
	}

	public function list_fields(){
		return $this->db_mbr->list_fields( $this->table );
	}

	public function get_status_booking($params){

		$current = date('Y-m-d');
		$expired = $params->expired_date;
		$visit = $params->visit_date;
		$color = '';
		if( $params->tiket_isused == 1 ){

			$label = '<label class="label label-primary">Issued</label>';
			
		}else{

			/*expired*/
			if($current > $expired){
				$label = '<label class="label label-danger">Expired</label>';
			}else{
				/*available*/
				if($visit == $current || $visit < $current AND $expired > $current){
					$label = '<label class="label label-success">Available</label>';
				}
				
				/*comming soon*/
				if( $current <  $visit ){
					$label = '<label class="label label-warning">Soon</label>';
				}
			}

		}
		
		return $label;

	}

	public function get_available_ticket($kode, $limit){
		$this->db_mbr->from($this->table);
		$this->db_mbr->where('booking_code', $kode);
		$this->db_mbr->where('tiket_isused', 0);
		$this->db_mbr->order_by('id', 'ASC');
		$this->db_mbr->limit($limit);
		return $this->db_mbr->get()->result();
	}

	public function get_reverse_ticket($kode, $limit){
		$this->db_mbr->from($this->table);
		$this->db_mbr->where('booking_code', $kode);
		$this->db_mbr->where('tiket_isused', 1);
		$this->db_mbr->order_by('id', 'ASC');
		$this->db_mbr->limit($limit);
		return $this->db_mbr->get()->result();
	}
	
	public function save_reverse_used($data){
		$this->db_mbr->where_in('id', $data);  
		$this->db_mbr->delete('t_barcodeuses');
	}

	public function save_reverse_used_individual($kode_ticket, $kode_booking){
		$this->db_mbr->from($this->table);
		$this->db_mbr->where('booking_code', $kode_booking);
		$this->db_mbr->where('barcode', $kode_ticket);
		$dt = $this->db_mbr->get()->row();
		
		foreach ($dt as $key => $row_dt) {
            $dataid[] = $row_dt->used_id;
        }
				
		$this->db_mbr->where_in('id', $dataid);  
		$this->db_mbr->delete('t_barcodeuses');
	}

	public function save_used_individual($kode_ticket, $kode_booking){
		$this->db_mbr->from($this->table);
		$this->db_mbr->where('booking_code', $kode_booking);
		$this->db_mbr->where('barcode', $kode_ticket);
		$dt = $this->db_mbr->get()->row();
		return $this->db_mbr->insert('t_barcodeuses', array('barcode_id' => $dt->id) );
	}

	public function save_used($data){
		return $this->db_mbr->insert_batch('t_barcodeuses', $data );
	}
}
