<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_history_redeem_grouped extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'redeem/C_history_redeem_grouped');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('redeem/M_history_redeem_grouped', 'm_history_redeem_grouped');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        /*load view index*/
        $this->load->view('V_history_redeem/index_grouped', $data);
    }
    
    public function get_data()
    {
        /*get data from model*/
        $list = $this->m_history_redeem_grouped->get_datatables();
        $data = array();
        $no = $_POST['start'];
        
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            if( $row_list->tiket_isused == 0 ){
                $row[] = '<div class="center"><label class="pos-rel">
                        <input type="checkbox" class="checked ace" name="selected_id[]" value="'.$row_list->barcode.'"/>
                        <span class="lbl"></span>
                    </label></div>';  
            }else{
                $row[] = '<div class="center"><i class="fa fa-check-circle green bigger-120"></i></div>';  
            }
           
            $row[] = $row_list->booking_code;
            $row[] = $this->tanggal->formatDateFormDmy($row_list->visit_date);
            $row[] = $this->tanggal->formatDateFormDmy($row_list->expired_date);
			$row[] = $row_list->name_to;
			$row[] = $row_list->datepricetype_id;
			$row[] = $row_list->is_group;
			$row[] = $row_list->qty_request;
			$row[] = $row_list->qty;
			$row[] = $row_list->user_id;
            $row[] = $this->tanggal->formatDateTime($row_list->use_time);
            $data[] = $row;
        }
             

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_history_redeem_grouped->count_all(),
                        "recordsFiltered" => $this->m_history_redeem_grouped->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        
    }

    public function find_result_data()
    {   
        
    }

    public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }

	public function export_excel(){

        $result = $this->m_history_redeem_grouped->get_data();

        $data = array(
            'value' => $result,
        );
        // echo '<pre>'; print_r($data);die;
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-type:   application/x-msexcel; charset=utf-8");
        header("Content-Disposition: attachment; filename=export_data_".date('d/m/Y').".xls"); 
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        $html = '';
        $html .= '<p><h3><b>Hasil Pencarian Data</b></h3></p>';
        $html .= $this->load->view('V_history_redeem/view_excel_grouped', $data, true);
        echo $html;
    }
	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
