<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_ticket_reverse_redeem extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'redeem/C_ticket_reverse_redeem');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('redeem/M_ticket_redeem', 'm_ticket_redeem');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        /*load view index*/
        $this->load->view('V_ticket_reverse_redeem/index', $data);
    }
    
    public function get_data()
    {
        /*get data from model*/
        $list = $this->m_ticket_redeem->get_datatables();
        $data = array();
        $no = $_POST['start'];
        if(isset($_GET['kode']) AND $_GET['kode'] != ''){
            foreach ($list as $row_list) {
                $no++;
                $row = array();
                if( $row_list->tiket_isused == 0 ){
                    $row[] = '<div class="center"><label class="pos-rel">
                            <input type="checkbox" class="checked ace" name="selected_id[]" value="'.$row_list->barcode.'"/>
                            <span class="lbl"></span>
                        </label></div>';  
                }else{
                    $row[] = '<div class="center"><i class="fa fa-check-circle green bigger-120"></i></div>';  
                }
                       
                $row[] = $row_list->booking_code;
                $row[] = $row_list->barcode;
                $row[] = ucfirst($row_list->user_name);
                $row[] = $this->tanggal->formatDateFormDmy($row_list->visit_date);
                $row[] = $this->tanggal->formatDateFormDmy($row_list->expired_date);
                $row[] = $row_list->principal_name;
                $row[] = $row_list->name_to;
                $row[] = $row_list->datepricetype_id;
                $row[] = $row_list->is_group;
                // get status booking
                $status_booking = $this->m_ticket_redeem->get_status_booking($row_list);

                $row[] = '<div class="center">'.$status_booking.'</div>';
                $row[] = $this->tanggal->formatDateTime($row_list->use_time);
                $data[] = $row;
            }
        }        

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_ticket_redeem->count_all(),
                        "recordsFiltered" => $this->m_ticket_redeem->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        
        $this->load->library('form_validation');
        $val = $this->form_validation;

        if($_POST['btn_redeem'] == 'rombongan'){
            $val->set_rules('jml_rombongan', 'Jumlah Rombongan', 'trim|required|integer|greater_than[1]');
        }

        if($_POST['btn_redeem'] == 'perorangan'){
            $val->set_rules('kode_booking_perorangan', 'Kode Booking', 'trim|required');
            $val->set_rules('selected_id[]', 'Tiket', 'trim|required', array('required' => 'Tidak ada item yang dipilih'));
        }

        // $val->set_rules('payment_number', 'Nomor Billing', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");
        $val->set_message('integer', "Masukan angka pada field \"%s\"");
        $val->set_message('greater_than', "\"%s\" minimal 1");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            
            // jika rombongan
            if($_POST['btn_redeem'] == 'rombongan'){
                // get tiket available
                $available = $this->m_ticket_redeem->get_reverse_ticket($_POST['kode_booking_rombongan'], $_POST['jml_rombongan']);
                foreach ($available as $key => $row_dt) {
                    $dataid[] = $row_dt->used_id;
                }
                
                $this->m_ticket_redeem->save_reverse_used($dataid);

            }

            if($_POST['btn_redeem'] == 'perorangan'){
                // print_r($_POST);die;
                foreach ($_POST['selected_id'] as $key => $row_dt) {
                    $this->m_ticket_redeem->save_reverse_used_individual($row_dt, $_POST['kode_booking_perorangan']);
                }
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                $output = array( "data" => http_build_query($_POST) . "\n" );

                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'btn_redeem' => $_POST['btn_redeem'], 'output' => $output));
            }
        }
    }

    public function find_result_data()
    {   
        $output = $this->m_ticket_redeem->get_data();
        $current_date = date('Y-m-d');
        $isused =  array();
        $expired =  array();
        $available =  array();
        $soon =  array();

        foreach($output as $row){
            // tiket issued
            if($row->tiket_isused == 1){
                $isused[] = $row;
            }
            if($row->tiket_isused == 0){
                // tiket expired
                if($current_date > $row->expired_date){
                    $expired[] = $row;
                }
                // available
                if($row->visit_date == $current_date || $row->visit_date < $current_date AND $row->expired_date > $current_date){
					$available[] = $row;
                }
                // soon
                if( $current_date <  $row->visit_date ){
					$soon[] = $row;
				}
            }
        }

        $result = array(
                    'count' => count($output), 
                    'isused' => count($isused), 
                    'expired' => count($expired), 
                    'available' => count($output) - count($isused), 
                    'soon' => count($soon), 
                    'td_booking_code' => isset($output[0]->booking_code)?$output[0]->booking_code:'-',
                    'td_user_name' => isset($output[0]->user_name)?$output[0]->user_name:'-',
                    'td_visit_date' => isset($output[0]->visit_date)?$this->tanggal->formatDate($output[0]->visit_date):'-',
                    'td_expired_date' => isset($output[0]->expired_date)?$this->tanggal->formatDate($output[0]->expired_date):'-',
                    'td_principal_name' => isset($output[0]->user_id)?$output[0]->user_id:'-',
                    'td_name_to' => isset($output[0]->name_to)?$output[0]->name_to:'-',
                    'td_datepricetype_id' => isset($output[0]->datepricetype_id)?$output[0]->datepricetype_id:'-',
                    'td_is_group' => isset($output[0]->is_group)?$output[0]->is_group:'-',
                );
        echo json_encode($result);
    }

    public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }

}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
