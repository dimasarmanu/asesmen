<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_download_etiket extends CI_Model {

	var $table = 'v_barcoderequest';
	var $column = array('v_barcoderequest.invoice_code');
	var $select = 'v_barcoderequest.*';
	var $order = array('v_barcoderequest.id' => 'DESC');

	public function __construct()
	{
		parent::__construct();
		$this->db_dynamic = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);
	}

	private function _main_query()
	{
		$this->db_dynamic->select($this->select);
		$this->db_dynamic->from($this->table);
		$this->db_dynamic->where('v_barcoderequest.request_status', 1);

		/*search by tgl*/
		if (isset($_GET['from_tgl']) AND $_GET['from_tgl'] != 0 || isset($_GET['to_tgl']) AND $_GET['to_tgl'] != 0) {
			$this->db_dynamic->where("v_barcoderequest.request_date between '".$_GET['from_tgl']."' and '".$_GET['to_tgl']."'");
        }

	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();
		
		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db_dynamic->like($item, $_POST['search']['value']) : $this->db_dynamic->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_dynamic->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_dynamic->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_dynamic->limit($_POST['length'], $_POST['start']);
		$query = $this->db_dynamic->get();
		// print_r($this->db_dynamic->last_query());die;
		return $query->result();
	}

	function get_data()
	{
		$this->_main_query();
		$query = $this->db_dynamic->get();
		// print_r($this->db_dynamic->last_query());die;
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_dynamic->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		return $this->db_dynamic->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->_main_query();
		if(is_array($id)){
			$this->db_dynamic->where_in(''.$this->table.'.id',$id);
			$query = $this->db_dynamic->get();
			return $query->result();
		}else{
			$this->db_dynamic->where(''.$this->table.'.id',$id);
			$query = $this->db_dynamic->get();
			return $query->row();
		}
	}

	public function save($table, $data)
	{
		$this->db_dynamic->insert($table, $data);
		return $this->db_dynamic->insert_id();
	}

	public function update($table, $where, $data)
	{
		$this->db_dynamic->update($table, $data, $where);
		return $this->db_dynamic->affected_rows();
	}

	public function list_fields(){
		return $this->db_dynamic->list_fields( $this->table );
	}

	public function get_status_booking($params){

		$current = date('Y-m-d');
		$expired = $params->expired_date;
		$visit = $params->visit_date;
		$color = '';
		if( $params->tiket_isused == 1 ){

			$label = '<label class="label label-primary">Issued</label>';
			
		}else{

			/*expired*/
			if($current > $expired){
				$label = '<label class="label label-danger">Expired</label>';
			}else{
				/*available*/
				if($visit == $current || $visit < $current AND $expired > $current){
					$label = '<label class="label label-success">Available</label>';
				}
				
				/*comming soon*/
				if( $current <  $visit ){
					$label = '<label class="label label-warning">Soon</label>';
				}
			}

		}
		
		return $label;

	}

	public function get_available_ticket($kode, $limit){
		$this->db_dynamic->from($this->table);
		$this->db_dynamic->where('booking_code', $kode);
		$this->db_dynamic->where('tiket_isused', 0);
		$this->db_dynamic->order_by('id', 'ASC');
		$this->db_dynamic->limit($limit);
		return $this->db_dynamic->get()->result();
	}

	public function save_used($data){
		return $this->db_dynamic->insert_batch('t_barcodeuses', $data );
	}

	public function save_used_individual($kode_ticket, $kode_booking){
		$this->db_dynamic->from($this->table);
		$this->db_dynamic->where('booking_code', $kode_booking);
		$this->db_dynamic->where('barcode', $kode_ticket);
		$dt = $this->db_dynamic->get()->row();
		return $this->db_dynamic->insert('t_barcodeuses', array('barcode_id' => $dt->id) );
	}

}
