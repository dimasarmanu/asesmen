<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_reseller_balance extends CI_Model {

	var $table = 'v_resellerstatus';
	var $column = array('v_resellerstatus.cursaldo');
	var $select = 'v_resellerstatus.*';
	var $order = array('v_resellerstatus.cursaldo' => 'DESC');
	

	public function __construct()
	{
		parent::__construct();
		$this->db_dynamic = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);
	}

	private function _main_query(){
		$this->db_dynamic->select($this->select);
		$this->db_dynamic->from($this->table);
	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if(isset($_POST['search'])){
				($i===0) ? $this->db_dynamic->like($item, $_POST['search']['value']) : $this->db_dynamic->or_like($item, $_POST['search']['value']);
			}
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_dynamic->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_dynamic->order_by(key($order), $order[key($order)]);
		}

	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_dynamic->limit($_POST['length'], $_POST['start']);
		$query = $this->db_dynamic->get();
		//print_r($this->db_dynamic->last_query());die;
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_dynamic->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_main_query();
		return $this->db_dynamic->count_all_results();
	}

	function get_data()
	{
		$this->_get_datatables_query();
		$query = $this->db_dynamic->get();
		return $query->result();
	}


	public function list_fields(){
		return $this->db_dynamic->list_fields( $this->table );
	}


}
