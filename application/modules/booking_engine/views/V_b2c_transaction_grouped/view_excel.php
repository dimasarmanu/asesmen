<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class="flip-content">
				<tr>  
					<th width="30px" class="center"></th>
					<th>Tanggal</th>
					<th>Status</th>
					<th>Tipe hari</th>
					<th>Produk</th>
					<th>Metode</th>
					<th>PAX</th>
					<th>Total</th>
				</tr>
		</thead>

		<tbody>
				<?php 
						$count = count($value); 
						if( $count > 0 ) :
						$no=0; foreach($value as $row_dt) : $no++;
				?>
						<tr>
							<td align="center"><?php echo $no?></td>
							<td> <?php echo $this->tanggal->formatDateFormDmy($row_dt->transaction_time)?></td>
							<td> <?php echo $row_dt->paymentstatus_name?></td>
							<td> <?php echo $row_dt->dateprice_type?></td>
							<td> <?php echo $row_dt->product_name?></td>
							<td> <?php echo $row_dt->paymenttype_name?> </td>
							<td> <?php echo $row_dt->order_qty?></td>
							<td> <?php echo $row_dt->totaltopay_afterdiscount?></td>
						</tr>
				<?php 
						endforeach; 
						else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
						endif;
				?>
		</tbody>
</table>