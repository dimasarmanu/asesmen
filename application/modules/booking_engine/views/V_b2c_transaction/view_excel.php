<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class="flip-content">
				<tr>  
					<th width="30px" class="center"></th>
					<th>Kode Booking</th>
					<th>Tanggal</th>
					<th>Kunjungan</th>
					<th>Tipe hari</th>
					<th>Nama Pemesan</th>
					<th>No Telp</th>
					<th>Email</th>
					<th>Produk</th>
					<th>Metode Pembayaran</th>
					<th>No Referensi</th>
					<th>Jumlah</th>
					<th>Total</th>
				</tr>
		</thead>

		<tbody>
				<?php 
						$count = count($value); 
						if( $count > 0 ) :
						$no=0; foreach($value as $row_dt) : $no++;
						$debit_from_name = $row_dt->debit_from_name;
						if(trim($row_dt->debit_from_name) == "") {
							$debit_from_name = $row_dt->member_id;
						} 
				?>
						<tr>
								<td align="center"><?php echo $no?></td>
								<td> <?php echo $row_dt->invoice_code?> </td>
								<td> <?php echo $this->tanggal->formatDateFormDmy($row_dt->transaction_time)?> </td>
								<td> <?php echo $this->tanggal->formatDateFormDmy($row_dt->visit_date)?> </td>
								<td> <?php echo $row_dt->dateprice_type?></td>
								<td> <?php echo $row_dt->account_name?></td>
								<td> <?php echo $row_dt->phone?> </td>
								<td> <?php echo $row_dt->account_email?></td>
								<td> <?php echo $row_dt->product_name?></td>
								<td> <?php echo $row_dt->paymenttype_name?></td>
								<td> <?php echo $debit_from_name?> </td>
								<td> <?php echo $row_dt->order_qty?></td>
								<td> <?php echo $row_dt->totaltopay_afterdiscount?></td>

						</tr>
				<?php 
						endforeach; 
						else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
						endif;
				?>
		</tbody>
</table>