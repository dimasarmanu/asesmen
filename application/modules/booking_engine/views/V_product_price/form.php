<script>
jQuery(function($) {

  $('.date-picker').datepicker({
	autoclose: true,
	todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
	$(this).prev().focus();
  });
});

$(document).ready(function() {
	$('select[name="customer_type"]').change(function() {
		$('#product_id option').remove();
		if ($(this).val()) {
			load_dd_product();
		}
	});
});

function load_dd_product(){
	$.getJSON("<?php echo site_url('booking_engine/C_product_price/product_list') ?>/" + $( "#customer_type option:selected" ).val(), '', function(data) {
			$('<option value="">- Pilih produk -</option>').appendTo($('#product_id'));
			$.each(data, function(i, o) {
				$('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#product_id'));		
			});
		});
}
</script>
<div class="page-header">
	<h1>
	<?php echo $title?>
	<small>
	<i class="ace-icon fa fa-angle-double-right"></i>
	<?php echo $breadcrumbs?>
	</small>
	</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="widget-body">
				<div class="widget-main no-padding">
					<form class="form-horizontal" method="post" id="form-default" action="<?php echo site_url('booking_engine/C_product_price/process')?>" enctype="multipart/form-data">
						<br>
						<div class="form-group">
							<label class="control-label col-md-3">ID</label>
							<div class="col-md-1">
								<input name="id" id="id" value="<?php echo isset($value)?$value->productid:0?>" placeholder="Auto" class="form-control" type="text" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Tipe customer</label>
							<div class="col-md-4">
								<select class="chosen-slect form-control" name="customer_type" id="customer_type">
									<option value="1" selected="">PERORANGAN</option>
									<option value="2">RESELLER</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Nama produk</label>
							<div class="col-md-5">
								<?php echo  $this->master->custom_selection_with_dbdynamic(array('table'=>'v_setprice', 'where'=>' isactive = 1 AND accountgroup_id = 1', 'id'=>'id', 'name' => 'name'),isset($value)?$value->product_id:'','product_id','product_id','chosen-slect form-control',($flag=='read')?'readonly':'','', $this->session->userdata('user')->db_name);?>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="control-label col-md-3">Weekdays (Rp)</label>
							<div class="col-md-3">
								<input name="weekday_price" id="weekday_price" value="<?php echo isset($value)?$value->weekday_price:0?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Disc (%) weekdays</label>
							<div class="col-md-1">
								<input name="weekday_discount" id="weekday_discount" value="<?php echo isset($value)?$value->weekday_discount:0?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Weekday (Rp)</label>
							<div class="col-md-3">
								<input name="weekend_price" id="weekend_price" value="<?php echo isset($value)?$value->weekend_price:0?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Disc (%) weekend/public holiday</label>
							<div class="col-md-1">
								<input name="weekend_discount" id="weekend_discount" value="<?php echo isset($value)?$value->weekend_discount:0?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Highseason (Rp)</label>
							<div class="col-md-3">
								<input name="highseason_price" id="highseason_price" value="<?php echo isset($value)?$value->highseason_price:0?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Disc (%) weekend/public holiday</label>
							<div class="col-md-1">
								<input name="highseason_discount" id="highseason_discount" value="<?php echo isset($value)?$value->highseason_discount:0?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
				
						
						<div class="form-actions center">
							<a onclick="getMenu('booking_engine/C_product')" href="#" class="btn btn-sm btn-success">
								<i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
								Kembali ke daftar
							</a>
							<?php if($flag != 'read'):?>
							<button type="reset" id="btnReset" class="btn btn-sm btn-danger">
							<i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
							Reset
							</button>
							<button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
							<i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
							Submit
							</button>
							<?php endif; ?>
						</div>
					</form>
				</div>
			</div>
			
			<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
			</div><!-- /.row -->
			<script src="<?php echo base_url()?>/assets/js/jquery-ui.custom.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.ui.touch-punch.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/bootstrap-markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.hotkeys.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootstrap-wysiwyg.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootbox.js"></script>
			<script type="text/javascript">
					jQuery(function($) {
						$('#editor').ace_wysiwyg({
							toolbar:
							[
								{
									name:'font',
									title:'Custom tooltip',
									values:['Some Font!','Arial','Verdana','Comic Sans MS','Custom Font!']
								},
								null,
								{
									name:'fontSize',
									title:'Custom tooltip',
									values:{1 : 'Size#1 Text' , 2 : 'Size#1 Text' , 3 : 'Size#3 Text' , 4 : 'Size#4 Text' , 5 : 'Size#5 Text'}
								},
								null,
								{name:'bold', title:'Custom tooltip'},
								{name:'italic', title:'Custom tooltip'},
								{name:'strikethrough', title:'Custom tooltip'},
								{name:'underline', title:'Custom tooltip'},
								null,
								'insertunorderedlist',
								'insertorderedlist',
								'outdent',
								'indent',
								null,
								{name:'justifyleft'},
								{name:'justifycenter'},
								{name:'justifyright'},
								{name:'justifyfull'},
								null,
								{
									name:'createLink',
									placeholder:'Custom PlaceHolder Text',
									button_class:'btn-purple',
									button_text:'Custom TEXT'
								},
								{name:'unlink'},
								null,
								{
									name:'insertImage',
									placeholder:'Custom PlaceHolder Text',
									button_class:'btn-inverse',
									//choose_file:false,//hide choose file button
									button_text:'Set choose_file:false to hide this',
									button_insert_class:'btn-pink',
									button_insert:'Insert Image'
								},
								null,
								{
									name:'foreColor',
									title:'Custom Colors',
									values:['red','green','blue','navy','orange'],
									/**
										You change colors as well
									*/
								},
								/**null,
								{
									name:'backColor'
								},*/
								null,
								{name:'undo'},
								{name:'redo'},
								null,
								'viewSource'
							],
							//speech_button:false,//hide speech button on chrome
							
							'wysiwyg': {
								hotKeys : {} //disable hotkeys
							}
							
						}).prev().addClass('wysiwyg-style2');
						
						
						//handle form onsubmit event to send the wysiwyg's content to server
						$('#form-default').on('submit', function(){
							
							//put the editor's html content inside the hidden input to be sent to server
							
							$('#content').val($('#editor').html());
							var formData = new FormData($('#form-default')[0]);
							/*formData.append('content', $('input[name=wysiwyg-value]' , this).val($('#editor').html()) ); */
							//pf_file_name = new Array();
							pf_file = new Array();
								var formData = new FormData($('#form-default')[0]);
								
								i=0;
								
								//formData.append('pf_file_name', pf_file_name);
							url = $('#form-default').attr('action');
							// ajax adding data to database
								$.ajax({
									url : url,
									type: "POST",
									data: formData,
									dataType: "JSON",
									contentType: false,
									processData: false,
									
									beforeSend: function() {
										achtungShowLoader();
									},
									uploadProgress: function(event, position, total, percentComplete) {
									},
									complete: function(xhr) {
										var data=xhr.responseText;
										var jsonResponse = JSON.parse(data);
										if(jsonResponse.status === 200){
											$.achtung({message: jsonResponse.message, timeout:5});
											$('#page-area-content').load('booking_engine/C_product?_=' + (new Date()).getTime());
										}else{
											$.achtung({message: jsonResponse.message, timeout:5});
										}
										achtungHideLoader();
									}
							});
							//but for now we will show it inside a modal box
							/*$('#modal-wysiwyg-editor').modal('show');
							$('#wysiwyg-editor-value').css({'width':'99%', 'height':'200px'}).val($('#editor').html());*/
							
							return false;
						});
						$('#form-default').on('reset', function() {
							$('#editor').empty();
						});
					});
					
			</script>