<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
});

$(document).ready(function() {
	get_summary_transaction();
	oTable = $('#dynamic-table').DataTable({ 				
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"ordering": true,
		"searching": true,
		"bPaginate": true,
		"bInfo": true,
		"pageLength": 50,
		"ajax": {
			"url": $('#dynamic-table').attr('base-url'),
			"type": "POST"
		},
	});

	$('#btn_search_data').click(function (e) {
		e.preventDefault();
		$.ajax({
			url: 'booking_engine/C_reseller_issued/find_data',
			type: "post",
			data: $('#form_search').serialize(),
			dataType: "json",
			beforeSend: function() {
				achtungShowLoader();  
			},
			success: function(data) {
				achtungHideLoader();
				find_data_reload(data);
			}
		});
	});

	$('#btn_reset_data').click(function (e) {
		e.preventDefault();
		oTable.ajax.url('booking_engine/C_reseller_issued/get_data').load();
		$('#form_search')[0].reset();
	});

	$('#btn_export_excel').click(function (e) {
		e.preventDefault();
		$.ajax({
			url: 'booking_engine/C_reseller_issued/find_data',
			type: "post",
			data: $('#form_search').serialize(),
			dataType: "json",
			beforeSend: function() {
				achtungShowLoader();  
			},
			success: function(data) {
				achtungHideLoader();
				export_excel(data);
			}
		});
	});
});


function find_data_reload(result){
	get_summary_transaction();
	oTable.ajax.url('booking_engine/C_reseller_issued/get_data?'+result.data).load();

}

function export_excel(result){
	window.open('booking_engine/C_reseller_issued/export_excel?'+result.data+'','_blank');  
}

function get_summary_transaction(){
	$.ajax({
		url: 'booking_engine/C_reseller_issued/find_data',
		type: "post",
		data: $('#form_search').serialize(),
		dataType: "json",
		success: function(result) {
			$.getJSON("booking_engine/C_reseller_issued/get_summary_transaction?" +result.data, '', function (response) {
				$('#in_total').text(formatMoney(response.in_total));
				$('#out_total').text(formatMoney(response.out_total));
				$('#balance_total').text(formatMoney(response.balance_total));
			});
		}
	});
}

</script>
<div class="row">
	<div class="col-xs-12">

		<div class="page-header">
			<h1>
				<?php echo $title?>
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<?php echo isset($breadcrumbs)?$breadcrumbs:''?>
				</small>
			</h1>
		</div><!-- /.page-header -->

		<form class="form-horizontal" method="post" id="form_search" action="#">
			<div class="row">
				<div class="col-md-12">
					<center>
						<h4>FORM PENCARIAN DATA TRANSAKSI<br>
							<small style="font-size:12px"> Silahkan lakukan pencarian data dengan parameter dibawah ini </small>
						</h4>
					</center>
					<p><b>PENCARIAN BERDASARKAN WAKTU TRANSAKSI (YYYY-MM-DD)</b></p>
					<div class="form-group">
						<label class="control-label col-md-2">Tanggal</label>
							<div class="col-md-2">
								<div class="input-group">
									<input class="form-control date-picker" name="from_tgl" id="from_tgl" type="text" data-date-format="yyyy-mm-dd" value="<?=$month_before;?>"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>

							<label class="control-label col-md-1">s/d Tanggal</label>
							<div class="col-md-2">
								<div class="input-group">
									<input class="form-control date-picker" name="to_tgl" id="to_tgl" type="text" data-date-format="yyyy-mm-dd" value="<?=$date_now;?>"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 ">&nbsp;</label>
						<div class="col-md-10" style="margin-left: 6px">
							<a href="#" id="btn_search_data" class="btn btn-xs btn-default">
								<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								Search
							</a>
							<a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
								<i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
								Reset
							</a>
							<a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
								<i class="fa fa-file-word-o bigger-110"></i>
								Export Excel
							</a>
						</div>
					</div>

				</div>
			</div>
			<hr class="separator">
			<!-- div.table-responsive -->
			<div class="row" style="margin-top: -16px;">
				<div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: gold">
					<span style="font-size: 14px">Total Transaksi</span>
					<h3 style="font-weight: bold; margin-top : 0px"><span id="in_total"></span></h3>
				</div>
			</div>
			<!-- div.dataTables_borderWrap -->
			<div class="row">
				<table id="dynamic-table" base-url="booking_engine/C_reseller_issued/get_data" url-detail="booking_engine/C_reseller_issued/show_detail" class="table table-bordered table-hover">
				 <thead>
					<tr>
						<th style="width:10px"></th>
						<th style="width:100px">No.Order</th>
						<th style="width:150px">Tanggal</th>
						<th>Reseller</th>
						<th>Customer</th>
						<th>Produk</th>
						<th style="width:100px">PAX</th>
						<th style="width:100px">Total(Rp)</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>

		</form>

	</div><!-- /.col -->
</div><!-- /.row -->





