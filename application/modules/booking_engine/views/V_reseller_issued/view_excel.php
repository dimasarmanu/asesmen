<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class="flip-content">
				<tr>  
					<th width="30px" class="center"></th>
					<th>No.Order</th>
					<th>Tanggal</th>
					<th>Reseller</th>
					<th>Produk</th>
					<th>PAX</th>
					<th>Total</th>
				</tr>
		</thead>

		<tbody>
				<?php 
						$count = count($value); 
						if( $count > 0 ) :
						$no=0; foreach($value as $row_dt) : $no++;
				?>
						<tr>
							<td align="center"><?php echo $no?></td>
							<td> <?php echo $row_dt->invoice_code?></td>
							<td> <?php echo $this->tanggal->formatDateFormDmy($row_dt->request_date)?></td>
							<td> <?php echo $row_dt->account_name . " [" . $row_dt->account_email . "]";?></td>
							<td> <?php echo $row_dt->product_name . "[" . $row_dt->dateprice_type . "]";?></td>
							<td> <?php echo $row_dt->qty?></td>
							<td> <?php echo $row_dt->rtotal?></td>
						</tr>
				<?php 
						endforeach; 
						else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
						endif;
				?>
		</tbody>
</table>