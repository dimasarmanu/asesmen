<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
});

$(document).ready(function() {
	get_summary_transaction();
	oTable = $('#dynamic-table').DataTable({ 				
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"ordering": true,
		"searching": true,
		"bPaginate": true,
		"bInfo": true,
		"pageLength": 50,
		"ajax": {
			"url": $('#dynamic-table').attr('base-url'),
			"type": "POST"
		},
	});

	$('#btn_search_data').click(function (e) {
		e.preventDefault();
		$.ajax({
			url: 'booking_engine/C_reseller_balance/find_data',
			type: "post",
			data: $('#form_search').serialize(),
			dataType: "json",
			beforeSend: function() {
				achtungShowLoader();  
			},
			success: function(data) {
				achtungHideLoader();
				find_data_reload(data);
			}
		});
	});

	$('#btn_reset_data').click(function (e) {
		e.preventDefault();
		oTable.ajax.url('booking_engine/C_reseller_balance/get_data').load();
		$('#form_search')[0].reset();
	});

	$('#btn_export_excel').click(function (e) {
		e.preventDefault();
		$.ajax({
			url: 'booking_engine/C_reseller_balance/find_data',
			type: "post",
			data: $('#form_search').serialize(),
			dataType: "json",
			beforeSend: function() {
				achtungShowLoader();  
			},
			success: function(data) {
				achtungHideLoader();
				export_excel(data);
			}
		});
	});
});


function find_data_reload(result){
	get_summary_transaction();
	oTable.ajax.url('booking_engine/C_reseller_balance/get_data?'+result.data).load();

}

function export_excel(result){
	window.open('booking_engine/C_reseller_balance/export_excel?'+result.data+'','_blank');  
}

function get_summary_transaction(){
	$.ajax({
		url: 'booking_engine/C_reseller_balance/find_data',
		type: "post",
		data: $('#form_search').serialize(),
		dataType: "json",
		success: function(result) {
			$.getJSON("booking_engine/C_reseller_balance/get_summary_transaction?" +result.data, '', function (response) {
				$('#in_total').text(formatMoney(response.in_total));
				$('#out_total').text(formatMoney(response.out_total));
				$('#balance_total').text(formatMoney(response.balance_total));
			});
		}
	});
}

</script>
<div class="row">
	<div class="col-xs-12">

		<div class="page-header">
			<h1>
				<?php echo $title?>
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<?php echo isset($breadcrumbs)?$breadcrumbs:''?>
				</small>
			</h1>
		</div><!-- /.page-header -->
		<form class="form-horizontal" method="post" id="form_search" action="#">
			<!-- div.table-responsive -->
			<div class="row">
				<div class="form-group">
					<div class="col-md-10" style="margin-left: 6px">
						<a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
							<i class="fa fa-file-word-o bigger-110"></i>
							Export Excel
						</a>
					</div>
				</div>
				<div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: gold">
					<span style="font-size: 14px">Total Balance</span>
					<h3 style="font-weight: bold; margin-top : 0px"><span id="balance_total"></span></h3>
				</div>
			</div>
			<!-- div.dataTables_borderWrap -->
			<div class="row">
				<table id="dynamic-table" base-url="booking_engine/C_reseller_balance/get_data" url-detail="booking_engine/C_reseller_balance/show_detail" class="table table-bordered table-hover">
				 <thead>
					<tr>
						<th style="width:10px"></th>
						<th>Nama</th>
						<th>Email</th>
						<th>Telp</th>
						<th>Tipe</th>
						<th>Kelas Deposit</th>
						<th style="width:100px">Debet(Rp)</th>
						<th style="width:100px">Kredit(Rp)</th>
						<th style="width:100px">Balance(Rp)</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>

		</form>

	</div><!-- /.col -->
</div><!-- /.row -->





