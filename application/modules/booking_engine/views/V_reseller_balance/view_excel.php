<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class="flip-content">
				<tr>  
					<th width="30px" class="center"></th>
					<th>Nama</th>
					<th>Email</th>
					<th>Telp</th>
					<th>Tipe</th>
					<th>Kelas Deposit</th>
					<th style="width:100px">Debet(Rp)</th>
					<th style="width:100px">Kredit(Rp)</th>
					<th style="width:100px">Balance(Rp)</th>
				</tr>
		</thead>

		<tbody>
				<?php 
						$count = count($value); 
						if( $count > 0 ) :
						$no=0; foreach($value as $row_dt) : $no++;
				?>
						<tr>
							<td align="center"><?php echo $no?></td>
							<td> <?php echo $row_dt->name?></td>
							<td> <?php echo $row_dt->email?></td>
							<td> <?php echo $row_dt->phone?></td>
							<td> <?php echo $row_dt->accounttype_name?></td>
							<td> <?php echo $row_dt->class_name?></td>
							<td> <?php echo number_format($row_dt->in_value)?></td>
							<td> <?php echo number_format($row_dt->out_value)?> </td>
							<td> <?php echo number_format($row_dt->cursaldo)?> </td>
							<td> <?php echo $row_dt->balance?></td>
						</tr>
				<?php 
						endforeach; 
						else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
						endif;
				?>
		</tbody>
</table>