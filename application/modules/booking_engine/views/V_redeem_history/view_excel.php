<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>  
          <th width="30px" class="center"></th>
          <th>Status</th>
          <th>Kode Booking</th>
          <th>No Tiket</th>
          <th>Kunjungan</th>
		  <th>Expired</th>
		  <th>Nama Pemesan</th>
          <th>Produk</th>
          <th>Wahana</th>
        </tr>
    </thead>

    <tbody>
        <?php 
            $count = count($value); 
            if( $count > 0 ) :
            $no=0; foreach($value as $row_dt) : $no++; 
            $date_now = date("Y-m-d");
            $status = 'USED'; 
            if( $row_dt->tiket_isused == 0 ){
                $status = 'AVAILABLE';
            }
        ?>
            <tr>
                <td align="center"><?php echo $no?></td>
                <td><?php echo $status?> </td>
                <td><?php echo $row_dt->invoice_code?> </td>
                <td><?php echo $row_dt->barcode?> </td>
                <td><?php echo $this->tanggal->formatDateFormDmy($row_dt->visit_date)?></td>
                <td><?php echo $this->tanggal->formatDateFormDmy($row_dt->end_date)?></td>
                <td><?php echo $row_dt->name?></td>
                <td><?php echo $row_dt->product_name . " " .$row_dt->dateprice_type?> </td>
                <td><?php echo $row_dt->wahana_name?> </td>
            </tr>
        <?php 
            endforeach; 
            else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
            endif;
        ?>
    </tbody>
</table>