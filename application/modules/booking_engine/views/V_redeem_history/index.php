<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>

jQuery(function($) {

	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
	
});

$(document).ready(function(){

	oTable = $('#dynamic-table').DataTable({ 
					
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"ordering": false,
		"searching": true,
		"bPaginate": true,
		"bInfo": true,
		"pageLength": 50,
		"ajax": {
				"url": $('#dynamic-table').attr('base-url'),
				"type": "POST"
		},

	});

	$('#btn_search_data').click(function (e) {    
			e.preventDefault();
			$.ajax({
					url: 'booking_engine/C_redeem_history/find_data',
					type: "post",
					data: $('#form_history_redeem').serialize(),
					dataType: "json",
					beforeSend: function() {
						achtungShowLoader();  
					},
					success: function(data) {
						achtungHideLoader();
						find_data_reload(data);
					}
			});
	});
	$('#btn_export_excel').click(function (e) {
		e.preventDefault();
		$.ajax({
			url: 'booking_engine/C_redeem_history/find_data',
			type: "post",
			data: $('#form_search').serialize(),
			dataType: "json",
			beforeSend: function() {
				achtungShowLoader();  
			},
			success: function(data) {
				achtungHideLoader();
				export_excel(data);
			}
		});

	});

});

function export_excel(result){
	window.open('booking_engine/C_redeem_history/export_excel?'+result.data+'','_blank');  
}

function find_data_reload(result){

	oTable.ajax.url('booking_engine/C_redeem_history/get_data?'+result.data).load();

}

</script>

<div class="row">
	<div class="col-xs-12">

		<div class="page-header">
			<h1>
				<?php echo $title?>
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<?php echo isset($breadcrumbs)?$breadcrumbs:''?>
				</small>
			</h1>
		</div><!-- /.page-header -->

		<form class="form-horizontal" method="post" id="form_history_redeem" action="<?php echo base_url().'booking_engine/C_redeem_history/process'?>">

			<!-- pencarian detail data -->
			<div class="row">
				<div class="col-md-12">
					<b>Pencarian Data Redeem</b><br><br>
					<div class="form-group">
						<label class="control-label col-md-2">Tanggal</label>
							<div class="col-md-2">
								<div class="input-group">
									<input class="form-control date-picker" name="from_tgl" id="from_tgl" type="text" data-date-format="yyyy-mm-dd" value="<?=$month_before;?>"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>

							<label class="control-label col-md-1">s/d Tanggal</label>
							<div class="col-md-2">
								<div class="input-group">
									<input class="form-control date-picker" name="to_tgl" id="to_tgl" type="text" data-date-format="yyyy-mm-dd" value="<?=$date_now;?>"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>
							</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2 ">&nbsp;</label>
						<div class="col-md-10" style="margin-left: 6px">
							<a href="#" id="btn_search_data" class="btn btn-xs btn-default">
								<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								Search
							</a>
							<a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
								<i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
								Reset
							</a>
							<a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
								<i class="fa fa-file-word-o bigger-110"></i>
								Export Excel
							</a>
						</div>
					</div>

				</div>
			</div>

			<hr class="separator">

			<!-- form redeem rombongan -->
			<table id="dynamic-table" base-url="booking_engine/C_redeem_history/get_data" url-detail="booking_engine/C_redeem_history/show_detail" class="table table-bordered table-hover">
					<thead>
					<tr>  
							<th width="30px" class="center"></th>
							<th style="width:100px">Kode Booking</th>
							<th style="width:100px">No.Tiket</th>
							<th style="width:100px">Kunjungan</th>
							<th style="width:100px">Expired</th>
							<th>Nama Pemesan</th>
							<th>Produk</th>
							<th>Wahana</th>
					</tr>
					</thead>
					<tbody>
					</tbody>
			</table>

		</form>

	</div><!-- /.col -->
</div><!-- /.row -->





