<script>
$(document).ready(function(){
  $('#show_index_data').load('booking_engine/C_highseason_calendar/datatable_view');
  $('#calendar_view').click(function () {
    $('#show_index_data').load('booking_engine/C_highseason_calendar/calendar_view');
  });
  $('#datatable_view').click(function () {
    $('#show_index_data').load('booking_engine/C_highseason_calendar/datatable_view');
  });
})

</script>
    
<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

    <div class="clearfix" style="margin-bottom:-5px">
      <?php echo $this->authuser->show_button('booking_engine/C_highseason_calendar','C','',1)?>
      <?php echo $this->authuser->show_button('booking_engine/C_highseason_calendar','D','',5)?>
      <div class="pull-right tableTools-container"></div>
    </div>
    <hr class="separator">
    <!-- div.table-responsive -->
    <form class="form-horizontal" method="post" id="form_highseason" action="<?php echo site_url('booking_engine/C_highseason_calendar/process')?>" enctype="multipart/form-data">
      <div class="form-group">
        <label class="control-label col-md-2">Tampilkan dengan</label>
        <div class="col-md-4">
          <div class="radio">
                <label>
                  <input name="show_by" id="calendar_view" type="radio" class="ace" value="calendar" />
                  <span class="lbl"> Fullcalendar</span>
                </label>
                <label>
                  <input name="show_by" id="datatable_view" type="radio" class="ace" value="datatable" checked/>
                  <span class="lbl"> Datatable</span>
                </label>
          </div>
        </div>
      </div>
    </form>

    <div id="show_index_data"></div>

  </div><!-- /.col -->
</div><!-- /.row -->






