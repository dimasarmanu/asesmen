<link rel="stylesheet" href="<?php echo  base_url()?>assets/css/jquery-ui.custom.css" />
<link rel="stylesheet" href="<?php echo  base_url()?>assets/css/fullcalendar.css" />
<!-- div.dataTables_borderWrap -->
<div class="row">
      <div class="col-sm-9">
        <div class="space"></div>

        <!-- #section:plugins/data-time.calendar -->
        <div id="calendar"></div>

        <!-- /section:plugins/data-time.calendar -->
      </div>
    </div>

<!-- fullcalendar -->
<script src="<?php echo base_url()?>assets/js/jquery-ui.custom.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/moment.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar.js"></script>
<script src="<?php echo base_url()?>assets/js/bootbox.js"></script>

<script type="text/javascript">

jQuery(function($) {

/* initialize the external events
-----------------------------------------------------------------*/

$('#external-events div.external-event').each(function() {

// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
// it doesn't need to have a start or end
var eventObject = {
title: $.trim($(this).text()) // use the element's text as the event title
};

// store the Event Object in the DOM element so we can get to it later
$(this).data('eventObject', eventObject);



});

/* initialize the calendar
-----------------------------------------------------------------*/

var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();


var calendar = $('#calendar').fullCalendar({
//isRTL: true,
buttonHtml: {
prev: '<i class="ace-icon fa fa-chevron-left"></i>',
next: '<i class="ace-icon fa fa-chevron-right"></i>'
},

header: {
left: 'prev,next today',
center: 'title',
right: 'month'
},

// events: getEventData(),
eventSources: [
// your event source
{
    url: 'booking_engine/C_highseason_calendar/get_highseason_date',
    method: 'POST',
    extraParams: {
    custom_param1: 'something',
    custom_param2: 'somethingelse'
    },
    failure: function() {
    alert('there was an error while fetching events!');
    },
}
// any other sources...
],
editable: true,
droppable: true, // this allows things to be dropped onto the calendar !!!
drop: function(date, allDay) { 
// retrieve the dropped element's stored Event Object
var originalEventObject = $(this).data('eventObject');
var categoryId = $(this).attr('data-id');
var $extraEventClass = $(this).attr('data-class');
console.log(originalEventObject);
// we need to copy it, so that multiple events don't have a reference to the same object
var copiedEventObject = $.extend({}, originalEventObject);
// assign it the date that was reported
copiedEventObject.start = date;
copiedEventObject.allDay = allDay;
copiedEventObject.categoryId = categoryId;
if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];

// render the event on the calendar
// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            
},
selectable: true,
selectHelper: true,
select: function(start, end, allDay) {
}
,
eventClick: function(calEvent, jsEvent, view) {




}

});

})

function delete_data(myid){
if(confirm('Are you sure?')){
$.ajax({
    url: 'booking_engine/C_highseason_calendar/delete',
    type: "post",
    data: {ID:myid},
    dataType: "json",
    beforeSend: function() {
    achtungShowLoader();  
    },
    uploadProgress: function(event, position, total, percentComplete) {
    },
    complete: function(xhr) {     
    var data=xhr.responseText;
    var jsonResponse = JSON.parse(data);
    if(jsonResponse.status === 200){
        $.achtung({message: jsonResponse.message, timeout:5});
        return false;
    }else{
        $.achtung({message: jsonResponse.message, timeout:5});
    }
    achtungHideLoader();
    }

});

}else{
return false;
}

}

</script>
