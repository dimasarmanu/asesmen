<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class="flip-content">
				<tr>  
					<th width="30px" class="center"></th>
					<th>Ref</th>
					<th>Tanggal</th>
					<th>Reseller</th>
					<th>Kelas Deposit</th>
					<th>Debet</th>
					<th>Kredit</th>
					<th>Balance</th>
				</tr>
		</thead>

		<tbody>
				<?php 
						$count = count($value); 
						if( $count > 0 ) :
						$no=0; foreach($value as $row_dt) : $no++;
				?>
						<tr>
							<td align="center"><?php echo $no?></td>
							<td> <?php echo $row_dt->refnumber?></td>
							<td> <?php echo $this->tanggal->formatDateFormDmy($row_dt->trans_time)?></td>
							<td> <?php echo $row_dt->name . " [" . $row_dt->email . "]"?></td>
							<td> <?php echo $row_dt->depositclass_name?></td>
							<td> <?php echo number_format($row_dt->in_value)?></td>
							<td> <?php echo number_format($row_dt->out_value)?> </td>
							<td> <?php echo $row_dt->balance?></td>
						</tr>
				<?php 
						endforeach; 
						else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
						endif;
				?>
		</tbody>
</table>