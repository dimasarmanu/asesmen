<style>
	.chart-legend li span{
		display: inline-block;
		width: 20px;
		height: 20px;
		margin-right: 5px;
	}
	.chart-legend ul{
		list-style-type: none;
	}
	.chart-legend {
		font-size: 11px;
		width:400px;
	}
</style>
<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->
	<!--Content Here -->
	<div class="col-md-12">
		<div class="row">
			<div class="gallery-env" id="divtabaccordion">
				<h4 class="cufon14">Penjualan tiket oleh reseller vs perorangan.</h4>
				<div class="clear"></div>
				<div class="col-md-12">
					Penjualan bulanan  <?=date("Y");?>.
					<canvas id="b2cvsb2b" style="padding: 30px"></canvas>
					<div id="b2cvsb2b-legend" class="chart-legend"></div>
				</div>
				<div class="clear"></div>
				<br>&nbsp;
				<hr>
				<h4 class="cufon14">Penjualan tiket perorangan</h4>
				<div class="clear"></div>
				<div class="col-md-6">
					Penjualan harian minggu berjalan  <?=date("M-Y");?>.
					<canvas id="canvasharian" style="padding: 30px"></canvas>
					<div id="harian-legend" class="chart-legend" style="display:none"></div>
				</div>
				<div class="col-md-6">
					Penjualan mingguan <?=date("M-Y");?>.
					<canvas id="canvasmingguan" style="padding: 30px"></canvas>
					<div id="mingguan-legend" class="chart-legend" style="display:none"></div>
				</div>
				<div class="clear"></div>
				<br>&nbsp;
				<hr>

				<div class="col-md-6">
					Penjualan periode tahun <?=date("Y");?> per-kategori jenis tiket
					<canvas id="canvasrange-waktu" style="padding: 30px"></canvas>
					<div id="range-waktu-legend" class="chart-legend"></div>
				</div>
				<div class="col-md-6">
					Customer Individu<br><br>
					<table class="table table-bordered table-striped datatable dataTable" id="datalist" aria-describedby="datalist_info">
						<thead>
							<tr role="row">
								<th class="alignCenter sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="No" style="width: 82px;">No</th>
								<th class="alignCenter alignLeft sorting" role="columnheader" tabindex="0" aria-controls="datalist" rowspan="1" colspan="1" aria-label="Nama: activate to sort column ascending" style="width: 959px;">Nama</th>
								<th class="alignCenter alignRight sorting" role="columnheader" tabindex="0" aria-controls="datalist" rowspan="1" colspan="1" aria-label="Jumlah: activate to sort column ascending" style="width: 182px;">Tiket</th>
								<th class="alignCenter alignRight sorting" role="columnheader" tabindex="0" aria-controls="datalist" rowspan="1" colspan="1" aria-label="Jumlah: activate to sort column ascending" style="width: 182px;">Jumlah</th>
							</tr>
						</thead>
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
				<hr>


			</div>
		</div>
		</div>
	
  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable.js'?>"></script>
<script src="../assets/chartjs/Chart.js"></script>
<script src="../assets/chartjs/Chart.HorizontalBar.js"></script>
<script src="../assets/chartjs/chart-extend.js"></script>
<script src="../assets/js/autoNumeric.js"></script>
<script type="text/javascript">

	var ChartB2CvsB2B = <?=$ChartB2CvsB2B?>;
	var ChartHarianData = <?=$ChartHarianData?>;
	var ChartMingguanData = <?=$ChartMingguanData?>;
	var ChartRangeTimeData = <?=$ChartRangeTimeData?>;
	
	$(document).ready(function() {
		load_chart_b2cb2b();
		load_chart_harian();
		load_chart_mingguan();
		load_pie_chart();
		
		oTable = $('#datalist').dataTable({
			"sDom": 'tp',
			"bSort": false,
			"bProcessing": false,
			"bPaginate": false,
			"bServerSide": true,
			"bFilter": true,
			"sAjaxSource": "<?=base_url();?>booking_engine/C_dashboard/bestreseller_list",
			"aoColumns": [
				{"mDataProp": null, "bSortable": false, sWidth: 50, sClass: 'alignCenter'},
				{"mDataProp": 'name'},
				{"mDataProp": 'total', sWidth: 50, sClass: 'alignRight'},
				{"mDataProp": 'amount', sWidth: 50, sClass: 'alignRight'}
			], "fnDrawCallback": function(oSettings) {
				var iDisplayStart = oSettings._iDisplayStart;
				for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
					var incr = iDisplayStart + 1;
					$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html("<center>" + incr + "</center>");
					iDisplayStart++;
				}
			}, "fnRowCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
				jQuery('td:eq(3)', nRow).autoNumeric('init', {mDec: '0'});
			},
			"fnServerData": function(sSource, aoData, fnCallback) {
				jQuery.ajax({
					"dataType": 'json',
					"type": "GET",
					"url": "<?=base_url();?>booking_engine/C_dashboard/bestreseller_list",
					"data": aoData,
					"success": fnCallback
				});
			}
		});
	});
	
	function load_chart_b2cb2b(){
		var ctx_b2cvsb2b = document.getElementById("b2cvsb2b").getContext("2d");
		ctx_b2cvsb2b.canvas.height = 50;
		window.myLine = new Chart(ctx_b2cvsb2b).Line(ChartB2CvsB2B, {
			responsive: true,
			barShowStroke: true
		});
		document.getElementById('b2cvsb2b-legend').innerHTML = window.myLine.generateLegend();
	}

	function load_chart_harian(){
		var ctx_harian = document.getElementById("canvasharian").getContext("2d");
		ctx_harian.canvas.height = 150;
		window.myBar = new Chart(ctx_harian).Bar(ChartHarianData, {
			responsive: true,
			barShowStroke: false,
			showTooltips: false,
			onAnimationComplete: function() {

				var ctx = this.chart.ctx;
				ctx.font = this.scale.font;
				ctx.fillStyle = this.scale.textColor
				ctx.textAlign = "center";
				ctx.textBaseline = "bottom";

				this.datasets.forEach(function(dataset) {
					dataset.bars.forEach(function(bar) {
						if (bar.value > 0) {
							ctx.fillText(bar.value, bar.x, bar.y - 5);

						}
					});
				});
			}
		});
		document.getElementById('harian-legend').innerHTML = window.myBar.generateLegend();
	}

	function load_chart_mingguan(){
		var ctx_mingguan = document.getElementById("canvasmingguan").getContext("2d");
		ctx_mingguan.canvas.height = 150;
		window.myBar = new Chart(ctx_mingguan).Bar(ChartMingguanData, {
			responsive: true,
			barShowStroke: false,
			showTooltips: false,
			onAnimationComplete: function() {

				var ctx = this.chart.ctx;
				ctx.font = this.scale.font;
				ctx.fillStyle = this.scale.textColor
				ctx.textAlign = "center";
				ctx.textBaseline = "bottom";

				this.datasets.forEach(function(dataset) {
					dataset.bars.forEach(function(bar) {
						if (bar.value > 0) {
							ctx.fillText(bar.value, bar.x, bar.y - 5);

						}
					});
				});
			}
		});
		document.getElementById('mingguan-legend').innerHTML = window.myBar.generateLegend();

	}

	function load_pie_chart(){
		var ctx_range_waktu = document.getElementById("canvasrange-waktu").getContext("2d");
		window.myPie = new Chart(ctx_range_waktu).Pie(ChartRangeTimeData);
		document.getElementById('range-waktu-legend').innerHTML = window.myPie.generateLegend();
	}

</script>


