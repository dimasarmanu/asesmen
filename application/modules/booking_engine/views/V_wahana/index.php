<div class="row">
	<div class="col-xs-12">

		<div class="page-header">
			<h1>
				<?php echo $title?>
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<?php echo isset($breadcrumbs)?$breadcrumbs:''?>
				</small>
			</h1>
		</div><!-- /.page-header -->
		<form class="form-horizontal" method="post" id="form_search" action="booking_engine/C_wahana/find_data">
			
	        <div class="clearfix" style="margin-bottom:-5px">
				<?php echo $this->authuser->show_button('booking_engine/C_wahana','C','',1)?>
				<div class="pull-right tableTools-container"></div>
			</div>
			<hr class="separator">
			<!-- div.table-responsive -->

			<!-- div.dataTables_borderWrap -->
			<div style="margin-top:-15px">
				<table id="dynamic-table" base-url="booking_engine/C_wahana" url-detail="booking_engine/C_wahana/show_detail" class="table table-striped table-bordered table-hover">
				 <thead>
					<tr>  
						<th width="30px" class="center"></th>
						<th >Nama</th>
						<th width="30px">Kapasitas</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</form>
		</div>
	</div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable.js'?>"></script>



