<script>
jQuery(function($) {

  $('.date-picker').datepicker({
	autoclose: true,
	todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
	$(this).prev().focus();
  });
});

$(document).ready(function() {

});

</script>
<div class="page-header">
	<h1>
	<?php echo $title?>
	<small>
	<i class="ace-icon fa fa-angle-double-right"></i>
	<?php echo $breadcrumbs?>
	</small>
	</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="widget-body">
				<div class="widget-main no-padding">
					<form class="form-horizontal" method="post" id="form-default" action="<?php echo site_url('booking_engine/C_wahana/process')?>" enctype="multipart/form-data">
						<br>
						<div class="form-group">
							<label class="control-label col-md-3">ID</label>
							<div class="col-md-1">
								<input name="id" id="id" value="<?php echo isset($value)?$value->id:0?>" placeholder="Auto" class="form-control" type="text" readonly>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Nama</label>
							<div class="col-md-4">
								<input name="wahana_name" id="wahana_name" value="<?php echo isset($value)?$value->wahana_name:''?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Kapasitas</label>
							<div class="col-md-1">
								<input name="default_capaity" id="default_capaity" value="<?php echo isset($value)?$value->default_capaity:1?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						
						
						<div class="form-actions center">
							<a onclick="getMenu('booking_engine/C_wahana')" href="#" class="btn btn-sm btn-success">
								<i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
								Kembali ke daftar
							</a>
							<?php if($flag != 'read'):?>
							<button type="reset" id="btnReset" class="btn btn-sm btn-danger">
							<i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
							Reset
							</button>
							<button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
							<i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
							Submit
							</button>
							<?php endif; ?>
						</div>
					</form>
				</div>
			</div>
			
			<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
			</div><!-- /.row -->
			<script src="<?php echo base_url()?>/assets/js/jquery-ui.custom.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.ui.touch-punch.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/bootstrap-markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.hotkeys.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootstrap-wysiwyg.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootbox.js"></script>
			<script type="text/javascript">
				jQuery(function($) {
					$('#form-default').on('submit', function(){
						$('#content').val($('#editor').html());
						var formData = new FormData($('#form-default')[0]);
						pf_file = new Array();
						var formData = new FormData($('#form-default')[0]);
						i=0;
							
						url = $('#form-default').attr('action');
						$.ajax({
							url : url,
							type: "POST",
							data: formData,
							dataType: "JSON",
							contentType: false,
							processData: false,
							beforeSend: function() {
								achtungShowLoader();
							}, uploadProgress: function(event, position, total, percentComplete) {
							}, complete: function(xhr) {
								var data=xhr.responseText;
								var jsonResponse = JSON.parse(data);
								if(jsonResponse.status === 200){
									$.achtung({message: jsonResponse.message, timeout:5});
									$('#page-area-content').load('booking_engine/C_wahana?_=' + (new Date()).getTime());
								}else{
									$.achtung({message: jsonResponse.message, timeout:5});
								}
								achtungHideLoader();
							}
						});
						return false;
					});
					$('#form-default').on('reset', function() { $('#editor').empty(); });
				});
					
			</script>