<script>
jQuery(function($) {

  $('.date-picker').datepicker({
	autoclose: true,
	todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
	$(this).prev().focus();
  });
});

$(document).ready(function() {
	$('select[name="customer_type"]').change(function() {
		$('#product_id option').remove();
		if ($(this).val()) {
			load_dd_product();
		}
	});
});

function load_dd_product(){
	$.getJSON("<?php echo site_url('booking_engine/C_product_bundling/productcanbundling_list') ?>/" + $( "#customer_type option:selected" ).val(), '', function(data) {
		$('<option value="">- Pilih produk -</option>').appendTo($('#product_id'));
		$.each(data, function(i, o) {
			$('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#product_id'));		
		});
	});
}
</script>
<div class="page-header">
	<h1>
	<?php echo $title?>
	<small>
	<i class="ace-icon fa fa-angle-double-right"></i>
	<?php echo $breadcrumbs?>
	</small>
	</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="widget-body">
				<div class="widget-main no-padding">
					<form class="form-horizontal" method="post" id="form-default" action="<?php echo site_url('booking_engine/C_product_bundling/process')?>" enctype="multipart/form-data">
						<br>
						<div class="form-group">
							<label class="control-label col-md-3">ID</label>
							<div class="col-md-1">
								<input name="id" id="id" value="<?php echo isset($value)?$value->id:0?>" placeholder="Auto" class="form-control" type="text" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Tipe customer</label>
							<div class="col-md-4">
								<select class="chosen-slect form-control" name="customer_type" id="customer_type">
									<option value="1" selected="">PERORANGAN</option>
									<option value="2">RESELLER</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Nama produk</label>
							<div class="col-md-5">
								<?php echo  $this->master->custom_selection_with_dbdynamic(array('table'=>'v_productcanbundling', 'where'=>'accountgroup_id = 1', 'id'=>'id', 'name' => 'name'),isset($value)?$value->product_id:'','product_id','product_id','chosen-slect form-control',($flag=='read')?'readonly':'','', $this->session->userdata('user')->db_name);?>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="control-label col-md-3">Wahana</label>
							<div class="col-md-3">
								<?php echo  $this->master->custom_selection_with_dbdynamic(array('table'=>'m_wahanas', 'where'=>'id IS NOT NULL AND id <> -1', 'id'=>'id', 'name' => 'wahana_name'),isset($value)?$value->wahana_id:'','wahana_id','wahana_id','chosen-slect form-control',($flag=='read')?'readonly':'','', $this->session->userdata('user')->db_name);?>
							</div>
						</div>
						
						
						<div class="form-actions center">
							<a onclick="getMenu('booking_engine/C_product_bundling')" href="#" class="btn btn-sm btn-success">
								<i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
								Kembali ke daftar
							</a>
							<?php if($flag != 'read'):?>
							<button type="reset" id="btnReset" class="btn btn-sm btn-danger">
							<i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
							Reset
							</button>
							<button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
							<i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
							Submit
							</button>
							<?php endif; ?>
						</div>
					</form>
				</div>
			</div>
			
			<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
			</div><!-- /.row -->
			<script src="<?php echo base_url()?>/assets/js/jquery-ui.custom.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.ui.touch-punch.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/bootstrap-markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.hotkeys.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootstrap-wysiwyg.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootbox.js"></script>
			<script type="text/javascript">
				jQuery(function($) {
					$('#form-default').on('submit', function(){
						$('#content').val($('#editor').html());
						var formData = new FormData($('#form-default')[0]);
						pf_file = new Array();
						var formData = new FormData($('#form-default')[0]);
						i=0;
							
						url = $('#form-default').attr('action');
						$.ajax({
							url : url,
							type: "POST",
							data: formData,
							dataType: "JSON",
							contentType: false,
							processData: false,
							beforeSend: function() {
								achtungShowLoader();
							}, uploadProgress: function(event, position, total, percentComplete) {
							}, complete: function(xhr) {
								var data=xhr.responseText;
								var jsonResponse = JSON.parse(data);
								if(jsonResponse.status === 200){
									$.achtung({message: jsonResponse.message, timeout:5});
									$('#page-area-content').load('booking_engine/C_product_bundling?_=' + (new Date()).getTime());
								}else{
									$.achtung({message: jsonResponse.message, timeout:5});
								}
								achtungHideLoader();
							}
						});
						return false;
					});
					$('#form-default').on('reset', function() { $('#editor').empty(); });
				});
					
			</script>