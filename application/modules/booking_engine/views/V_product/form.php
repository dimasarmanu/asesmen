<script>
jQuery(function($) {

  $('.date-picker').datepicker({
	autoclose: true,
	todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
	$(this).prev().focus();
  });
});

$(document).ready(function() {

	$('select[name="accounttype_id"]').change(function() {
		$('#category_id option').remove();
		$('#depositclass_id option').remove();
		if ($(this).val()) {
			load_dd_category();
			load_dd_kelasdeposit();
		}
	});
	load_dd_category();
	load_dd_kelasdeposit();
});

function load_dd_category(){
	$.getJSON("<?php echo site_url('booking_engine/C_product/category_product') ?>/" + $( "#accounttype_id option:selected" ).val(), '', function(data) {
			$('<option value="">-Pilih categori-</option>').appendTo($('#category_id'));
			$.each(data, function(i, o) {
				if(o.category_id == <?php echo isset($value)?$value->category_id:0?>){
					$('<option value="' + o.category_id + '" selected>' + o.category_name + '</option>').appendTo($('#category_id'));
				}else{
					$('<option value="' + o.category_id + '">' + o.category_name + '</option>').appendTo($('#category_id'));
				}
				
			});
		});
}
function load_dd_kelasdeposit(){
	$.getJSON("<?php echo site_url('booking_engine/C_product/kelas_deposit') ?>/" + $( "#accounttype_id option:selected" ).val(), '', function(data) {
			$('<option value="">-Pilih categori-</option>').appendTo($('#depositclass_id'));
			$.each(data, function(i, o) {
				if(o.id == <?php echo isset($value)?$value->depositclass_id:0?>){
					$('<option value="' + o.id + '" selected>' + o.name + '</option>').appendTo($('#depositclass_id'));	
				}else{
					$('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#depositclass_id'));	
				}
				
			});
		});
}
</script>
<div class="page-header">
	<h1>
	<?php echo $title?>
	<small>
	<i class="ace-icon fa fa-angle-double-right"></i>
	<?php echo $breadcrumbs?>
	</small>
	</h1>
	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->
			<div class="widget-body">
				<div class="widget-main no-padding">
					<form class="form-horizontal" method="post" id="form-default" action="<?php echo site_url('booking_engine/C_product/process')?>" enctype="multipart/form-data">
						<br>
						<div class="form-group">
							<label class="control-label col-md-3">ID</label>
							<div class="col-md-1">
								<input name="id" id="id" value="<?php echo isset($value)?$value->productid:0?>" placeholder="Auto" class="form-control" type="text" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Status</label>
							<div class="col-md-4">
								<?php echo $isactive;?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Nama</label>
							<div class="col-md-4">
								<input name="name" id="name" value="<?php echo isset($value)?$value->productname:''?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Gambar produk</label>
							<div class="col-md-3">
							<input name="product_image" id="product_image" placeholder="" class="form-control" type="file">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="control-label col-md-3">Wahana</label>
							<div class="col-md-3">
								<?php echo  $this->master->custom_selection_with_dbdynamic(array('table'=>'m_wahanas', 'where'=>'id IS NOT NULL', 'id'=>'id', 'name' => 'wahana_name'),isset($value)?$value->wahana_id:'','wahana_id','wahana_id','chosen-slect form-control',($flag=='read')?'readonly':'','', $this->session->userdata('user')->db_name);?>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="control-label col-md-3">Tipe customer</label>
							<div class="col-md-3">
								<?php echo  $this->master->custom_selection_with_dbdynamic(array('table'=>'m_accounttypes', 'where'=>'group_id = 2 OR group_id = 1', 'id'=>'id', 'name' => 'name'),isset($value)?$value->accounttype_id:'','accounttype_id','accounttype_id','chosen-slect form-control',($flag=='read')?'readonly':'','', $this->session->userdata('user')->db_name);?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Kategori</label>
							<div class="col-md-3">
								<select class="chosen-slect form-control" name="category_id" id="category_id">
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Kelas deposit</label>
							<div class="col-md-3">
								<select class="chosen-slect form-control" name="depositclass_id" id="depositclass_id">
								</select>
							</div>
						</div>
						
						<hr>
						<div class="form-group">
							<label class="control-label col-md-3">Mulai tayang</label>
								<div class="col-md-2">
									<div class="input-group">
										<input class="form-control date-picker" name="start_tayang" id="start_tayang" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value)?$value->start_tayang:''?>"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							<label class="control-label col-md-1">Sampai</label>
								<div class="col-md-2">
									<div class="input-group">
										<input class="form-control date-picker" name="end_tayang" id="end_tayang" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value)?$value->end_tayang:''?>"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Berlaku dari</label>
								<div class="col-md-2">
									<div class="input-group">
										<input class="form-control date-picker" name="start_date" id="start_date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value)?$value->start_date:''?>"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							<label class="control-label col-md-1">Sampai</label>
								<div class="col-md-2">
									<div class="input-group">
										<input class="form-control date-picker" name="end_date" id="end_date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value)?$value->end_date:''?>"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Min tanggal kedatangan (H-) hari</label>
							<div class="col-md-1">
								<input name="visitdate_transdate_min" id="visitdate_transdate_min" value="<?php echo isset($value)?$value->visitdate_transdate_min:0?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="control-label col-md-3">Urutan tampian</label>
							<div class="col-md-1">
								<input name="show_order" id="show_order" value="<?php echo isset($value)?$value->show_order:1?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Keterangan</label>
							<div class="col-md-5">
								<textarea name="note" class="form-control" style="height:150px !important"><?php echo isset($value)?$value->productdesc:''?></textarea>
							</div>
						</div>
					
						
						<div class="form-actions center">
							<a onclick="getMenu('booking_engine/C_product')" href="#" class="btn btn-sm btn-success">
								<i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
								Kembali ke daftar
							</a>
							<?php if($flag != 'read'):?>
							<button type="reset" id="btnReset" class="btn btn-sm btn-danger">
							<i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
							Reset
							</button>
							<button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
							<i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
							Submit
							</button>
							<?php endif; ?>
						</div>
					</form>
				</div>
			</div>
			
			<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
			</div><!-- /.row -->
			<script src="<?php echo base_url()?>/assets/js/jquery-ui.custom.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.ui.touch-punch.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/markdown/bootstrap-markdown.js"></script>
			<script src="<?php echo base_url()?>/assets/js/jquery.hotkeys.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootstrap-wysiwyg.js"></script>
			<script src="<?php echo base_url()?>/assets/js/bootbox.js"></script>
			<script type="text/javascript">
					jQuery(function($) {
						$('#editor').ace_wysiwyg({
							toolbar:
							[
								{
									name:'font',
									title:'Custom tooltip',
									values:['Some Font!','Arial','Verdana','Comic Sans MS','Custom Font!']
								},
								null,
								{
									name:'fontSize',
									title:'Custom tooltip',
									values:{1 : 'Size#1 Text' , 2 : 'Size#1 Text' , 3 : 'Size#3 Text' , 4 : 'Size#4 Text' , 5 : 'Size#5 Text'}
								},
								null,
								{name:'bold', title:'Custom tooltip'},
								{name:'italic', title:'Custom tooltip'},
								{name:'strikethrough', title:'Custom tooltip'},
								{name:'underline', title:'Custom tooltip'},
								null,
								'insertunorderedlist',
								'insertorderedlist',
								'outdent',
								'indent',
								null,
								{name:'justifyleft'},
								{name:'justifycenter'},
								{name:'justifyright'},
								{name:'justifyfull'},
								null,
								{
									name:'createLink',
									placeholder:'Custom PlaceHolder Text',
									button_class:'btn-purple',
									button_text:'Custom TEXT'
								},
								{name:'unlink'},
								null,
								{
									name:'insertImage',
									placeholder:'Custom PlaceHolder Text',
									button_class:'btn-inverse',
									//choose_file:false,//hide choose file button
									button_text:'Set choose_file:false to hide this',
									button_insert_class:'btn-pink',
									button_insert:'Insert Image'
								},
								null,
								{
									name:'foreColor',
									title:'Custom Colors',
									values:['red','green','blue','navy','orange'],
									/**
										You change colors as well
									*/
								},
								/**null,
								{
									name:'backColor'
								},*/
								null,
								{name:'undo'},
								{name:'redo'},
								null,
								'viewSource'
							],
							//speech_button:false,//hide speech button on chrome
							
							'wysiwyg': {
								hotKeys : {} //disable hotkeys
							}
							
						}).prev().addClass('wysiwyg-style2');
						
						
						//handle form onsubmit event to send the wysiwyg's content to server
						$('#form-default').on('submit', function(){
							
							//put the editor's html content inside the hidden input to be sent to server
							
							$('#content').val($('#editor').html());
							var formData = new FormData($('#form-default')[0]);
							/*formData.append('content', $('input[name=wysiwyg-value]' , this).val($('#editor').html()) ); */
							//pf_file_name = new Array();
							pf_file = new Array();
								var formData = new FormData($('#form-default')[0]);
								
								i=0;
								
								//formData.append('pf_file_name', pf_file_name);
							url = $('#form-default').attr('action');
							// ajax adding data to database
								$.ajax({
									url : url,
									type: "POST",
									data: formData,
									dataType: "JSON",
									contentType: false,
									processData: false,
									
									beforeSend: function() {
										achtungShowLoader();
									},
									uploadProgress: function(event, position, total, percentComplete) {
									},
									complete: function(xhr) {
										var data=xhr.responseText;
										var jsonResponse = JSON.parse(data);
										if(jsonResponse.status === 200){
											$.achtung({message: jsonResponse.message, timeout:5});
											$('#page-area-content').load('booking_engine/C_product?_=' + (new Date()).getTime());
										}else{
											$.achtung({message: jsonResponse.message, timeout:5});
										}
										achtungHideLoader();
									}
							});
							//but for now we will show it inside a modal box
							/*$('#modal-wysiwyg-editor').modal('show');
							$('#wysiwyg-editor-value').css({'width':'99%', 'height':'200px'}).val($('#editor').html());*/
							
							return false;
						});
						$('#form-default').on('reset', function() {
							$('#editor').empty();
						});
					});
					
			</script>