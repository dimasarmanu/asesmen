<div class="row">
	<div class="col-xs-12">

		<div class="page-header">
			<h1>
				<?php echo $title?>
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<?php echo isset($breadcrumbs)?$breadcrumbs:''?>
				</small>
			</h1>
		</div><!-- /.page-header -->
		<form class="form-horizontal" method="post" id="form_search" action="booking_engine/C_product/find_data">
			<b>Pencarian Data Produk</b><br><br>
			<div class="form-group">
				<label class="control-label col-md-2">Kategori</label>
	            <div class="col-md-3">
	            	<?php echo  $this->master->custom_selection_with_dbdynamic(array('table'=>'m_category', 'where'=>'id IS NOT NULL', 'id'=>'id', 'name' => 'name'),1,'category_id','category_id','chosen-slect form-control','','', $this->session->userdata('user')->db_name);?>							
	            </div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2">Status</label>
				<div class="col-md-2">
					<?php echo $isactive;?>
				</div>
			</div>
			<div class="form-group">
	          <label class="control-label col-md-2 ">&nbsp;</label>
	          <div class="col-md-10" style="margin-left: 6px">
	            <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
	              <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
	              Search
	            </a>
	            <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
	              <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
	              Reset
	            </a>
	          </div>
	        </div>
	        <hr>
	        <div class="clearfix" style="margin-bottom:-5px">
				<?php echo $this->authuser->show_button('booking_engine/C_product','C','',1)?>
				<a href="#" class="btn btn-xs btn-primary" onclick="getMenu('booking_engine/C_product_price/form')"><i class="ace-icon glyphicon glyphicon-plus bigger-50"></i>Pengaturan Harga</a>
				<div class="pull-right tableTools-container"></div>
			</div>
			<hr class="separator">
			<!-- div.table-responsive -->

			<!-- div.dataTables_borderWrap -->
			<div style="margin-top:-15px">
				<table id="dynamic-table" base-url="booking_engine/C_product" url-detail="booking_engine/C_product/show_detail" class="table table-striped table-bordered table-hover">
				 <thead>
					<tr>  
						<th width="30px" class="center"></th>
						<th width="100px">Image</th>
						<th>Nama</th>
						<th>Kategori</th>
						<th>Wahana</th>
						<th>Harga</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</form>
		</div>
	</div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable.js'?>"></script>



