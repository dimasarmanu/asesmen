<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_download_etiket extends MX_Controller {

	/*function constructor*/
	function __construct() {
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_download_etiket');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_download_etiket', 'm_download_etiket');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
	}

	public function index() {
		/*define variable data*/
		
		$today = date('Y-m-d');
		$amonth_before = date($today, strtotime(' -30 day'));
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show(),
			'date_now'=>$today,
			'month_before'=>$amonth_before,
		);
		/*load view index*/
		$this->load->view('V_download_etiket/index', $data);
	}
	
	public function get_data() {
		/*get data from model*/ 
		$list = $this->m_download_etiket->get_datatables();
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$date_now = date("Y-m-d");
			$row[] = '<div class="center"><a href="#" onclick="getMenu('."'booking_engine/C_download_etiket/download/".$row_list->invoice_code."'".')" class="btn btn-xs btn-primary">Download</a></div>'; 
			$row[] = $row_list->invoice_code;
			$row[] = $this->tanggal->formatDateFormDmy($row_list->visit_date);
			$row[] = $this->tanggal->formatDateFormDmy($row_list->request_date);
			$row[] = $row_list->name;
			$row[] = $row_list->email_to;
			$row[] = $row_list->product_name. " " . $row_list->dateprice_type;
			
			$row[] = '';
			$row[] = '';
			$data[] = $row;
		}
			 
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m_download_etiket->count_all(),
						"recordsFiltered" => $this->m_download_etiket->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function download($booking_code) {
		

		$file = "/var/www/html/demo/waterkingdom.jatrav.com/assets/images/invoice/" . $booking_code;

		if (file_exists($file)) {

			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename="' . urlencode($booking_code) . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}else{
			echo 'File tidak ditemukan!';
		}
	}

	public function process() {	
	}

	public function find_data() {   
		$output = array( "data" => http_build_query($_POST) . "\n" );
		echo json_encode($output);
	}

	public function export_excel(){

		$result = $this->m_download_etiket->get_data();

		$data = array(
			'value' => $result,
		);
		// echo '<pre>'; print_r($data);die;
		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-type:   application/x-msexcel; charset=utf-8");
		header("Content-Disposition: attachment; filename=export_data_".date('d/m/Y').".xls"); 
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		$html = '';
		$html .= '<p><h3><b>Hasil Pencarian Data</b></h3></p>';
		$html .= $this->load->view('V_download_etiket/view_excel', $data, true);
		echo $html;
	}
	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
