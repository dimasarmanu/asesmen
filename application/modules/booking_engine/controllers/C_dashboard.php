<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_dashboard extends MX_Controller {

	function __construct() {
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_dashboard');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		
		/*load model*/
		$this->load->model('booking_engine/M_dashboard', 'm_dashboard');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
		$this->db_dynamic = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);

	}

	public function index() {
		/*define variable data*/
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show()
		);
		
		$data["ChartB2CvsB2B"] = $this->HGChartBulananDataB2BvsB2C1();
		$data["ChartHarianData"] = $this->Total_Sales_ChartHarianData();
		$data["ChartMingguanData"] = $this->Total_Sales_ChartMingguanData();
		$data["ChartRangeTimeData"] = $this->Total_Sales_ChartRangeTimeData();
		
		
		//print_r($this->session->userdata('menus')[0]->menu_id);die;
		$this->load->view('V_dashboard/dashboard_view', $data);
		
	}
	
	 public function HGChartBulananDataB2BvsB2C1() {
		$query = $this->db_dynamic->query('CALL sp_b2cyear()');
		$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);
		
		
		$ArrData = Array();
		//$ArrData = Array(156715000, 158103250, 264752000, 379982000, 230483000, 116413500, 76501000, 0, 0, 0, 0, 0);
		$oo = $query->row_array();
		//print_r($oo);die;
		
		foreach ($oo as $key => $val) {
			array_push($ArrData, $oo[$key]);
		}

		$query2 = $this->db_dynamic->query('CALL sp_b2byear()');
		$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);

		$ArrData2 = Array();
		//$ArrData2 = Array(1852930000, 3263867500, 3348175070, 1820362510, 1396055020, 2283763000, 1899902500, 0, 0, 0, 0, 0);
		$oo2 = $query2->row_array();
		foreach ($oo2 as $key => $val) {
			array_push($ArrData2, $oo2[$key]);
		}

		$objchartdata = new stdClass();
		$objchartdata->labels = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
		$objchartdata->datasets = array(
			array(
				"label" => "B2C (Perorangan)",
				"fillColor" => "rgba(220,220,220,0.2)",
				"strokeColor" => "rgba(220,220,220,1)",
				"pointColor" => "rgba(220,220,220,1)",
				"pointStrokeColor" => "#fff",
				"pointHighlightFill" => "#fff",
				"pointHighlightStroke" => "rgba(220,220,220,1)",
				"data" => $ArrData
			), 
			array(
				"label" => "B2B (Reseller)",
				"fillColor" => "rgba(151,187,205,0.2)",
				"strokeColor" => "rgba(151,187,205,1)",
				"pointColor" => "rgba(151,187,205,1)",
				"pointStrokeColor" => "#fff",
				"pointHighlightFill" => "#fff",
				"pointHighlightStroke" => "rgba(151,187,205,1)",
				"data" => $ArrData2
			)
		);
		return json_encode($objchartdata);
	}

	public function Total_Sales_ChartHarianData() {
		$sql = 'CALL sp_reportdaily()';
		$query = $this->db_dynamic->query($sql);
		$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);

		$temp = null;
		$ArrDay = Array();
		$oo = $query->row_array();
		foreach ($oo as $key => $val) {
			$temp_value = $oo[$key];
			if (strpos($oo[$key], "(") !== false) {
				array_push($ArrDay, $oo[$key]);
			}
		}

		$tempData = null;
		$ArrData = Array();

		foreach ($oo as $key => $val) {
			$temp_value = $oo[$key];
			if (strpos($oo[$key], "(") === false) {
				array_push($ArrData, $oo[$key]);
			}
		}


		$objchartdata = new stdClass();
		$objchartdata->labels = $ArrDay;
		$objchartdata->datasets = array(
			array(
				"label" => "E-Ticket",
				"fillColor" => "rgba(60, 182, 231, 1)",
				"data" => $ArrData
			)
		);
		return json_encode($objchartdata);
	}

	public function Total_Sales_ChartBulananData() {
		$sql = 'CALL sp_reprotyearperiod()';
		$query = $this->db_dynamic->query($sql);
		//$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);

		$temp = null;
		$ArrData = Array();
		$oo = $query->row_array();
		foreach ($oo as $key => $val) {
			$temp_value = $oo[$key];
			array_push($ArrData, $oo[$key]);
		}




		$objchartdata = new stdClass();
		$objchartdata->labels = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
		$objchartdata->datasets = array(
			array(
				"label" => "E-Ticket Tahun 2016",
				"fillColor" => "rgba(60, 182, 231, 1)",
				"strokeColor" => "rgba(60, 182, 231, 1)",
				"highlightFill" => "rgba(45,252,157,2.5)",
				"highlightStroke" => "rgba(45,252,157,1)",
				"backgroundColor" => "window.chartColors.red",
				"borderColor" => "window.chartColors.red",
				"fill" => "true",
				"data" => $ArrData
			)
		);
		return json_encode($objchartdata);
	}

	public function Total_Sales_ChartMingguanData() {
		$sql = 'CALL sp_reportmonthly()';
		$query = $this->db_dynamic->query($sql);
		$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);

		$temp = null;
		$ArrDay = Array();
		$oo = $query->row_array();
		foreach ($oo as $key => $val) {
			$temp_value = $oo[$key];
			if (strpos($oo[$key], "Minggu") !== false) {
				array_push($ArrDay, $oo[$key]);
			}
		}

		$tempData = null;
		$ArrData = Array();

		foreach ($oo as $key => $val) {
			$temp_value = $oo[$key];
			if (strpos($oo[$key], "Minggu") === false) {
				array_push($ArrData, $oo[$key]);
			}
		}


		$objchartdata = new stdClass();
		$objchartdata->labels = $ArrDay;
		$objchartdata->datasets = array(
			array(
				"label" => "E-Ticket",
				"fillColor" => "rgba(60, 182, 231, 1)",
				"data" => $ArrData
			)
		);
		return json_encode($objchartdata);
	}

	public function Total_Sales_ChartRangeTimeData() {
		$sql = 'CALL sp_reportyearpie()';
		$query = $this->db_dynamic->query($sql);
		$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);

		$temp = null;
		$ArrData = Array();
		$num = 0;
		foreach ($query->result() as $row) {
			unset($temp);
			$temp = array();

			$temp['value'] = $row->total;
			if ($num == 0) {
				$temp['color'] = "#F7464A";
				$temp['highlight'] = "#5AD3D1";
			}

			if ($num == 1) {
				$temp['color'] = "#46BFBD";
				$temp['highlight'] = "#5AD3D1";
			}

			if ($num == 2) {
				$temp['color'] = "#FDB45C";
				$temp['highlight'] = "#FFC870";
			}

			if ($num == 3) {
				$temp['color'] = "#949FB1";
				$temp['highlight'] = "#A8B3C5";
			}

			$temp['label'] = $row->dateprice_type;
			array_push($ArrData, $temp);
			$num++;
		}
		$objchartdata = $ArrData;
		return json_encode($objchartdata);
	}

	public function BestReseller_list() {
		$osEcho = $this->input->get('sEcho');
		$sLimit = "";
		$sWhere = "";

		if ($this->input->get('iDisplayLength') && $this->input->get('iDisplayLength') != '-1') {
			$sLimit = "LIMIT " . intval($this->input->get('iDisplayStart')) . ", " . intval($this->input->get('iDisplayLength'));
		}

		$sSearch = "";

		$sql = 'CALL sp_reporttop10individu()';
		$querycount = $this->db_dynamic->query($sql);
		$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);


		$iTotal = 10;

		$query = $this->db_dynamic->query($sql);
		$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);


		$iFilteredTotal = $query->num_rows();

		$output = array(
			"sSearch" => $sSearch,
			"sEcho" => $osEcho,
			"iTotalRecords" => $iFilteredTotal,
			"iTotalDisplayRecords" => $iTotal,
			"aaData" => array()
		);
		$temp = null;
		foreach ($query->result() as $row) {
			unset($temp);
			$temp = array();
			$temp['name'] = $row->name;
			$temp['total'] = $row->total;
			$temp['amount'] = $row->amount;
			array_push($output['aaData'], $temp);
		}

		header('Access-Control-Allow-Origin: *');
		header("Content-Type: application/json");

		echo json_encode($output);
	}

	public function Total_Summary_Customer() {
		$sql = 'CALL sp_reportregistered()';
		$query = $this->db_dynamic->query($sql);
		//$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);

		$row = $query->row();

		$objchartdata = new stdClass();
		$objchartdata->user_active = $row->user_active;
		$objchartdata->user_registered = $row->user_registered;

		return json_encode($objchartdata);
	}

	public function Total_Summary_Payment() {
		$sql = 'CALL sp_reportdaily_tile()';
		$query = $this->db_dynamic->query($sql);
		//$this->db_dynamic->freeDBResource($this->db_dynamic->conn_id);

		$row = $query->row();

		$objchartdata = new stdClass();
		$objchartdata->lunas = $row->Lunas;
		$objchartdata->batal = $row->Batal;
		$objchartdata->tunggu = $row->Tunggu;

		return json_encode($objchartdata);
	}


}

/* End of file empty_module.php */
/* Location: ./application/modules/empty_module/controllers/empty_module.php */

