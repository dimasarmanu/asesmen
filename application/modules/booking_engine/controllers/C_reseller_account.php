<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class C_reseller_account extends MX_Controller {
	
	/*function constructor*/
	function __construct() {
		
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_reseller_account');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_reseller_account', 'm_reseller_account');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
	}
	
	public function index() {
		/*define variable data*/
		$data = array(
        'title' => $this->title,
        'breadcrumbs' => $this->breadcrumbs->show()
		);
		$data['is_active'] = $this->get_status_type('is_active', 1, '');
		/*load view index*/
		$this->load->view('V_reseller_account/index', $data);
	}
	
	public function form($id='') {
		/*if id is not null then will show form edit*/
		$data['is_active'] = $this->get_status_type('is_active', 1, '');
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'booking_engine/C_reseller_account/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_reseller_account->get_by_id($id);			
			/*initialize flag for form*/
			$data['flag'] = "update";
			$data['is_active'] = $this->get_status_type('is_active', $data['value']->is_active, '');
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'booking_engine/C_reseller_account/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";
		}

		
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_reseller_account/form', $data);
	}
	
	/*function for view data only*/
	public function show($id) {
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('View '.strtolower($this->title).'', 'booking_engine/C_reseller_account/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_reseller_account->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "read";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['is_active'] = $this->get_status_type('is_active', $data['value']->is_active, 'readonly');
		/*load form view*/
		$this->load->view('V_reseller_account/form', $data);
	}
	
	
	public function show_detail( $id ) {
		$fields = $this->m_reseller_account->list_fields();
		$data = $this->m_reseller_account->get_by_id( $id );
		$html = $this->master->show_detail_row_table( $fields, $data );      	
		echo json_encode( array('html' => $html) );
	}
	
	public function get_data() {
		/*get data from model*/
		$list = $this->m_reseller_account->get_datatables();
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$row[] = '<div class="center"><div class="btn-group">
			<button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
			<span class="ace-icon fa fa-caret-down icon-on-right"></span>
			</button>
			<ul class="dropdown-menu dropdown-inverse">
			'.$this->authuser->show_button_dropdown('booking_engine/C_reseller_account', array('R','U') , $row_list->id).' 
			</ul>
			</div></div>';
			$row[] = $row_list->id;
			$row[] = $row_list->name;
			$row[] = $row_list->email;
			$row[] = $row_list->phone;
			$row[] = $row_list->address;
			$row[] = ($row_list->is_active == 1) ? '<div class="center"><span class="label label-sm label-success">Active</span></div>' : '<div class="center"><span class="label label-sm label-danger">Not active</span></div>';
			$data[] = $row;
		}
		
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->m_reseller_account->count_all(),
		"recordsFiltered" => $this->m_reseller_account->count_filtered(),
		"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	

	public function process() {
		// print_r($_POST);die;
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('name', 'Nama', 'trim|required');
		$val->set_rules('email', 'Email', 'trim|required');
		$val->set_rules('password', 'Password', 'trim|required');
		$val->set_rules('phone', 'Phone', 'trim|required');
		$val->set_rules('company_name', 'Nama bandan usaha', 'trim|required');
		$val->set_rules('accounttype_id', 'Tipe Reseller', 'trim|required');
		$val->set_rules('is_active', 'Status', 'trim|required');
		
		$val->set_message('required', "Silahkan isi field \"%s\"");
		
		if ($val->run() == FALSE) {
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}else{                       
			$this->db->trans_begin();
			$id = ($this->input->post('id'))?$this->input->post('id'):0;
			
			$dataexc = array(
			'name' => $val->set_value('name'),
			'email' => $val->set_value('email'),
			'phone' => $val->set_value('phone'),
			'company_name' => $val->set_value('company_name'),
			'accounttype_id' => $val->set_value('accounttype_id'),
			'is_active' => $val->set_value('is_active')
			);
			
			if($id==0){
				$dataexc['password']= hash('md5', $val->set_value('password'));
				$this->m_reseller_account->save('m_accounts', $dataexc);
				$newId = $this->db->insert_id();
			}else{

				$this->m_reseller_account->update(array('id' => $id), $dataexc);
				$newId = $id;
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}else{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}
	
	public function delete() {
		$id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
		$toArray = explode(',',$id);
		if($id!=null){
			if($this->m_reseller_account->delete_by_id($toArray)){
				echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
				}else{
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
			}
			}else{
			echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
		}		
	}

	public function find_data() {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }
	

	function get_status_type($id, $selected_value, $readonly) {
		$status = "<select class='chosen-slect form-control' name='" .$id."' id='".$id ."' " . $readonly .">";
		$aktif = $selected_value==1?'selected':'';
		$tidakaktif = $selected_value==0?'selected':'';

		$status .= "<option value=1 ".$aktif.">AKTIF</option>";
		$status .= "<option value=0 ".$tidakaktif.">TIDAK AKTIF</option>";
		$status .= "</select>";
		return $status;
	}
	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
