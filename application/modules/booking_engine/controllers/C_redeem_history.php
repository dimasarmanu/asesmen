<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_redeem_history extends MX_Controller {

	/*function constructor*/
	function __construct() {
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_redeem_history');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_redeem_history', 'm_redeem_history');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
	}

	public function index() {
		/*define variable data*/
		
		$today = date('Y-m-d');
		$amonth_before = date($today, strtotime(' -30 day'));
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show(),
			'date_now'=>$today,
			'month_before'=>$amonth_before,
		);
		/*load view index*/
		$this->load->view('V_redeem_history/index', $data);
	}
	
	public function get_data() {
		/*get data from model*/
		$list = $this->m_redeem_history->get_datatables();
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$date_now = date("Y-m-d");
			if( $row_list->tiket_isused == 0 ){
				$row[] = '<div class="center" style="background-color:green;color:#fff">AVAILABLE</div>';
			}else{
				$row[] = '<div class="center" style="background-color:yellow">USED</div>';  
			}
					
			$row[] = $row_list->invoice_code;
			$row[] = $row_list->barcode;
			$row[] = $this->tanggal->formatDateFormDmy($row_list->visit_date);

			if($date_now > $row_list->visit_date) {
				$row[] = '<div class="center" style="background-color:red">' . $this->tanggal->formatDateFormDmy($row_list->end_date) . '</div>'; 
			}else{
				$this->tanggal->formatDateFormDmy($row_list->end_date);
			}

			$row[] = $row_list->name;
			$row[] = $row_list->product_name. " " . $row_list->dateprice_type;
			$row[] = $row_list->wahana_name;
			$row[] = '';
			$row[] = '';
			$data[] = $row;
		}
			 
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m_redeem_history->count_all(),
						"recordsFiltered" => $this->m_redeem_history->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function process() {	
	}

	public function find_data() {   
		$output = array( "data" => http_build_query($_POST) . "\n" );
		echo json_encode($output);
	}

	public function export_excel(){

		$result = $this->m_redeem_history->get_data();

		$data = array(
			'value' => $result,
		);
		// echo '<pre>'; print_r($data);die;
		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-type:   application/x-msexcel; charset=utf-8");
		header("Content-Disposition: attachment; filename=export_data_".date('d/m/Y').".xls"); 
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		$html = '';
		$html .= '<p><h3><b>Hasil Pencarian Data</b></h3></p>';
		$html .= $this->load->view('V_redeem_history/view_excel', $data, true);
		echo $html;
	}
	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
