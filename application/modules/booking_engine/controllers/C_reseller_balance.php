<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_reseller_balance extends MX_Controller {

	/*function constructor*/
	function __construct() {

		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_reseller_balance');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_reseller_balance', 'm_reseller_balance');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

	}

	public function index() {
		/*define variable data*/
		
		$today = date('Y-m-d');
		$amonth_before = date($today, strtotime(' -30 day'));
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show(),
			'date_now'=>$today,
			'month_before'=>$amonth_before,
		);
		/*load view index*/
		$this->load->view('V_reseller_balance/index', $data);
	}

	public function get_data() {
		/*get data from model*/
		$list = $this->m_reseller_balance->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $row_list) {
			$no++;
			$row = array();        
			$row[] = '';
			$row[] = $row_list->name;
			$row[] = $row_list->email;
			$row[] = $row_list->phone;
			$row[] = $row_list->accounttype_name;
			$row[] = $row_list->class_name;
			$row[] = "<div align='right'>" . number_format($row_list->in_value) . "</div>";
			$row[] = "<div align='right'>" . number_format($row_list->out_value) . "</div>";
			$row[] = "<div align='right'>" . number_format($row_list->cursaldo) . "</div>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_reseller_balance->count_all(),
			"recordsFiltered" => $this->m_reseller_balance->count_filtered(),
			"data" => $data,
		);

		//output to json format
		echo json_encode($output);
	}

	public function find_data() {   
		$output = array( "data" => http_build_query($_POST) . "\n" );
		echo json_encode($output);
	}

	public function export_excel(){

		$result = $this->m_reseller_balance->get_data();

		$data = array(
			'value' => $result,
		);
		// echo '<pre>'; print_r($data);die;
		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-type:   application/x-msexcel; charset=utf-8");
		header("Content-Disposition: attachment; filename=export_data_".date('d/m/Y').".xls"); 
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		$html = '';
		$html .= '<p><h3><b>Hasil Pencarian Data</b></h3></p>';
		$html .= $this->load->view('V_reseller_balance/view_excel', $data, true);
		echo $html;
	}

	public function get_summary_transaction(){
        $result = $this->m_reseller_balance->get_data();
        
        foreach($result as $row_list){
            $in_total[] = $row_list->in_value;
            $out_total[] = $row_list->out_value;
        }

        $in_total = isset($in_total) ? array_sum($in_total) : 0;
        $out_total = isset($out_total) ? array_sum($out_total) : 0;
        $balance_total = $in_total - $out_total;
        echo json_encode(array('in_total' => $in_total, 'out_total' => $out_total, 'balance_total' => $balance_total));
    }
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
