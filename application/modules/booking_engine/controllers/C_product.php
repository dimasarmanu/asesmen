<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class C_product extends MX_Controller {
	
	/*function constructor*/
	function __construct() {
		
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_product');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_product', 'm_product');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
	}
	
	public function index() {
		/*define variable data*/
		$data = array(
        'title' => $this->title,
        'breadcrumbs' => $this->breadcrumbs->show()
		);
		$data['isactive'] = $this->get_status_type('isactive', 1, '');
		/*load view index*/
		$this->load->view('V_product/index', $data);
	}
	
	public function form($id='') {
		/*if id is not null then will show form edit*/
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'booking_engine/C_produk/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_product->get_by_id($id);
			$data['isactive'] = $this->get_status_type('isactive', $data['value']->isactive, '');
			/*initialize flag for form*/
			$data['flag'] = "update";
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'booking_engine/C_produk/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";
			$data['isactive'] = $this->get_status_type('isactive', 1, '');
		}
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_product/form', $data);
	}
	
	/*function for view data only*/
	public function show($id) {
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('View '.strtolower($this->title).'', 'booking_engine/C_produk/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_product->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "read";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['isactive'] = $this->get_status_type('isactive', $data['value']->isactive, 'readonly');
		/*load form view*/
		$this->load->view('V_product/form', $data);
	}
	
	
	public function show_detail( $id ) {
		$fields = $this->m_product->list_fields();
		$data = $this->m_product->get_by_id( $id );
		$html = $this->master->show_detail_row_table( $fields, $data );      
		
		echo json_encode( array('html' => $html) );
	}
	
	public function get_data() {
		/*get data from model*/
		$list = $this->m_product->get_datatables();
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$row[] = '<div class="center"><div class="btn-group">
			<button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
			<span class="ace-icon fa fa-caret-down icon-on-right"></span>
			</button>
			<ul class="dropdown-menu dropdown-inverse">
			'.$this->authuser->show_button_dropdown('booking_engine/C_product', array('R','U','D') , $row_list->productid).' 
			</ul>
			</div></div>';
			$row[] = "<img src='http://demowaterkingdom.jatrav.com/assets/images/product/" . $row_list->productimage . "' width='100px'/>";
			$row[] = $row_list->productname;
			$row[] = $row_list->categoryname;
			$row[] = $row_list->wahana_name;
			$temp['price'] = array();
			$price = "<ul><li>Hari Kerja : " . number_format($row_list->weekday_price_after_discount) . "</li><li>Akhir Pekan : " . number_format($row_list->weekend_price_after_discount) . "</li><li>Musim Liburan : " . number_format($row_list->highseason_price_after_discount) . "</li></ul>";
			$row[] = $price;
			
			
			$data[] = $row;
		}
		
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->m_product->count_all(),
		"recordsFiltered" => $this->m_product->count_filtered(),
		"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	

	public function process() {
		// print_r($_POST);die;
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('name', 'Nama', 'trim|required');
		$val->set_rules('category_id', 'Kategori', 'trim|required');
		$val->set_rules('accounttype_id', 'Tipe customer', 'trim|required');
		$val->set_rules('depositclass_id', 'Kelas deposit', 'trim');
		$val->set_rules('start_tayang', 'Mulai tayang', 'trim|required');
		$val->set_rules('end_tayang', 'Akhir tayang', 'trim|required');
		$val->set_rules('start_date', 'Mulai periode', 'trim|required');
		$val->set_rules('end_date', 'Akhir periode', 'trim|required');
		$val->set_rules('show_order', 'Urutan tampilan', 'trim|required');
		$val->set_rules('visitdate_transdate_min', 'Minimum h- order', 'trim|required');
		$val->set_rules('isactive', 'Status aktif', 'trim');
		$val->set_rules('note', 'Deskripsi/keterangan', 'trim');
		$val->set_rules('product_image', 'Upload gambar', 'trim');
		$val->set_rules('wahana_id', 'Wahana', 'trim');
		$val->set_message('required', "Silahkan isi field \"%s\"");
		
		if ($val->run() == FALSE) {
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}else{
			$id = ($this->input->post('id'))?$this->input->post('id'):0;
			$check_exist_transaction = $this->m_product->transction_check('t_orders', 'product_id', 't_orders.product_id = ' .$id);

			if($check_exist_transaction->num_rows() > 0) {
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan, sudah ada transaksi'));
				return;
			}

			$this->db->trans_begin();
			
			
			$dataexc = array(
			'name' => $val->set_value('name'),
			'wahana_id' => $val->set_value('wahana_id'),
			'category_id' => $val->set_value('category_id'),
			'accounttype_id' => $val->set_value('accounttype_id'),
			'depositclass_id' => $val->set_value('depositclass_id'),
			'start_tayang' => $val->set_value('start_tayang'),
			'end_tayang' => $val->set_value('end_tayang'),
			'start_date' => $val->set_value('start_date'),
			'end_date' => $val->set_value('end_date'),
			'show_order' => $this->input->post('show_order'),
			'visitdate_transdate_min' => $val->set_value('visitdate_transdate_min'),
			'isactive' => $val->set_value('isactive'),
			'note' => $val->set_value('note')
			);
			
			if(isset($_FILES['product_image']['name']) AND $_FILES['product_image']['name'] != ''){
			//	/*hapus dulu file yang lama*/
				if( $id != 0 ){
					$res_dt = $this->m_product->get_by_id($id);
					if($res_dt->productimage != NULL){
						if (file_exists('/var/www/html/demo/waterkingdom.jatrav.com/assets/images/product/'.$res_dt->productimage)) {
							unlink('/var/www/html/demo/waterkingdom.jatrav.com/assets/images/product/'.$res_dt->productimage);
						}    
					}
				}
				$dataexc['product_image'] = $this->upload_file->doUpload('product_image', '/var/www/html/demo/waterkingdom.jatrav.com/assets/images/product/');
			}
			
			if($id==0){
				$this->m_product->save('m_products',$dataexc);
				$newId = $this->db->insert_id();
			}else{
				$this->m_product->update(array('id' => $id), $dataexc);
				$newId = $id;
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}else{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}
	
	public function delete() {
		$id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
		$toArray = explode(',',$id);
		if($id!=null){
			if($this->m_product->delete_by_id($toArray)){
				echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
				}else{
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
			}
			}else{
			echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
		}		
	}

	public function find_data() {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }
	
	public function category_product($tipeakun='') {
		$db_booking_engine = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);
		$query = "SELECT category_id , UPPER(category_name) AS category_name from v_accountcategorymaps WHERE id=?";
		$exc = $db_booking_engine->query($query, array($tipeakun));
		echo json_encode($exc->result());
	}

	public function kelas_deposit($tipeakun='') {
		$db_booking_engine = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);
		$query = "SELECT id , UPPER(name) AS name FROM m_depositclass WHERE accounttype_id=?";
		$exc = $db_booking_engine->query($query, array($tipeakun));
		

		echo json_encode($exc->result());
	}

	function get_status_type($id, $selected_value, $readonly) {
		$status = "<select class='chosen-slect form-control' name='" .$id."' id='".$id ."' " . $readonly .">";
		$aktif = $selected_value==1?'selected':'';
		$tidakaktif = $selected_value==0?'selected':'';

		$status .= "<option value=1 ".$aktif.">AKTIF</option>";
		$status .= "<option value=0 ".$tidakaktif.">TIDAK AKTIF</option>";
		$status .= "</select>";
		return $status;
	}
	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
