<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class C_product_bundling extends MX_Controller {
	
	/*function constructor*/
	function __construct() {
		
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_product_bundling');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_product_bundling', 'm_product_bundling');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
	}
	
	public function index() {
		/*define variable data*/
		$data = array(
        'title' => $this->title,
        'breadcrumbs' => $this->breadcrumbs->show()
		);
		/*load view index*/
		$this->load->view('V_product_bundling/index', $data);
	}
	
	public function form($id='') {
		/*if id is not null then will show form edit*/
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'booking_engine/C_product_bundling/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_product_bundling->get_by_id($id);
			/*initialize flag for form*/
			$data['flag'] = "update";
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'booking_engine/C_product_bundling/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";
		}
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_product_bundling/form', $data);
	}
	
	/*function for view data only*/
	public function show($id) {
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('View '.strtolower($this->title).'', 'booking_engine/C_product_bundling/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_product_bundling->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "read";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_product_bundling/form', $data);
	}
	
	
	public function show_detail( $id ) {
		$fields = $this->m_product_bundling->list_fields();
		$data = $this->m_product_bundling->get_by_id( $id );
		$html = $this->master->show_detail_row_table( $fields, $data );      
		
		echo json_encode( array('html' => $html) );
	}
	
	public function get_data() {
		/*get data from model*/
		$list = $this->m_product_bundling->get_datatables();
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$row[] = '<div class="center"><div class="btn-group">
			<button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
			<span class="ace-icon fa fa-caret-down icon-on-right"></span>
			</button>
			<ul class="dropdown-menu dropdown-inverse">
			'.$this->authuser->show_button_dropdown('booking_engine/C_product_bundling', array('D') , $row_list->id).' 
			</ul>
			</div></div>';
			$row[] = $row_list->product_name;
			$row[] = $row_list->wahana_name;	
			$data[] = $row;
		}
		
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->m_product_bundling->count_all(),
		"recordsFiltered" => $this->m_product_bundling->count_filtered(),
		"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	

	public function process() {
		// print_r($_POST);die;
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('product_id', 'Product', 'trim|required');
		$val->set_rules('wahana_id', 'Wahana', 'trim|required');
		$val->set_message('required', "Silahkan isi field \"%s\"");
		
		if ($val->run() == FALSE) {
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}else{
			$id = ($this->input->post('id'))?$this->input->post('id'):0;
			
			$query_double = $this->m_product->transction_check("m_productpaket", "product_id", "m_productpaket.product_id = ".$val->set_value('product_id')." AND m_productpaket.wahana_id = '" . $val->set_value('wahana_id') . "'");
			if($query_double->num_rows() > 0) {
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan, sudah ada transaksi'));
				return;
			}

			$check_exist_transaction = $this->m_product->transction_check('t_orders', 'product_id', 't_orders.product_id = ' . $val->set_value('product_id'));
			if($check_exist_transaction->num_rows() > 0) {
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan, sudah ada transaksi'));
				return;
			}

			$this->db->trans_begin();
			
			
			$dataexc = array(
			'product_id' => $val->set_value('product_id'),
			'wahana_id' => $val->set_value('wahana_id')
			);
			
			
			if($id==0){
				$this->m_product_bundling->save('m_productpaket',$dataexc);
				$newId = $this->db->insert_id();
			}else{
				$this->m_product_bundling->update(array('id' => $id), $dataexc);
				$newId = $id;
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}else{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}
	
	public function delete() {
		$id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;

		$db_booking_engine = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);
		$query = "SELECT id FROM t_orders WHERE product_id IN (SELECT product_id FROM m_productpaket WHERE m_productpaket.product_id IN ($id)) LIMIT 1";
		$check_exist = $db_booking_engine->query($query);

		if($check_exist->num_rows() > 0) {
			echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan, sudah ada transaksi'));
		}
		
		$toArray = explode(',',$id);
		if($id!=null){
			if($this->m_product_bundling->delete_by_id($toArray)){
				echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
				}else{
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
			}
			}else{
			echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
		}		
	}

	public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }

    public function productcanbundling_list($tipeakun='') {
		$db_booking_engine = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);
		$query = "SELECT id, name FROM v_productcanbundling WHERE isactive = 1 AND accountgroup_id =?";
		$exc = $db_booking_engine->query($query, array($tipeakun));
		echo json_encode($exc->result());
	}
	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
