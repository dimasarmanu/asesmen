<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class C_product_price extends MX_Controller {
	
	/*function constructor*/
	function __construct() {
		
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_product_price');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_product_price', 'm_product_price');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
	}
	
	public function index() {
	}
	
	public function form($id='') {
		/*if id is not null then will show form edit*/
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'booking_engine/C_produk_price/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_product->get_by_id($id);
			/*initialize flag for form*/
			$data['flag'] = "update";
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'booking_engine/C_produk_price/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";	
		}
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_product_price/form', $data);
	}


	public function process() {
		
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('product_id', 'Produk', 'trim|required');
		$val->set_rules('weekday_price', 'Harga weekday', 'trim|required');
		$val->set_rules('weekday_discount', 'Weekday discount', 'trim|required');

		$val->set_rules('weekend_price', 'Harga weekend', 'trim|required');
		$val->set_rules('weekend_discount', 'Weekend discount', 'trim|required');

		$val->set_rules('highseason_price', 'Harga highseason', 'trim|required');
		$val->set_rules('highseason_discount', 'Highseason discount', 'trim|required');
		
		$val->set_message('required', "Silahkan isi field \"%s\"");
		
		if ($val->run() == FALSE) {
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}else{                       
			$this->db->trans_begin();
			$id = ($this->input->post('id'))?$this->input->post('id'):0;
			
			$dataexc = array(
			'product_id' => $val->set_value('product_id'),
			'weekday_price' => $val->set_value('weekday_price'),
			'weekday_discount' => $val->set_value('weekday_discount'),

			'weekend_price' => $val->set_value('weekend_price'),
			'weekend_discount' => $val->set_value('weekend_discount'),

			'highseason_price' => $val->set_value('highseason_price'),
			'highseason_discount' => $val->set_value('highseason_discount')
			);
			
			$dataexcupdate = array();
			$dataexcupdate['is_active'] = 0;
				
			$this->m_product_price->update(array(
				'product_id' => $dataexc['product_id']
			), $dataexcupdate);

			$this->m_product_price->save('m_prices',$dataexc);
			$newId = $this->db->insert_id();
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}else{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}

	public function product_list($tipeakun='') {
		$db_booking_engine = $this->master->set_change_database_dynamic($this->session->userdata('user')->db_name);
		$query = "SELECT id, name FROM v_setprice WHERE isactive = 1 AND accountgroup_id =?";
		$exc = $db_booking_engine->query($query, array($tipeakun));
		echo json_encode($exc->result());
	}

	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
