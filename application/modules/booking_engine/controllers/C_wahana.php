<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class C_wahana extends MX_Controller {
	
	/*function constructor*/
	function __construct() {
		
		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_wahana');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_wahana', 'm_wahana');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
	}
	
	public function index() {
		/*define variable data*/
		$data = array(
        'title' => $this->title,
        'breadcrumbs' => $this->breadcrumbs->show()
		);
		/*load view index*/
		$this->load->view('V_wahana/index', $data);
	}
	
	public function form($id='') {
		/*if id is not null then will show form edit*/
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'booking_engine/C_wahana/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_wahana->get_by_id($id);
			/*initialize flag for form*/
			$data['flag'] = "update";
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'booking_engine/C_wahana/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";
		}
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_wahana/form', $data);
	}
	
	/*function for view data only*/
	public function show($id) {
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('View '.strtolower($this->title).'', 'booking_engine/C_wahana/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_wahana->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "read";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_wahana/form', $data);
	}
	
	
	public function show_detail( $id ) {
		$fields = $this->m_wahana->list_fields();
		$data = $this->m_wahana->get_by_id( $id );
		$html = $this->master->show_detail_row_table( $fields, $data );      
		
		echo json_encode( array('html' => $html) );
	}
	
	public function get_data() {
		/*get data from model*/
		$list = $this->m_wahana->get_datatables();
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$row[] = '<div class="center"><div class="btn-group">
			<button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
			<span class="ace-icon fa fa-caret-down icon-on-right"></span>
			</button>
			<ul class="dropdown-menu dropdown-inverse">
			'.$this->authuser->show_button_dropdown('booking_engine/C_wahana', array('R','U','D') , $row_list->id).' 
			</ul>
			</div></div>';
			$row[] = $row_list->wahana_name;
			$row[] = $row_list->default_capaity;		
			$data[] = $row;
		}
		
		$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $this->m_wahana->count_all(),
		"recordsFiltered" => $this->m_wahana->count_filtered(),
		"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	

	public function process() {
		// print_r($_POST);die;
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('wahana_name', 'Nama', 'trim|required');
		$val->set_rules('default_capaity', 'Kapasitas', 'trim|required');
		$val->set_message('required', "Silahkan isi field \"%s\"");
		
		if ($val->run() == FALSE) {
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}else{                       
			$this->db->trans_begin();
			$id = ($this->input->post('id'))?$this->input->post('id'):0;
			
			$dataexc = array(
			'wahana_name' => $val->set_value('wahana_name'),
			'default_capaity' => $val->set_value('default_capaity')
			);
			
			if($id==0){
				$this->m_wahana->save('m_wahanas',$dataexc);
				$newId = $this->db->insert_id();
			}else{
				$this->m_wahana->update(array('id' => $id), $dataexc);
				$newId = $id;
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}else{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}
	
	public function delete() {
		$id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
		$toArray = explode(',',$id);
		if($id!=null){
			if($this->m_wahana->delete_by_id($toArray)){
				echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
				}else{
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
			}
			}else{
			echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
		}		
	}

	public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }
	
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
