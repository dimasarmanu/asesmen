<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_b2c_transaction_grouped extends MX_Controller {

	/*function constructor*/
	function __construct() {

		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'booking_engine/C_b2c_transaction_grouped');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('booking_engine/M_b2c_transaction_grouped', 'm_b2c_transaction_grouped');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

	}

	public function index() {
		/*define variable data*/
		
		$today = date('Y-m-d');
		$amonth_before = date($today, strtotime(' -30 day'));
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show(),
			'date_now'=>$today,
			'month_before'=>$amonth_before,
		);
		/*load view index*/
		$this->load->view('V_b2c_transaction_grouped/index', $data);
	}

	public function get_data() {
		/*get data from model*/
		$list = $this->m_b2c_transaction_grouped->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $row_list) {
			$no++;
			$row = array();        
			$row[] = '';
			$row[] = $this->tanggal->formatDateFormDmy($row_list->transaction_time);
			$row[] = $row_list->paymentstatus_name;
			$row[] = $row_list->dateprice_type;
			$row[] = $row_list->product_name;
			$row[] = "<div align='center'>" . $row_list->paymenttype_name . "</div>";
			$row[] = "<div align='center'>" . $row_list->order_qty . "</div>";
			$row[] = "<div align='right'>" . number_format($row_list->totaltopay_afterdiscount) . "</div>";
			$data[] = $row;
		}
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m_b2c_transaction_grouped->count_all(),
						"recordsFiltered" => $this->m_b2c_transaction_grouped->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function find_data() {   
		$output = array( "data" => http_build_query($_POST) . "\n" );
		echo json_encode($output);
	}

	public function export_excel(){

		$result = $this->m_b2c_transaction_grouped->get_data();

		$data = array(
			'value' => $result,
		);
		// echo '<pre>'; print_r($data);die;
		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-type:   application/x-msexcel; charset=utf-8");
		header("Content-Disposition: attachment; filename=export_data_".date('d/m/Y').".xls"); 
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		$html = '';
		$html .= '<p><h3><b>Hasil Pencarian Data</b></h3></p>';
		$html .= $this->load->view('V_b2c_transaction_grouped/view_excel', $data, true);
		echo $html;
	}

	public function get_summary_transaction(){
		$result = $this->m_b2c_transaction_grouped->get_data();
		
		foreach($result as $row_list){
			$arr_total[] = $row_list->totaltopay_afterdiscount;
		}
		$total = isset($arr_total) ? array_sum($arr_total) : 0;
		echo json_encode(array('total_transaction' => $total));
	}
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
