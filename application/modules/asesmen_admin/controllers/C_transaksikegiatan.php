<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_transaksikegiatan extends MX_Controller
{

    /*function constructor*/
    function __construct()
    {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'assesmen_admin/C_transaksikegiatan');
        /*session redirect login if not login*/
        if ($this->session->userdata('logged') != TRUE) {
            echo 'Session Expired !';
            exit;
        }
        /*load model*/
        $this->load->model('M_transaksikegiatan', 'M_transaksikegiatan');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this))) ? $this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
    }

    public function index()
    {
        //echo '<pre>';print_r($this->session->all_userdata());
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        /*load view index*/
        $this->load->view('V_transaksikegiatan/index', $data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->M_transaksikegiatan->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">
                        <label class="pos-rel">
                            <input type="checkbox" class="ace" name="selected_id[]" value="' . $row_list->idk . '"/>
                            <span class="lbl"></span>
                        </label>
                      </div>';
            $row[] = '<div class="center">
                        ' . $this->authuser->show_button('asesmen_admin/C_transaksikegiatan', 'R', $row_list->idk, 2) . '
                        ' . $this->authuser->show_button('asesmen_admin/C_transaksikegiatan', 'U', $row_list->idk, 2) . '
                        ' . $this->authuser->show_button('asesmen_admin/C_transaksikegiatan', 'D', $row_list->idk, 2) . '
                      </div>';
            $row[] = '<div class="center">' . $row_list->idk . '</div>';
            $row[] = strtoupper($row_list->kegiatan_nama);
            $row[] = strtoupper($row_list->kegiatan_status);
            $row[] = '<div class="btn-toolbar">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary btn-white dropdown-toggle">
                                Action
                                <span class="ace-icon fa fa-caret-down icon-on-right"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-default">
                            <li><a href="#" onclick="getMenu('."'asesmen_admin/C_transaksikegiatan/form_asesor/".$row_list->idk."'".')">Pengaturan asesor</a></li>
                                <li>
                                    <a href="#">Kelola Peserta</a>
                                </li>
                            </ul>
                        </div><!-- /.btn-group -->

                    </div>
                   ';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_transaksikegiatan->count_all(),
            "recordsFiltered" => $this->M_transaksikegiatan->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function form_asesor($id = '')
    {
        /*if id is not null then will show form edit*/
        if ($id != '') {
            /*breadcrumbs for edit*/
            $this->breadcrumbs->push('Edit ' . strtolower($this->title) . '', 'M_transaksikegiatan/' . strtolower(get_class($this)) . '/' . __FUNCTION__ . '/' . $id);
            /*get value by id*/
            $data['value'] = $this->M_transaksikegiatan->get_by_id($id);
            /*initialize flag for form*/
            $data['flag'] = "update";
        } else {
            /*breadcrumbs for create or add row*/
            $this->breadcrumbs->push('Add ' . strtolower($this->title) . '', 'M_transaksikegiatan/' . strtolower(get_class($this)) . '/form');
            /*initialize flag for form add*/
            $data['flag'] = "create";
        }
        /*title header*/
        $data['title'] = $this->title;
        /*show breadcrumbs*/
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_kegiatan/form_asesor', $data);
    }
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
