<script>
    jQuery(function($) {

        $('.date-picker').datepicker({
                autoclose: true,
                todayHighlight: true
            })
            //show datepicker when clicking on the icon
            .next().on(ace.click_event, function() {
                $(this).prev().focus();
            });
    });

    jQuery('#timepicker1').timepicker({
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false,
        disableFocus: true,
        icons: {
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down'
        }
    }).on('focus', function() {
        $('#timepicker1').timepicker('showWidget');
    }).next().on(ace.click_event, function() {
        $(this).prev().focus();
    });



    $(document).ready(function() {

    });
</script>
<div class="page-header">
    <h1>
        <?php echo $title ?>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            <?php echo $breadcrumbs ?>
        </small>
    </h1>
</div><!-- /.page-header -->
<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form class="form-horizontal" method="post" id="form-default" action="<?php echo site_url('asesmen_admin/C_kegiatan/process') ?>" enctype="multipart/form-data">
                    <br>
                    <div class="form-group">
                        <label class="control-label col-md-3">ID</label>
                        <div class="col-md-1">
                            <input name="id" id="id" value="<?php echo isset($value) ? $value->idk : 0 ?>" placeholder="Auto" class="form-control" type="text" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Kode Kegiatan</label>
                        <div class="col-md-4">
                            <input name="tahapan_nama" id="tahapan_nama" value="<?php echo isset($value) ? $value->kegiatan_id : '' ?>" placeholder="" class="form-control" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Description</label>
                        <div class="col-md-4">
                            <textarea name="description" class="form-control" <?php echo ($flag == 'read') ? 'readonly' : '' ?> style="height:70px !important"><?php echo isset($value) ? $value->kegiatan_nama : '' ?></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Tanggal Pertama Kegiatan</label>
                        <div class="col-md-4">

                            <input class="form-control date-picker" name="date" id="date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value) ? $value->kegiatan_tanggal_pertama : '' ?>" />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Tanggal Kedua Kegiatan</label>
                        <div class="col-md-4">
                            <input class="form-control date-picker" name="date" id="date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value) ? $value->kegiatan_tanggal_kedua : '' ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Tanggal Ketiga Kegiatan</label>
                        <div class="col-md-4">
                            <input class="form-control date-picker" name="date" id="date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value) ? $value->kegiatan_tanggal_ketiga : '' ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Kegiatan Mulai</label>
                        <div class="col-md-2">
                            <input name="name" id="name" value="<?php echo isset($value) ? $value->kegiatan_pemb_waktu : '' ?>" placeholder="" class="form-control" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                        <label class="control-label col-md-3">Kegiatan Selesai</label>
                        <div class="col-md-2">
                            <input name="code" id="code" value="<?php echo isset($value) ? $value->kegiatan_pemb_waktu_selesai : '' ?>" placeholder="" class="form-control" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Kegiatan Mulai</label>
                        <div class="col-md-2">
                            <input name="name" id="name" value="<?php echo isset($value) ? $value->kegiatan_pemb_waktu_wita : '' ?>" placeholder="" class="form-control" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                        <label class="control-label col-md-3">Kegiatan Selesai</label>
                        <div class="col-md-2">
                            <input name="code" id="code" value="<?php echo isset($value) ? $value->kegiatan_pemb_waktu_selesai_wita : '' ?>" placeholder="" class="form-control" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Kegiatan Mulai</label>
                        <div class="col-md-2">
                            <input name="timepicker1" id="timepicker1" value="<?php echo isset($value) ? $value->kegiatan_pemb_waktu_wit : '' ?>" placeholder="" class="form-control" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                        <label class="control-label col-md-3">Kegiatan Selesai</label>
                        <div class="col-md-2">
                            <input name="code" id="code" value="<?php echo isset($value) ? $value->kegiatan_pemb_waktu_selesai_wit : '' ?>" placeholder="" class="form-control time-picker" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Kegiatan URL</label>
                        <div class="col-md-6">
                            <input name="tahapan_nama" id="tahapan_nama" value="<?php echo isset($value) ? $value->kegiatan_url : '' ?>" placeholder="" class="form-control" type="text" <?php echo ($flag == 'read') ? 'readonly' : '' ?>>
                        </div>
                    </div>

                   

                    <div class="form-group">
							<label class="control-label col-md-3">Status Kegiatan</label>
							<div class="col-md-4">
								<?php echo $status_kegiatan;?>
							</div>
                        </div>
                        
                        <div class="form-group">
							<label class="control-label col-md-3">Status Kegiatan Trial</label>
							<div class="col-md-4">
								<?php echo $status_kegiatan_trial;?>
							</div>
						</div>

                    

                    <div class="form-actions center">
                        <a onclick="getMenu('asesmen_admin/C_tahapan')" href="#" class="btn btn-sm btn-success">
                            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                            Kembali ke daftar
                        </a>
                        <?php if ($flag != 'read') : ?>
                            <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                                <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                                Reset
                            </button>
                            <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                                <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                                Submit
                            </button>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url() ?>/assets/js/jquery-ui.custom.js"></script>
<script src="<?php echo base_url() ?>/assets/js/jquery.ui.touch-punch.js"></script>
<script src="<?php echo base_url() ?>/assets/js/markdown/markdown.js"></script>
<script src="<?php echo base_url() ?>/assets/js/markdown/bootstrap-markdown.js"></script>
<script src="<?php echo base_url() ?>/assets/js/jquery.hotkeys.js"></script>
<script src="<?php echo base_url() ?>/assets/js/bootstrap-wysiwyg.js"></script>
<script src="<?php echo base_url() ?>/assets/js/bootbox.js"></script>
<script type="text/javascript">
    jQuery(function($) {
        $('#form-default').on('submit', function() {
            $('#content').val($('#editor').html());
            var formData = new FormData($('#form-default')[0]);
            pf_file = new Array();
            var formData = new FormData($('#form-default')[0]);
            i = 0;

            url = $('#form-default').attr('action');
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                dataType: "JSON",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    achtungShowLoader();
                },
                uploadProgress: function(event, position, total, percentComplete) {},
                complete: function(xhr) {
                    var data = xhr.responseText;
                    var jsonResponse = JSON.parse(data);
                    if (jsonResponse.status === 200) {
                        $.achtung({
                            message: jsonResponse.message,
                            timeout: 5
                        });
                        $('#page-area-content').load('asesmen_admin/C_tahapan?_=' + (new Date()).getTime());
                    } else {
                        $.achtung({
                            message: jsonResponse.message,
                            timeout: 5
                        });
                    }
                    achtungHideLoader();
                }
            });
            return false;
        });
        $('#form-default').on('reset', function() {
            $('#editor').empty();
        });
    });
</script>