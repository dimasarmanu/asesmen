<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tahapan extends CI_Model {

	var $table = 'tbl_tahapan';
	var $column = array('tbl_tahapan.thp_id');
	var $select = 'tbl_tahapan.*';
	var $order = array('tbl_tahapan.thp_id' => 'ASC');
	

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
	}

	
	private function _main_query(){
		$this->db_mbr->select($this->select);
		$this->db_mbr->from($this->table);
	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();
		$i = 0;
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db_mbr->like($item, $_POST['search']['value']) : $this->db_mbr->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_mbr->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_mbr->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_mbr->limit($_POST['length'], $_POST['start']);
		$query = $this->db_mbr->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_mbr->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_main_query();
		return $this->db_mbr->count_all_results();
    }
    

    public function get_by_id($id)
	{
        $this->_main_query();
        
		if(is_array($id)){
			$this->db_mbr->where_in(''.$this->table.'.thp_id',$id);
			$query = $this->db_mbr->get();
			return $query->result();
		}else{
			$this->db_mbr->where(''.$this->table.'.thp_id',$id);
			$query = $this->db_mbr->get();
			return $query->row();
		}
		
	}

	public function save($data)
	{
		$this->db_mbr->insert($this->table, $data);
		return $this->db_mbr->insert_id();
	}

	public function update($where, $data)
	{
		$this->db_mbr->update($this->table, $data, $where);
		return $this->db_mbr->affected_rows();
	}

	public function delete_by_id($id)
	{
		$get_data = $this->get_by_id($id);
		$this->db_mbr->where_in(''.$this->table.'.thp_id', $id);
		return $this->db_mbr->update($this->table);
	}


}
