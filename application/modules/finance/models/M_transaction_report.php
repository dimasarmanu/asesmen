<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaction_report extends CI_Model {

	var $table = 'v_requestticket';
	var $column = array('v_requestticket.email_to','v_requestticket.booking_code','v_requestticket.partner_productname','v_requestticket.principal_name','v_requestticket.user_name','v_requestticket.datepricetype_id' );
	var $select = 'v_requestticket.*';
	var $order = array('v_requestticket.trans_time' => 'DESC');

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
	}

	private function _main_query(){
		$this->db_mbr->select($this->select);
		$this->db_mbr->from($this->table);
		// filter by session principal
		$this->db_mbr->where('v_requestticket.principal_id', $this->session->userdata('principal')->id );

		/*search by month*/
		if (isset($_GET['from_month']) AND $_GET['from_month'] != 0 || isset($_GET['to_month']) AND $_GET['to_month'] != 0) {
            $this->db_mbr->where("MONTH(trans_time) between '".$_GET['from_month']."' and '".$_GET['to_month']."'");
        }
        /*search by year*/
        if (isset($_GET['year']) AND $_GET['year'] != 0) {
            $this->db_mbr->where('YEAR(trans_time)='.$_GET['year'].'');	
        }

		/*search by tgl*/
		if (isset($_GET['from_tgl']) AND $_GET['from_tgl'] != 0 || isset($_GET['to_tgl']) AND $_GET['to_tgl'] != 0) {
			$this->db_mbr->where("trans_time between '".$_GET['from_tgl']."' and '".$_GET['to_tgl']."'");
        }

        /*search by principal*/
        if (isset($_GET['principal_id']) AND $_GET['principal_id'] != 0) {
            $this->db_mbr->where('principal_id='.$_GET['principal_id'].'');	
        }

        /*search by produk principal*/
        if (isset($_GET['product']) AND $_GET['product'] != 0) {
            $this->db_mbr->where('product_id='.$_GET['product'].'');	
        }

        /*search by reseller*/
        if (isset($_GET['account_id']) AND $_GET['account_id'] != 0) {
            $this->db_mbr->where('account_id='.$_GET['account_id'].'');	
        }

	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db_mbr->like($item, $_POST['search']['value']) : $this->db_mbr->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_mbr->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_mbr->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_mbr->limit($_POST['length'], $_POST['start']);
		$query = $this->db_mbr->get();
		// print_r($this->db_mbr->last_query());die;
		return $query->result();
	}

	function get_data()
	{
		$this->_main_query();
		$query = $this->db_mbr->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_mbr->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_main_query();
		return $this->db_mbr->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->_main_query();
		if(is_array($id)){
			$this->db_mbr->where_in(''.$this->table.'.id',$id);
			$query = $this->db_mbr->get();
			return $query->result();
		}else{
			$this->db_mbr->where(''.$this->table.'.id',$id);
			$query = $this->db_mbr->get();
			return $query->row();
		}
		
	}

	public function save($data)
	{
		$this->db_mbr->insert($this->table, $data);
		return $this->db_mbr->insert_id();
	}

	public function update($where, $data)
	{
		$this->db_mbr->update($this->table, $data, $where);
		return $this->db_mbr->affected_rows();
	}

	public function delete_by_id($id)
	{
		$get_data = $this->get_by_id($id);
		if( $this->delete_image_default($get_data[0]) ){
			$this->db_mbr->where_in(''.$this->table.'.id', $id);
			return $this->db_mbr->delete($this->table);
		}else{
			return false;
		}
		
	}

	public function delete_image_default($data){
		/*print_r($data);die;*/
		/*if file images exist*/
		if ( file_exists(PATH_MBR.$data->icon_image) ) {
			if($data->icon_image != NULL){
				/*delete first icon_image file*/
	            unlink(PATH_MBR.$data->icon_image);
			}
        }
        return true;
	}


	public function list_fields(){
		return $this->db_mbr->list_fields( $this->table );
	}


}
