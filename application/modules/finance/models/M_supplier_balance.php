<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_supplier_balance extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
		/*get data from session*/
		$this->supplier = $this->session->userdata('user')->relation_id;
	}

	public function get_balance(){
		if( $this->session->userdata('user')->user_id != 1 ){
			$this->db->where( array('supplier_id' => $this->supplier ) );
		}
		// filter by session principal
		$this->db_mbr->where('principal_id', $this->session->userdata('principal')->id );
		
		$this->db_mbr->select('v_supplierbalance.*');
		$this->db_mbr->select('(SELECT total FROM v_sum_principal_trans_by_price_type WHERE principal_id=v_supplierbalance.principal_id AND datepricetype_id="WEEKDAY") as weekday');
		$this->db_mbr->select('(SELECT total FROM v_sum_principal_trans_by_price_type WHERE principal_id=v_supplierbalance.principal_id AND datepricetype_id="WEEKEND") as weekend');
		$this->db_mbr->select('(SELECT total FROM v_sum_principal_trans_by_price_type WHERE principal_id=v_supplierbalance.principal_id AND datepricetype_id="HIGHSEASON") as highseason');
		return $this->db_mbr->get('v_supplierbalance')->result();
	}


}
