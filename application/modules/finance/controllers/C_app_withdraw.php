<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_app_withdraw extends MX_Controller {

	/*function constructor*/
	function __construct() {

		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'finance/C_app_withdraw');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('finance/M_app_topup', 'm_app_topup');
		$this->load->model('finance/M_transaction_report', 'm_transaction_report');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
		
	}

	public function index() {
		/*define variable data*/
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show()
		);
		/*load view index*/
		$this->load->view('V_app_topup/index_withdraw', $data);
	}

	public function form($id='')
	{
		/*if id is not null then will show form edit*/
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Verify '.strtolower($this->title).'', 'finance/C_app_withdraw/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_app_topup->get_by_id($id);
			/*initialize flag for form*/
			$data['flag'] = "update";
			// if click here then update seen
			$ress_dt = $data['value'];
			if( $ress_dt->status_topup == 0 ){
				$this->m_app_topup->update('t_supplierdeposits', array('id' => $ress_dt->deposit_id) , array('status_topup' => 1) );
			}
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'finance/C_app_withdraw/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";
		}
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_app_topup/form', $data);
	}

	/*function for view data only*/
	public function show($id)
	{
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('View '.strtolower($this->title).'', 'finance/C_app_withdraw/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_app_topup->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "read";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		
		/*load form view*/
		$this->load->view('V_app_topup/form', $data);
	}

	public function verifikasi($id)
	{
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('Verifikasi '.strtolower($this->title).'', 'finance/C_app_withdraw/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_app_topup->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "verify";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_app_topup/form_verifikasi', $data);
	}

	public function show_detail( $id )
	{
		$fields = $this->m_app_topup->list_fields();
		$data = $this->m_app_topup->get_by_id( $id );
		$html = $this->master->show_detail_row_table( $fields, $data );      

		echo json_encode( array('html' => $html) );
	}

	public function get_summary(){
		$result = $this->m_app_topup->get_data();
		foreach($result as $row_list){
			$arr_total_amount[] = $row_list->amount;
		}
		$total_amount = array_sum($arr_total_amount);

		$result_trans = $this->m_transaction_report->get_data();

		$arr_total_trans = array();

		foreach($result_trans as $row_list_trans){
			$arr_total_trans[] = $row_list_trans->total_priceprincipal;
		}
		$total_trans = array_sum($arr_total_trans);
		//print_r($arr_total_trans);

		// sisa total
		$balance = $total_trans - $total_amount;


		echo json_encode(array('total_amount' => $total_amount, 'total_trans' => $total_trans, 'balance' => $balance));
	}

	public function get_data()
	{
		/*get data from model*/
		$list = $this->m_app_topup->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = $row_list->topup_number;
			$row[] = $this->tanggal->formatDateFormDmy($row_list->deposit_date);
			//$row[] = "<b>Bank Pengirim : </b><br>Nama : " . $row_list->sender_name . "<br>No Rek : ". $row_list->sender_acc_no . "<br>Bank : " . $row_list->bank_name;
			$row[] = "Nama : " . $row_list->acc_name . "<br>No Rek : ". $row_list->acc_no . "<br>Bank : " . $row_list->acc_bank_name;
			$row[] = '<div align="right">Rp. '.number_format($row_list->amount).',-</div>';
			$status_topup = $this->m_app_topup->get_status_withdraw( $row_list );
			$row[] = '<div align="center">'.$status_topup.'</div>';
			
			//if( $row_list->status_topup != 2 ){
			//    $row[] = '<div class="center"><a href="#" onclick="getMenu('."'finance/C_app_withdraw/form/".$row_list->deposit_id."'".')" class="btn btn-xs btn-primary">Verify</a></div>'; 
			//}else{
			//    $row[] = '<div class="center">'.$row_list->approved_num.'</div>';
			//}
		   
				   
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m_app_topup->count_all(),
						"recordsFiltered" => $this->m_app_topup->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function process()
	{
		
		$this->load->library('form_validation');
		$val = $this->form_validation;
		if( $_POST['btn_submit'] == 1 ){
			$val->set_rules('pernyataan', 'Pernyataan', 'trim|required');            
		}
		$val->set_rules('app_by', 'Approved By', 'trim|required');

		$val->set_message('required', "Silahkan isi field \"%s\"");

		if ($val->run() == FALSE)
		{
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}
		else
		{                       
			$this->db->trans_begin();
			$id = ($this->input->post('id'))?$this->input->post('id'):0;

			$dataexc = array(
				'status_topup' => ( $_POST['btn_submit'] == 1 ) ? 2 : 3 ,
				'approved_num' => $val->set_value('supplier_id'),
				'approved_by' => $val->set_value('app_by'),
				'approved_date' => date('Y-m-d H:i:s'),
			);

			// updated
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
			/*update record*/
			$this->m_app_topup->update('t_withdrawprincipals', array('t_withdrawprincipals.id' => $id), $dataexc);
			
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}
			else
			{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}

	public function delete()
	{
		$id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
		$toArray = explode(',',$id);
		if($id!=null){
			if($this->m_app_topup->delete_by_id($toArray)){
				echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
			}else{
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
			}
		}else{
			echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
		}
		
	}


}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
