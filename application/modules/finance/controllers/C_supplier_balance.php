<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_supplier_balance extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'finance/C_supplier_balance');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('finance/M_supplier_balance', 'm_supplier_balance');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $balance = $this->m_supplier_balance->get_balance();        
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show(),
            'balance' => $balance,
            
        );
        // echo '<pre>';print_r($balance);die;
        /*load view index*/
        $this->load->view('V_supplier_balance/index', $data);
    }
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
