<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){
  
    $('#form_app_topup').ajaxForm({
      beforeSend: function() {
        achtungShowLoader();  
      },
      uploadProgress: function(event, position, total, percentComplete) {
      },
      complete: function(xhr) {     
        var data=xhr.responseText;
        var jsonResponse = JSON.parse(data);

        if(jsonResponse.status === 200){
          $.achtung({message: jsonResponse.message, timeout:5});
          $('#page-area-content').load('finance/C_app_topup?_=' + (new Date()).getTime());
        }else{
          $.achtung({message: jsonResponse.message, timeout:5});
        }
        achtungHideLoader();
      }
    }); 
})

</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="col-sm-6">
  <div class="widget-box">
    <div class="widget-header widget-header-flat">
      <h4 class="widget-title smaller">
        <i class="ace-icon fa fa-quote-left smaller-80"></i>
        Verifikasi Data Topup
      </h4>
    </div>

    <div class="widget-body">
      <div class="widget-main">

        <div class="row">
          <div class="col-xs-12">
            <blockquote>
              <p class="lighter line-height-125">
                <?php echo isset($value)?$value->principal_name:''?>
              </p>

              <address>
                <strong>No. <?php echo isset($value)?$value->topup_number.' - '.$this->tanggal->formatDateFormDmy($value->deposit_date) :''?></strong>

                <br>
                Total Topup : <?php echo isset($value)?number_format($value->amount):''?>
                <br>
                <i>"<?php $terbilang = new Kuitansi(); echo ucwords($terbilang->terbilang($value->amount))?> Rupiah"</i>
              </address>
              <small>
                a.n <?php echo isset($value)?$value->acc_name:''?> <?php echo isset($value)?$value->bank_name:''?> No. Rek (<?php echo isset($value)?$value->acc_no:''?>)
              </small>
              <br>
              Bukti Transfer : <br>
              <img src="<?php echo PATH_MBR_IMG.$value->attachment?>" alt="" width="100%">

            </blockquote>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<div class="col-sm-6">
  <div class="widget-box">
    <div class="widget-header widget-header-flat">
      <h4 class="widget-title smaller">
        <i class="ace-icon fa fa-quote-left smaller-80"></i>
        Form Approval Topup
      </h4>
    </div>

    <div class="widget-body">
      <div class="widget-main">

        <div class="row">
          <div class="col-xs-12 no-padding">
            <form class="form-horizontal" method="post" id="form_app_topup" action="<?php echo site_url('finance/C_app_topup/process')?>" enctype="multipart/form-data">
              <br>
              <div class="form-group">
                <label class="control-label col-md-3">ID</label>
                <div class="col-md-2">
                  <input name="id" id="id" value="<?php echo isset($value)?$value->deposit_id:0?>"  class="form-control" type="text" readonly>
                </div>
                <label class="control-label col-md-2">Date</label>
                <div class="col-md-3">
                  <input name="date" id="date" value="<?php echo date('d/m/Y') ?>"  class="form-control" type="text" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Approval Number</label>
                <div class="col-md-3">
                  <input name="app_num" id="app_num" value="APP-0001"  class="form-control" type="text" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Approved By</label>
                <div class="col-md-4">
                  <input name="app_by" id="app_by" value="<?php echo $this->session->userdata('user')->fullname ?>"  class="form-control" type="text" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Total Amount</label>
                <div class="col-md-3">
                  <input name="total_amount" id="total_amount" value="<?php echo isset($value)?number_format($value->amount):''?>"  class="form-control" type="text" style="text-align: right">
                </div>
              </div>
              <br>
              <div class="checkbox">
                <label>
                  <input name="pernyataan" type="checkbox" class="ace">
                  <span class="lbl"> Dengan ini menyatakan bahwa saya telah menyetujui dan menerima Topup Saldo dari yang bersangkutan kepada manajemen kami.</span>
                </label>
              </div>
              
              <hr class="separator">
              <div class="form-group">
                <div class="col-md-12 center">
                  <button class="btn btn-xs btn-success" type="submit" name="btn_submit" value="1">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-check-circle"></i> Accept &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </button>
                  <button class="btn btn-xs btn-danger" type="submit" name="btn_submit" value="0">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-times-circle"></i> Reject &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </button>
                </div>
              </div>

            </form>
            <!-- <p>
              <strong> Pemberitahuan ! </strong> 
              <ol>
                <li>Ordered List Item</li>
                <li class="text-primary">.text-primary Ordered List Item</li>
                <li class="text-danger">.text-danger Ordered List Item</li>

                <li class="text-success">
                  <b>.text-success</b>
                  Ordered List Item
                </li>
                <li class="text-warning">.text-warning Ordered List Item</li>
                <li class="text-muted">.text-muted Ordered List Item</li>
              </ol>
            </p> -->
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>


