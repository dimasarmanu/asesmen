<script>
$(document).ready(function(){

	get_summary_transaction();

});


function get_summary_transaction(){

	$.getJSON("finance/C_app_withdraw/get_summary", '', function (response) {
			$('#total_amount').text(formatMoney(response.total_amount));
			$('#total_trans').text(formatMoney(response.total_trans));
			$('#balance').text(formatMoney(response.balance));
	});

}

</script>
<div class="row">
	<div class="col-xs-12">

		<div class="page-header">
			<h1>
				<?php echo $title?>
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<?php echo isset($breadcrumbs)?$breadcrumbs:''?>
				</small>
			</h1>
		</div><!-- /.page-header -->

		<div class="row">
			<div class="pull-left" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: darkorange">
				<span style="font-size: 14px">Balance Saldo</span>
				<h3 style="font-weight: bold; margin-top : 0px"><span id="balance"></span></h3>
			</div>

			<div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: chartreuse">
				<span style="font-size: 14px">Total Transaksi</span>
				<h3 style="font-weight: bold; margin-top : 0px"><span id="total_trans"></span></h3>
			</div>

			<div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: gold">
				<span style="font-size: 14px">Total Withdraw</span>
				<h3 style="font-weight: bold; margin-top : 0px"><span id="total_amount"></span></h3>
			</div>
		</div>
		<hr class="separator">
		<div class="clearfix" style="margin-bottom:-5px">
			<?php //echo $this->authuser->show_button('finance/C_withdraw','C','',1)?>
			<button type="button" class="btn btn-xs btn-primary" onclick="getMenu('finace/C_withdraw/form')" disabled="disabled"><i class="fa fa-money"></i> Withdraw</button>
			<div class="pull-right tableTools-container"></div>
		</div>
	
		<hr class="separator">
		Terms & Conditions
		<hr class="separator">
		<ul>
			<li>Saldo anda tertera adalah jumlah transaksi berhasil terjual etiket yang didistribusikan menggunakan SYSTEM JARTAV ke reseller/agent.</li>
			<li>Anda dapat menarik dana saldo anda sejumlah transaksi etiket yang terjual dikalikan harga dasar yang terdaftar di SYSTEM JATRAV.</li>
			<li>Anda akan dikenakan biaya administrasi transfer sebesar Rp 5.000,- dalam sekali pernarikan dana saldo.</li>
			<li>Saldo anda (-) minus artinya telah dibayarkan kepada anda selaku pengelola tempat wisata atau topup yang dilakukan oleh partner kami.</li>
			<li>Tata cara dan pelaksanaan TOP UP SALDO yg di bayarkan oleh parter kami (JATRAV SYSTEM) kepada anda sebagai pihak pengelola tempat wisata dilakukan secara manual dan didaftarkan jumlahnya oleh operator kami.</>
			<li>Pihak JATRAV SYSTEM hanya bersifat fasilator system dalam melakukan TOP UP dari partner kami ke pihak pengelola tempat wisata, klik <u><b><a href="#" onclick="getMenu('finance/C_transaction_report')">disini</a></b></u> untuk melihat jumlah transaksi dan sisa TOP UP oleh partner kami.</li>
		</ul>
		<hr class="separator">

		<!-- div.dataTables_borderWrap -->
		<div class="row" style="margin-top: -2px">
			<table id="dynamic-table" base-url="finance/C_app_withdraw" url-detail="finance/C_app_withdraw/show_detail" class="table table-bordered table-hover">
				<thead>
					<tr>  
						<th width="50px">No</th>
						<th width="130px">No. Withdraw</th>
						<th width="100px">Tanggal</th>
						<!--<th >Informasi Pengirim</th>-->
						<th >Informasi Penerima</th>
						<th width="150px">Total</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_no_style.js'?>"></script>



