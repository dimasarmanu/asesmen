<script>
$(document).ready(function(){

  //get_summary_transaction();

});


function get_summary_transaction(){

  $.getJSON("finance/C_app_topup/get_summary", '', function (response) {
      $('#total_amount').text(formatMoney(response.total_amount));
      $('#total_trans').text(formatMoney(response.total_trans));
      $('#balance').text(formatMoney(response.balance));
  });

}

</script>
<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->
    <!--
    <div class="row">
      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: darkorange">
        <span style="font-size: 14px">Balance</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="balance"></span></h3>
      </div>

      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: chartreuse">
        <span style="font-size: 14px">Total Transaksi</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_trans"></span></h3>
      </div>

      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: gold">
        <span style="font-size: 14px">Total Topup</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_amount"></span></h3>
      </div>
    </div>
    -->

    <!-- div.dataTables_borderWrap -->
    <div class="row" style="margin-top: -2px">
      <table id="dynamic-table" base-url="finance/C_app_topup" url-detail="finance/C_app_topup/show_detail" class="table table-bordered table-hover">
        <thead>
          <tr>  
            <th width="50px">No</th>
            <th width="130px">No. Topup</th>
            <th width="100px">Tanggal</th>
            <th >Informasi Pengirim</th>
			<th >Informasi Penerima</th>
            <th width="150px">Total</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_no_style.js'?>"></script>



