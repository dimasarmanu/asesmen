<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  
    <!-- PAGE CONTENT BEGINS -->
      <div class="widget-body">
        <div class="widget-main no-padding">

          <div id="accordion" class="accordion-style1 panel-group">

            <?php  foreach($balance as $key=>$row_balance) : ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a class="accordion-toggle <?php echo ($key!=0)?'collapsed':''?>" data-toggle="collapse" data-parent="#accordion" href="#<?php echo 'Collapse_'.$row_balance->principal_id; ?>">
                      <i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                      &nbsp;<?php echo $row_balance->principal_name?>
                    </a>
                  </h4>
                </div>

                <div class="panel-collapse collapse <?php echo ($key!=0)?'':'in'?>" id="<?php echo 'Collapse_'.$row_balance->principal_id; ?>">
                  <div class="panel-body">

                      <blockquote>
                      <p class="lighter line-height-125">
                        <?php echo $row_balance->principal_name?>
                      </p>
                      <small>
                      <?php echo $row_balance->supplier_name?>
                      </small>
                    </blockquote>
                    
                    <div class="col-sm-6">
                      <p style="font-size: 14px"><strong>TOTAL AMOUNT</strong></p>
                      <div class="infobox infobox-green">
                        <div class="infobox-icon">
                          <i class="ace-icon fa fa-money"></i>
                        </div>

                        <div class="infobox-data">
                          <span class="infobox-data-number"><?php echo number_format( $row_balance->amount )?></span>
                          <div class="infobox-content">amount</div>
                        </div>
                      </div>

                      <div class="infobox infobox-blue">
                        <div class="infobox-icon">
                          <i class="ace-icon fa fa-money"></i>
                        </div>

                        <div class="infobox-data">
                          <span class="infobox-data-number"><?php echo number_format( $row_balance->issued_amount )?></span>
                          <div class="infobox-content">issued</div>
                        </div>
                      </div>

                      <div class="infobox infobox-pink">
                        <div class="infobox-icon">
                          <i class="ace-icon fa fa-money"></i>
                        </div>

                        <div class="infobox-data">
                          <span class="infobox-data-number"><?php echo number_format( $row_balance->balance )?></span>
                          <div class="infobox-content">balance</div>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <p style="font-size: 14px"><strong>TOTAL ISSUED</strong></p>

                      <div class="infobox infobox-red">
                        <div class="infobox-icon">
                          <i class="ace-icon fa fa-money"></i>
                        </div>

                        <div class="infobox-data">
                          <span class="infobox-data-number"><?php echo number_format( $row_balance->weekday )?></span>
                          <div class="infobox-content">weekday</div>
                        </div>
                      </div>

                      <div class="infobox infobox-orange2">
                        <div class="infobox-icon">
                          <i class="ace-icon fa fa-money"></i>
                        </div>

                        <div class="infobox-data">
                          <span class="infobox-data-number"><?php echo number_format( $row_balance->weekend )?></span>
                          <div class="infobox-content">weekend</div>
                        </div>
                      </div>

                      <div class="infobox infobox-blue2">
                        <div class="infobox-icon">
                          <i class="ace-icon fa fa-money"></i>
                        </div>

                        <div class="infobox-data">
                          <span class="infobox-data-number"><?php echo number_format( $row_balance->highseason )?></span>
                          <div class="infobox-content">highseason</div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>

              </div>
            <?php endforeach; ?>

          </div>
          
        </div>
      </div>
    
    <!-- PAGE CONTENT ENDS -->
</div><!-- /.row -->


