<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_location extends MX_Controller
{

    /*function constructor*/
    function __construct()
    {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'pisikotes/C_location');
        /*session redirect login if not login*/
        if ($this->session->userdata('logged') != TRUE) {
            echo 'Session Expired !';
            exit;
        }
        /*load model*/
        $this->load->model('M_location', 'M_location');

        /*load library*/
        $this->load->library('bcrypt');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this))) ? $this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
    }

    public function index()
    {
        //echo '<pre>';print_r($this->session->all_userdata());
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        /*load view index*/
        $this->load->view('V_locations/index', $data);
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->M_location->get_datatables();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center">
                        <label class="pos-rel">
                            <input type="checkbox" class="ace" name="selected_id[]" value="' . $row_list->id . '"/>
                            <span class="lbl"></span>
                        </label>
                      </div>';
            $row[] = '<div class="center">
                        ' . $this->authuser->show_button('pisikotes/C_location', 'R', $row_list->id, 2) . '
                        ' . $this->authuser->show_button('pisikotes/C_location', 'U', $row_list->id, 2) . '
                        ' . $this->authuser->show_button('pisikotes/C_location', 'D', $row_list->id, 2) . '
                      </div>';
            $row[] = '<div class="center">' . $row_list->id . '</div>';
            $row[] = strtoupper($row_list->location_name);
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_location->count_all(),
            "recordsFiltered" => $this->M_location->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function form($id = '')
    {
        /*if id is not null then will show form edit*/
        if ($id != '') {
            /*breadcrumbs for edit*/
            $this->breadcrumbs->push('Edit ' . strtolower($this->title) . '', 'M_location/' . strtolower(get_class($this)) . '/' . __FUNCTION__ . '/' . $id);
            /*get value by id*/
            $data['value'] = $this->M_location->get_by_id($id);
            /*initialize flag for form*/
            $data['flag'] = "update";
        } else {
            /*breadcrumbs for create or add row*/
            $this->breadcrumbs->push('Add ' . strtolower($this->title) . '', 'M_location/' . strtolower(get_class($this)) . '/form');
            /*initialize flag for form add*/
            $data['flag'] = "create";
        }
        /*title header*/
        $data['title'] = $this->title;
        /*show breadcrumbs*/
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_locations/form', $data);
    }

    /*function for view data only*/
    public function show($id)
    {
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('View ' . strtolower($this->title) . '', 'M_location/' . strtolower(get_class($this)) . '/' . __FUNCTION__ . '/' . $id);
        /*define data variabel*/
        $data['value'] = $this->M_location->get_by_id($id);
        $data['title'] = $this->title;
        $data['flag'] = "read";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_locations/form', $data);
    }

    public function process()
    {
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('location_name', 'LocationName', 'trim|required');


        $val->set_message('required', "Silahkan isi field \"%s\"");


        if ($val->run() == FALSE) {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        } else {
            $this->db->trans_begin();
            $id = ($this->input->post('id')) ? $this->regex->_genRegex($this->input->post('id'), 'RGXINT') : 0;

            $dataexc = array(
                'location_name' => $this->regex->_genRegex($val->set_value('location_name'), 'RGXQSL'),
            );
            //print_r($dataexc);die;
            if ($id == 0) {
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' => $this->regex->_genRegex($this->session->userdata('user')->user_id, 'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname, 'RGXQSL')));
                /*save post data*/
                $newId = $this->M_location->save($dataexc);
                $this->logs->save('tmp_location', $newId, 'insert new record', json_encode($dataexc), 'id');
            } else {
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' => $this->regex->_genRegex($this->session->userdata('user')->user_id, 'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname, 'RGXQSL')));
                /*update record*/
                $this->M_location->update(array('id' => $id), $dataexc);
                $newId = $id;
                $this->logs->save('tmp_location', $newId, 'update record', json_encode($dataexc), 'id');
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            } else {
                $this->db->trans_commit();
                //redirect(base_url().'login/logout');
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
            }
        }
    }
}


/* End of file example.php */
/* Location: ./application/modules/example/controllers/example.php */
