<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>  
          <th width="30px" class="center"></th>
          <th>Kode Booking</th>
          <th>Nama Agent</th>
          <th>Tanggal</th>
          <th>Principal</th>
          <th>Nama Pemesan</th>
          <th>Tanggal Kedatangan</th>
          <th>Produk</th>
          <th>Hari Kunjungan</th>
          <th>Group</th>
          <th>Jml</th>
          <th>Modal</th>
          <th>Total Modal</th>
          <th>Jual</th>
          <th>Total</th>
          <th>Margin</th>
        </tr>
    </thead>

    <tbody>
        <?php 
            $count = count($value); 
            if( $count > 0 ) :
            $no=0; foreach($value as $row_dt) : $no++; 
        ?>
            <tr>
                <td align="center"><?php echo $no?></td>
                <td> <?php echo $row_dt->booking_code?> </td>
                <td> <?php echo $row_dt->user_name?> </td>
                <td> <?php echo $this->tanggal->formatDateFormDmy($row_dt->trans_time)?> </td>
                <td> <?php echo $row_dt->principal_name?> </td>
                <td> <?php echo $row_dt->name_to?> </td>
                <td> <?php echo $this->tanggal->formatDateFormDmy($row_dt->visit_date)?> </td>
                <td> <?php echo $row_dt->partner_productname?> </td>
                <td> <?php echo $row_dt->datepricetype_id?> </td>
                <td> <?php echo $row_dt->is_group?> </td>
                <td> <?php echo $row_dt->qty?> </td>
                <td> <?php echo $row_dt->price_principal?> </td>
                <td> <?php echo $row_dt->total_priceprincipal?> </td>
                <td> <?php echo $row_dt->price?> </td>
                <td> <?php echo $row_dt->total?> </td>
                <td> <?php 
                    $margin = ( $row_dt->total - $row_dt->total_priceprincipal ); 
                    echo $margin;
                ?> </td>
            </tr>
        <?php 
            endforeach; 
            else: echo '<tr><td colspan="8"><b>-Tidak ada data ditemukan-</b><td></tr>';
            endif;
        ?>
    </tbody>
</table>