<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_agent_balance extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
		/*get data from session*/
		$this->agent = $this->session->userdata('account')->id;
	}

	public function get_balance(){
		// if( $this->session->userdata('user')->user_id != 1 ){
		// 	$this->db->where( array('supplier_id' => $this->supplier ) );
		// }
		// filter by session principal
		$this->db_mbr->where('account_id', $this->agent );
		
		$this->db_mbr->select('v_deposits.*');
		return $this->db_mbr->get('v_deposits')->row();
	}


}
