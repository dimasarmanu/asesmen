<script>
$(document).ready(function(){
	$('select[name="company_id"]').change(function () {
		$('#accounttype_id option').remove();
		$('#principal_id option').remove();
		
		//if ($(this).val()) {
			$.getJSON("Templates/References/getAgentTypeByCompany/" + $(this).val(), '', function (data) {
				$('<option value="">-Pilih Tipe Agent-</option>').appendTo($('#accounttype_id'));
				$.each(data, function (i, o) {
					$('<option value="' + o.id + '">' + o.accounttype_name + '</option>').appendTo($('#accounttype_id'));
				});
			});
			$.getJSON("<?php echo site_url('Templates/References/getPrincipalByCompany') ?>/" + $(this).val(), '', function (data) {
                $('<option value="">-Pilih Principal-</option>').appendTo($('#principal_id'));
                $.each(data, function (i, o) {
                    $('<option value="' + o.id + '">' + o.principal_name + '</option>').appendTo($('#principal_id'));
                });

            });
		//} 
	});
});
</script>
<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

	 <form class="form-horizontal" method="post" id="form_search" action="mbl_reseller/C_price_setting/find_data">
		<b>Pencarian Data Harga</b><br><br>
		<div class="form-group">
			<label class="control-label col-md-2">Distributor</label>
			<div class="col-md-3">
			  <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), '' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
			</div>
			<label class="control-label col-md-2">Principal</label>
            <div class="col-md-3">
              <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_principals', 'where'=>array(), 'id'=>'id', 'name' => 'principal_name'), '' ,'principal_id','principal_id','chosen-slect form-control','','', 'db_mbr');?>
            </div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Agent</label>
            <div class="col-md-3">
              <select name="accounttype_id" id="accounttype_id" class="form-control">
                <option value="0">-Pilih Tipe Agent</option>
              </select>
            </div>
		</div>
		<hr>
		<div class="form-group">
			<label class="control-label col-md-2">Price Type</label>
			<div class="col-md-3">
				<?php echo $this->master->custom_selection_no_database(array('DOMESTIK-DEWASA','DOMESTIK-ANAK','MANCANEGARA-DEWASA', 'MANCANEGARA-ANAK'),'','price_type','price_type','chosen-slect form-control','','', 'db_mbr');?>
            </div>
			<label class="control-label col-md-2">Product Type</label>
			<div class="col-md-3">
				<?php echo $this->master->custom_selection_no_database(array('B2B', 'B2C'),'','product_type','product_type','chosen-slect form-control','','', 'db_mbr');?>
            </div>
		</div>
		
		<div class="form-group">
          <label class="control-label col-md-2 ">&nbsp;</label>
          <div class="col-md-10" style="margin-left: 6px">
            <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
              <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
              Search
            </a>
            <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
              <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Reset
            </a>
          </div>
        </div>
		
		<div class="form-group">
		  
		</div>
		
		<div class="clearfix" style="margin-bottom:-5px">
		  <?php echo $this->authuser->show_button('mbl_reseller/C_price_setting','C','',1)?>
		  <?php echo $this->authuser->show_button('mbl_reseller/C_price_setting','D','',5)?>
		  <div class="pull-right tableTools-container"></div>
		</div>
		<hr class="separator">
		<!-- div.table-responsive -->

		<!-- div.dataTables_borderWrap -->
		<div style="margin-top:-15px">
		  <table id="dynamic-table" base-url="mbl_reseller/C_price_setting" url-detail="mbl_reseller/C_price_setting/show_detail" class="table table-striped table-bordered table-hover">
		   <thead>
			<tr>  
			  <th width="30px" class="center"></th>
			  <th width="40px" class="center"></th>
			  <!--<th width="40px" class="center"></th>-->
			  <th width="40px"></th>
			  <!-- <th width="70px">ID</th> -->
			  <th>Information</th>
			  <th>Agent Type</th>
			  <th>Date Type</th>
			  <th>Basic Price</th>
			  <th>Selling Price</th>
			  <th>Start Date</th>
			  <th>Expired Date</th>
			  <th>Status</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
		</div>
	 </form>
  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_with_detail.js'?>"></script>



