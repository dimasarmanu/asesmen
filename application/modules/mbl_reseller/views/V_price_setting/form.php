<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
	jQuery(function($) {

		$('.date-picker').datepicker({
				autoclose: true,
				todayHighlight: true
			})
			//show datepicker when clicking on the icon
			.next().on(ace.click_event, function() {
				$(this).prev().focus();
			});
	});

	$(document).ready(function() {

		$('#form_price_setting').ajaxForm({
			beforeSend: function() {
				achtungShowLoader();
			},
			uploadProgress: function(event, position, total, percentComplete) {},
			complete: function(xhr) {
				var data = xhr.responseText;
				var jsonResponse = JSON.parse(data);

				if (jsonResponse.status === 200) {
					$.achtung({
						message: jsonResponse.message,
						timeout: 5
					});
					$('#page-area-content').load('mbl_reseller/C_price_setting?_=' + (new Date()).getTime());
				} else {
					$.achtung({
						message: jsonResponse.message,
						timeout: 5
					});
				}
				achtungHideLoader();
			}
		});

		$('select[name="company_id"]').change(function() {
			$('#accounttype_id option').remove();
			$('#product_id option').remove();
			//$('#principal_id option').remove();
			if ($(this).val()) {
				$.getJSON("<?php echo site_url('Templates/References/getAgentTypeByCompany') ?>/" + $(this).val(), '', function(data) {
					$('<option value="">-Pilih Agent-</option>').appendTo($('#accounttype_id'));
					$.each(data, function(i, o) {
						$('<option value="' + o.id + '">' + o.accounttype_name + '</option>').appendTo($('#accounttype_id'));
					});

				});

				
			}
		});

		$('select[name="principal_id"]').change(function() {
			$('#product_id option').remove();
			if ($(this).val()) {
				$.getJSON("<?php echo site_url('Templates/References/getProdukByPrincipal') ?>/" + $(this).val(), '', function(data) {
					$('<option value="">-Pilih Product-</option>').appendTo($('#product_id'));
					$.each(data, function(i, o) {
						$('<option value="' + o.id + '">' + o.partner_productname + '</option>').appendTo($('#product_id'));
					});

				});
			}
		});


	})

	function set_non_active(label, flag) {
		if (flag == 'active') {
			$('#' + label + ' .form-price').attr('readonly', false);
		} else {
			$('#' + label + ' .form-price').attr('readonly', true);
		}
	}
</script>

<div class="page-header">
	<h1>
		<?php echo $title?>
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			<?php echo $breadcrumbs?>
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="widget-body">
			<div class="widget-main no-padding">
				<form class="form-horizontal" method="post" id="form_price_setting" action="<?php echo site_url('mbl_reseller/C_price_setting/process')?>" enctype="multipart/form-data">
					<div class="form-group">
						<label class="control-label col-md-2">Distributor</label>
						<div class="col-md-3">
						<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), isset($value['dt_header'])?$value['dt_header']->company_id:'' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
							<?php //echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), '' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
						</div>
						<label class="control-label col-md-2">Principal</label>
						<div class="col-md-3">
							<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_principals', 'where'=>array('is_active'=>'Y'), 'id'=>'id', 'name' => 'principal_name'),isset($value['dt_header'])?$value['dt_header']->principal_id:'','principal_id','principal_id','chosen-slect form-control',($flag=='read')?'readonly':'','', 'db_mbr');?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Agent Type</label>
						<div class="col-md-3">
							<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_accounttypes', 'where'=>array('is_active'=>'Y', 'child_company_id' => NULL), 'id'=>'id', 'name' => 'accounttype_name'),isset($value['dt_header'])?$value['dt_header']->accounttype_id:'','accounttype_id','accounttype_id','chosen-slect form-control',($flag=='read')?'readonly':'','', 'db_mbr');?>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label class="control-label col-md-2">Product</label>
						<div class="col-md-3">
							<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_products', 'where'=>array('is_active'=>'Y'), 'id'=>'id', 'name' => 'partner_productname'),isset($value['dt_header'])?$value['dt_header']->product_id:'','product_id','product_id','chosen-slect form-control',($flag=='read')?'readonly':'','', 'db_mbr');?>
						</div>
						<label class="control-label col-md-2">Price Type</label>
						<div class="col-md-3">
							<?php // echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), ($this->session->userdata('user')->role != 1)?$this->session->userdata('user')->company_id:'' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
							<?php echo $this->master->custom_selection_no_database(array('DOMESTIK-DEWASA','DOMESTIK-ANAK','MANCANEGARA-DEWASA', 'MANCANEGARA-ANAK'),isset($value['dt_header']->price_type)?$value['dt_header']->price_type:'','price_type','price_type','chosen-slect form-control',($flag=='read')?'readonly':'','', 'db_mbr');?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">Product Type</label>
						<div class="col-md-3">
							<?php // echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), ($this->session->userdata('user')->role != 1)?$this->session->userdata('user')->company_id:'' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
							<?php echo $this->master->custom_selection_no_database(array('B2B', 'B2C'),isset($value['dt_header']->product_type)?$value['dt_header']->product_type:'','product_type','product_type','chosen-slect form-control',($flag=='read')?'readonly':'','', 'db_mbr');?>
						</div>
					</div>
					<h3 class="header smaller lighter green">
						<i class="ace-icon fa fa-cogs"></i>
						Setting Data Type
					</h3>
					<div class="row">
						<?php
					$color = array('orange','blue','red');
					shuffle($color);
					foreach($price_type as $key=>$row_price_type) :
					  $is_disabled = isset($value[$row_price_type->value]) ? ($value[$row_price_type->value]->is_active == 'Y') ? '' : 'readonly' : '';

				  ?>

						<div class="col-xs-12 col-sm-4 widget-container-col ui-sortable">
							<div class="widget-box widget-color-<?php echo array_shift($color)?> ui-sortable-handle">
								<!-- #section:custom/widget-box.options.collapsed -->
								<div class="widget-header widget-header-small">
									<h6 class="widget-title">
										<i class="ace-icon fa fa-sort"></i>
										<?php echo $row_price_type->label ?>
									</h6>
								</div>

								<!-- /section:custom/widget-box.options.collapsed -->
								<div class="widget-body" id="<?php echo $row_price_type->label ?>">
									<div class="widget-main">
										<div class="form-group">
											<label class="control-label col-md-4">ID</label>
											<div class="col-md-4">
												<input name="price_id[<?php echo $row_price_type->value?>]" id="price_id" value="<?php echo isset($value[$row_price_type->value])?$value[$row_price_type->value]->id:0?>" placeholder="Auto" class="form-control" type="text" readonly>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-4">Type</label>
											<div class="col-md-6">
												<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'global_parameter', 'where'=>array('flag'=>'price_type'), 'id'=>'value', 'name' => 'label'),$row_price_type->value,'datepricetype_id[]','datepricetype_id','chosen-slect form-control','readonly', '' , 'db_mbr');?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-4">Basic Price</label>
											<div class="col-md-6">
												<input name="price_principal[<?php echo $row_price_type->label?>]" id="price_principal" value="<?php echo isset($value[$row_price_type->value])?$value[$row_price_type->value]->price_principal:''?>" placeholder="" style="text-align: right" class="form-control form-price" type="text" <?php echo ($flag=='read')?'readonly':''?> <?php echo $is_disabled?>>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-4">Selling Price</label>
											<div class="col-md-6">
												<input name="price[<?php echo $row_price_type->label?>]" id="price" value="<?php echo isset($value[$row_price_type->value])?$value[$row_price_type->value]->price:''?>" placeholder="" style="text-align: right" class="form-control form-price" type="text" <?php echo ($flag=='read')?'readonly':''?> <?php echo $is_disabled?>>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-4">Publish</label>
											<div class="col-md-6">
												<input name="publish[<?php echo $row_price_type->label?>]" id="publish" value="<?php echo isset($value[$row_price_type->value])?$value[$row_price_type->value]->price_publish:''?>" placeholder="" style="text-align: right" class="form-control form-price" type="text" <?php echo ($flag=='read')?'readonly':''?> <?php echo $is_disabled?>>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php endforeach;?>
					</div>
					<div class="form-actions center" style="margin-top: 10px">

						<a onclick="getMenu('mbl_reseller/C_price_setting')" href="#" class="btn btn-sm btn-success">
							<i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
							Kembali ke daftar
						</a>
						<?php if($flag != 'read'):?>
						<button type="reset" id="btnReset" class="btn btn-sm btn-danger">
							<i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
							Reset
						</button>
						<button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
							<i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
							Submit
						</button>
						<?php endif; ?>
					</div>

				</form>
			</div>
		</div>

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->