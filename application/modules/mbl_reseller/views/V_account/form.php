<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){
  
    $('#form_product').ajaxForm({
      beforeSend: function() {
        achtungShowLoader();  
      },
      uploadProgress: function(event, position, total, percentComplete) {
      },
      complete: function(xhr) {     
        var data=xhr.responseText;
        var jsonResponse = JSON.parse(data);

        if(jsonResponse.status === 200){
          $.achtung({message: jsonResponse.message, timeout:5});
          $('#page-area-content').load('mbl_reseller/C_account?_=' + (new Date()).getTime());
        }else{
          $.achtung({message: jsonResponse.message, timeout:5});
        }
        achtungHideLoader();
      }
    }); 
})

</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
            <form class="form-horizontal" method="post" id="form_product" action="<?php echo site_url('mbl_reseller/C_account/process')?>" enctype="multipart/form-data">
                <br>

                <!-- hidden form -->
                <input type="hidden" name="password_hidden" value="<?php echo isset($value)?$value->password:0?>">
                <input type="hidden" name="user_id_temp" value="<?php echo isset($value->user_id_temp)?$value->user_id_temp:0?>">

                <p><b><i class="fa fa-key"></i> ACCOUNT SETTING</b></p>
                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Email</label>
                  <div class="col-md-3">
                    <input name="email" id="email" value="<?php echo isset($value)?$value->email:''?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                  <label class="control-label col-md-1">Password</label>
                  <div class="col-md-3">
                    <input name="password" id="password" value="<?php echo isset($value)?$value->password:''?>" placeholder="" class="form-control" type="password" disabled >
                  </div>
                </div>
                <br>
                <p><b><i class="fa fa-group"></i> MEMBER DATA</b></p>
                <div class="form-group">
                  <label class="control-label col-md-2">Fullname</label>
                  <div class="col-md-3">
                    <input name="user_name" id="user_name" value="<?php echo isset($value)?$value->user_name:''?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Address</label>
                  <div class="col-md-4">
                    <textarea name="address" id="address" cols="30" rows="10" class="form-control" style="height: 50px !important"><?php echo isset($value)?$value->address:''?></textarea>
                  </div>
                </div>

                <div class="form-group" style="margin-top: 5px">
                  <label class="control-label col-md-2">Phone</label>
                  <div class="col-md-2">
                    <input name="phone" id="phone" value="<?php echo isset($value)?$value->phone:''?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Distributor</label>
                  <div class="col-md-3">
					  <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), isset($value)?$value->company_id:'' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
					</div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Agent Type</label>
                  <div class="col-md-3">
                    <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_accounttypes', 'where'=>array('is_active'=>'Y'), 'id'=>'id', 'name' => 'accounttype_name'),isset($value)?$value->accounttype_id:'','accounttype_id','accounttype_id','chosen-slect form-control',($flag=='read')?'readonly':'','', 'db_mbr');?>
                  </div>
                </div>

                <br>
                <p><b><i class="fa fa-file"></i> DOCUMENT UPLOAD</b></p>

                <div class="form-group">
                  <label class="control-label col-md-2">NPWP</label>
                  <div class="col-md-3">
                    <input name="npwp" id="npwp" value="<?php echo isset($value)?$value->npwp:''?>" placeholder="" class="form-control" type="file" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>
                <?php if(isset($value->npwp) AND $value->npwp != '') :?>
                <div class="form-group">
                  <label class="col-md-2">&nbsp;</label>
                  <div class="col-md-3">
                    <a href="<?php echo PATH_MBR.'attachment/'.$value->npwp.''?>" target="_blank"><img width="100px" src="<?php echo PATH_MBR.'attachment/'.$value->npwp.''?>" alt=""></a>
                  </div>
                </div>
                <?php endif; ?>

                <div class="form-group">
                  <label class="control-label col-md-2">SIUP</label>
                  <div class="col-md-3">
                    <input name="siup" id="siup" value="<?php echo isset($value)?$value->siup:''?>" placeholder="" class="form-control" type="file" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>
                <?php if(isset($value->siup) AND $value->siup != '') :?>
                <div class="form-group">
                  <label class="col-md-2">&nbsp;</label>
                  <div class="col-md-3">
                    <a href="<?php echo PATH_MBR.'attachment/'.$value->siup.''?>" target="_blank"><img width="100px" src="<?php echo PATH_MBR.'attachment/'.$value->siup.''?>" alt=""></a>
                  </div>
                </div>
                <?php endif; ?>

                <div class="form-group">
                  <label class="control-label col-md-2">NIB</label>
                  <div class="col-md-3">
                    <input name="nib" id="nib" value="<?php echo isset($value)?$value->nib:''?>" placeholder="" class="form-control" type="file" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>
                <?php if(isset($value->nib) AND $value->nib != '') :?>
                <div class="form-group">
                  <label class="col-md-2">&nbsp;</label>
                  <div class="col-md-3">
                    <a href="<?php echo PATH_MBR.'attachment/'.$value->nib.''?>" target="_blank"><img width="100px" src="<?php echo PATH_MBR.'attachment/'.$value->nib.''?>" alt=""></a>
                  </div>
                </div>
                <?php endif; ?>

                <div class="form-group">
                  <label class="control-label col-md-2">Office Photo</label>
                  <div class="col-md-3">
                    <input name="photo_office" id="photo_office" value="<?php echo isset($value)?$value->photo_office:''?>" placeholder="" class="form-control" type="file" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>
                <?php if(isset($value->photo_office) AND $value->photo_office != '') :?>
                <div class="form-group">
                  <label class="col-md-2">&nbsp;</label>
                  <div class="col-md-3">
                    <a href="<?php echo PATH_MBR.'attachment/'.$value->photo_office.''?>" target="_blank"><img width="100px" src="<?php echo PATH_MBR.'attachment/'.$value->photo_office.''?>" alt=""></a>
                  </div>
                </div>
                <?php endif; ?>

                <hr>

                <div class="form-group">
                  <label class="control-label col-md-2">Is active?</label>
                  <div class="col-md-2">
                    <div class="radio">
                          <label>
                            <input name="is_active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->is_active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?> <?php echo ($flag=='read')?'readonly':''?> />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="is_active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->is_active == 'N') ? 'checked="checked"' : '' : ''; ?> <?php echo ($flag=='read')?'readonly':''?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Last update</label>
                  <div class="col-md-8" style="padding-top:8px;font-size:11px">
                      <i class="fa fa-calendar"></i> <?php echo isset($value->updated_date)?$this->tanggal->formatDateTime($value->updated_date):isset($value)?$this->tanggal->formatDateTime($value->created_date):date('d-M-Y H:i:s')?> - 
                      by : <i class="fa fa-user"></i> <?php echo isset($value->updated_by)?$value->updated_by:isset($value->created_by)?$value->created_by:$this->session->userdata('user')->username?>
                  </div>
                </div>


                <div class="form-actions center">

                  <a onclick="getMenu('mbl_reseller/C_account')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


