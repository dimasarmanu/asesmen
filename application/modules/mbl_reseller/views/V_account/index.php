<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->
    
    <form class="form-horizontal" method="post" id="form_search" action="mbl_reseller/C_account/find_data">
    <b>Pencarian Data Member</b><br><br>
    <div class="form-group">
        <label class="control-label col-md-2">Distributor</label>
        <div class="col-md-3">
          <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), ($this->session->userdata('user')->role != 1)?$this->session->userdata('user')->company_id:'' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
        </div>
        <div class="col-md-7" style="margin-left: -10px">
          <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
            <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
            Search
          </a>
          <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
            <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
            Reset
          </a>
          <a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
            <i class="fa fa-file-word-o bigger-110"></i>
            Export Excel
          </a>
        </div>
    </div>
    
    <div class="clearfix" style="margin-bottom:-5px">
      <?php echo $this->authuser->show_button('mbl_reseller/C_account','C','',1)?>
      <?php echo $this->authuser->show_button('mbl_reseller/C_account','D','',5)?>
      <div class="pull-right tableTools-container"></div>
    </div>
    <hr class="separator">
    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <div style="margin-top:-16px">
      <table id="dynamic-table" base-url="mbl_reseller/C_account" url-detail="mbl_reseller/C_account/show_detail" class="table table-striped table-bordered table-hover">
        <thead>
          <tr>  
            <!--<th width="30px" class="center"></th>
             <th width="40px" class="center"></th> 
            <th width="40px" class="center"></th>-->
            <th width="40px"></th>
            <th width="70px">ID</th>
            <th>Email</th>
            <th>Username</th>
            <th>Phone</th>
            <th>Type</th>
            <th>Company</th>
            <!-- <th>Gmail Token</th> -->
            <th>Status</th>
            <th>Verify</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

    </form>

  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable.js'?>"></script>



