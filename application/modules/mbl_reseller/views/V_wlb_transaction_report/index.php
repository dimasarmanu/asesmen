<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){

  get_summary_transaction();

  oTable = $('#dynamic-table').DataTable({ 
          
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "ordering": true,
    "searching": true,
    "bPaginate": true,
    "bInfo": true,
    "scrollY": "500px",
    // "scrollX": "500px",
    "pageLength": 50,
    "ajax": {
        "url": $('#dynamic-table').attr('base-url'),
        "type": "POST"
    },

  });

  $('#btn_search_data').click(function (e) {
    
      e.preventDefault();
      $.ajax({
      url: 'mbl_reseller/C_wlb_transaction_report/find_data',
      type: "post",
      data: $('#form_search').serialize(),
      dataType: "json",
      beforeSend: function() {
        achtungShowLoader();  
      },
      success: function(data) {
        achtungHideLoader();
        find_data_reload(data);
      }
    });
  });

  $('#btn_reset_data').click(function (e) {
      e.preventDefault();
      oTable.ajax.url('mbl_reseller/C_wlb_transaction_report/get_data').load();
      get_summary_transaction();
      $('#form_search')[0].reset();
  });

  $('#btn_export_excel').click(function (e) {
      e.preventDefault();
      $.ajax({
        url: 'mbl_reseller/C_wlb_transaction_report/find_data',
        type: "post",
        data: $('#form_search').serialize(),
        dataType: "json",
        beforeSend: function() {
          achtungShowLoader();  
        },
        success: function(data) {
          achtungHideLoader();
          export_excel(data);
        }
      });

    });


});


function find_data_reload(result){
  get_summary_transaction();
  oTable.ajax.url('mbl_reseller/C_wlb_transaction_report/get_data?'+result.data).load();

}

function export_excel(result){

  window.open('mbl_reseller/C_wlb_transaction_report/export_excel?'+result.data+'','_blank');  

}

$('select[name="principal_id"]').change(function () {
    if ($(this).val()) {
        $.getJSON("Templates/References/getProdukByPrincipal/" + $(this).val(), '', function (data) {
            $('#product option').remove();
            $('<option value="">-Pilih Produk-</option>').appendTo($('#product'));
            $.each(data, function (i, o) {
                $('<option value="' + o.id + '">' + o.partner_productname + '</option>').appendTo($('#product'));
            });

        });
    } else {
        $('#product option').remove()
    }
});

$('select[name="company_id"]').change(function () {
    if ($(this).val()) {
      $.getJSON("Templates/References/getAgentTypeByChildCompany/" + $(this).val(), '', function (data) {
        $('#accounttype_id option').remove();
        $('<option value="">-Pilih Tipe Agent-</option>').appendTo($('#accounttype_id'));
        $.each(data, function (i, o) {
          $('<option value="' + o.id + '">' + o.accounttype_name + '</option>').appendTo($('#accounttype_id'));
        });
      });

      $.getJSON("Templates/References/getAgentByCompany/" + $(this).val(), '', function (data) {
        $('#account_id option').remove();
        $('<option value="">-Pilih Agent-</option>').appendTo($('#account_id'));
        $.each(data, function (i, o) {
          $('<option value="' + o.id + '">' + o.user_name + '</option>').appendTo($('#account_id'));
        });
      });

    } else {
        $('#accounttype_id option').remove()
        $('#account_id option').remove()
    }
});


function get_summary_transaction(){

$.ajax({
  url: 'mbl_reseller/C_wlb_transaction_report/find_data',
  type: "post",
  data: $('#form_search').serialize(),
  dataType: "json",
  success: function(result) {
    $.getJSON("mbl_reseller/C_wlb_transaction_report/get_summary_transaction?" +result.data, '', function (response) {
        $('#total_modal').text(formatMoney(response.total_modal));
        $('#total_jual').text(formatMoney(response.total_jual));
        $('#total_margin').text(formatMoney(response.total_margin));
    });
  }
});

}

</script>

<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

    <form class="form-horizontal" method="post" id="form_search" action="#">

    <div class="row">
      <div class="col-md-12">
        <center>
          <h4>FORM PENCARIAN DATA TRANSAKSI<br>
            <small style="font-size:12px"> Silahkan lakukan pencarian data dengan parameter dibawah ini </small>
          </h4>
        </center>
        <div class="form-group">
          <label class="control-label col-md-2">Distributor</label>
          <div class="col-md-3">
            <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=>array('is_active' => 'Y', 'parent_company' => 2), 'id'=>'id', 'name' => 'name'),'','company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
          </div>
          <label class="control-label col-md-1">Tipe Agent</label>
          <div class="col-md-2">
            <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_accounttypes', 'where'=>array(), 'id'=>'id', 'name' => 'accounttype_name'), $this->session->userdata('user')->relation_id ,'accounttype_id','accounttype_id','chosen-slect form-control','','', 'db_mbr');?>
          </div>
          <label class="control-label col-md-1">Name</label>
            <div class="col-md-2">
              <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_accounts', 'where'=>array(), 'id'=>'id', 'name' => 'user_name'), '' ,'account_id','account_id','chosen-slect form-control','','', 'db_mbr');?>
            </div>
        </div>
        
        <p><b>PENCARIAN BERDASARKAN WAKTU TRANSAKSI</b></p>
        <div class="form-group">
            <label class="control-label col-md-2">Transaksi Bulan</label>
            <div class="col-md-2">
              <?php echo $this->master->get_bulan('' , 'from_month', 'from_month', 'form-control', '','') ?>
            </div>
            <label class="control-label col-md-1">s/d Bulan</label>
            <div class="col-md-2">
              <?php echo $this->master->get_bulan('' , 'to_month', 'to_month', 'form-control', '','') ?>
            </div>
            <label class="control-label col-md-1">Tahun</label>
            <div class="col-md-2">
              <?php echo $this->master->get_tahun('' , 'year', 'year', 'form-control', '', '') ?>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-2">Tanggal</label>
            <div class="col-md-2">
              <div class="input-group">
                <input class="form-control date-picker" name="from_tgl" id="from_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                <span class="input-group-addon">
                  <i class="fa fa-calendar bigger-110"></i>
                </span>
              </div>
            </div>

            <label class="control-label col-md-1">s/d Tanggal</label>
            <div class="col-md-2">
              <div class="input-group">
                <input class="form-control date-picker" name="to_tgl" id="to_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                <span class="input-group-addon">
                  <i class="fa fa-calendar bigger-110"></i>
                </span>
              </div>
            </div>
        </div>
        <p><b>PENCARIAN BERDASARKAN PRINCIPAL</b></p>
        <div class="form-group">
            <label class="control-label col-md-2">Principal</label>
            <div class="col-md-2">
              <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_principals', 'where'=>array(), 'id'=>'id', 'name' => 'principal_name'),'','principal_id','principal_id','chosen-slect form-control','','', 'db_mbr');?>
            </div>
            <label class="control-label col-md-1">Produk</label>
            <div class="col-md-2">
              <select name="product" id="product" class="form-control">
                <option value="0">-Silahkan Pilih</option>
              </select>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-2 ">&nbsp;</label>
          <div class="col-md-10" style="margin-left: 6px">
            <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
              <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
              Search
            </a>
            <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
              <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Reset
            </a>
            <a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
              <i class="fa fa-file-word-o bigger-110"></i>
              Export Excel
            </a>
          </div>
        </div>

      </div>
    </div>
    <hr class="separator">
    <!-- div.table-responsive -->

    <div class="row" style="margin-top: -16px;">
      <div class="pull-left"><h3>Riwayat Mutasi</h3></div>

      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: chartreuse">
        <span style="font-size: 14px">Total Margin</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_margin"></span></h3>
      </div>

      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: gold">
        <span style="font-size: 14px">Total Modal</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_modal"></span></h3>
      </div>

      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: darkorange">
        <span style="font-size: 14px">Total Penjualan</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_jual"></span></h3>
      </div>

    </div>

    <!-- div.dataTables_borderWrap -->
    <div class="row" style="margin-top: -2px">
      <table id="dynamic-table" base-url="mbl_reseller/C_wlb_transaction_report/get_data" url-detail="mbl_reseller/C_wlb_transaction_report/show_detail" class="table table-striped table-bordered table-hover">
       <thead>
        <tr>  
          <th width="30px" class="center"></th>
          <th>Booking</th>
          <!-- <th>Nama Agent</th>
          <th>Tanggal</th> -->
          <th>Produk</th>
          <!-- <th>Principal</th> -->
          <th>Nama Pemesan</th>
          <th>Kunjungan</th>
          <!-- <th>Hari Kunjungan</th> -->
          <th>Group</th>
          <th>Jml Tiket</th>
          <th>Modal</th>
          <th>Total Modal</th>
          <th>Jual</th>
          <th>Total</th>
          <th>Margin</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    </div>

    </form>

  </div><!-- /.col -->
</div><!-- /.row -->





