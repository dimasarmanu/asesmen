<div class="widget-main">
  <b><u>PERBANDINGAN HARGA <?php //echo strtoupper($dt_price->company_name)?></u></b><br><br>
  <table class="table table-bordered" style="width: 80% !important">
    <tr>
      <th rowspan="2">DESCRIPTION</th>
      <th colspan="3" class="center">HARGA ASAL</th>
      <th colspan="3" class="center">HARGA<?php //echo strtoupper($dt_price->company_name)?></th>
      <th rowspan="2" class="center">MARGIN</th>
    </tr>
    <tr>
      <th>BASIC</th>
      <th>SELLING</th>
      <th>PUBLISH</th>
      <th>BASIC</th>
      <th>SELLING</th>
      <th>PUBLISH</th>
    </tr>
  <?php 
    foreach($reff_price as $row_price) :
      echo '<tr>';
      echo '<td>'.$row_price->datepricetype_id.'</td>';
      echo '<td style="text-align: right">'.number_format($row_price->price_principal_ref).'</td>';
      echo '<td style="text-align: right">'.number_format($row_price->price_ref).'</td>';
      echo '<td style="text-align: right">'.number_format($row_price->price_publish_ref).'</td>';

      echo '<td style="text-align: right">'.number_format($row_price->price_principal).'</td>';
      echo '<td style="text-align: right">'.number_format($row_price->price).'</td>';
      echo '<td style="text-align: right">'.number_format($row_price->price_publish).'</td>';
      $margin = $row_price->price - $row_price->price_ref;
      echo '<td style="text-align: right">'.number_format($margin).'</td>';
      echo '</tr>';
    endforeach;
  ?>

  </table>
    
</div>