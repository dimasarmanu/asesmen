<div class="row">
	<div class="col-xs-12">

		<div class="page-header">
			<h1>
				<?php echo $title?>
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					<?php echo isset($breadcrumbs)?$breadcrumbs:''?>
				</small>
			</h1>
		</div><!-- /.page-header -->
		<!-- div.dataTables_borderWrap -->
		<div class="widget-body">
				<div class="widget-main no-padding">
					
				<form class="form-horizontal" method="post" id="form_search" action="mbl_reseller/C_deposit_history/find_data">
				<b>Pencarian Data</b><br><br>
				<div class="form-group">
						<label class="control-label col-md-2">Distributor</label>
						<div class="col-md-3">
							<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), ($this->session->userdata('user')->role != 1)?$this->session->userdata('user')->company_id:'' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
			</div>
						<div class="col-md-7" style="margin-left: -10px">
							<a href="#" id="btn_search_data" class="btn btn-xs btn-default">
								<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
								Search
							</a>
							<a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
								<i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
								Reset
							</a>
							<a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
								<i class="fa fa-file-word-o bigger-110"></i>
								Export Excel
							</a>
							<button type="button" class="btn btn-xs btn-primary" onclick="getMenu('mbl_reseller/C_deposit_history/form')"><i class="fa fa-money"></i> Topup Saldo Deposit</button>
						</div>
				</div>
				<hr class="separator">
		<div style="margin-top:-16px">
					<table id="dynamic-table" base-url="mbl_reseller/C_deposit_history" url-detail="mbl_reseller/C_deposit_history/show_detail" class="table table-bordered table-hover" >
					 <thead>
							<tr style="background: linear-gradient(to bottom, #cfd4ce 90%, #98948e 20%) !important">  
								<th width="50px">No</th>
								<th style="width:100px">No.Topup</th>
								<th>Tanggal</th>
								<th>Information Pengirim</th>
								<th>Information Penerima</th>
								<th>Principal</th>
								<th width="150px">Total</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
		</div>
			</div>
		</div>
	</div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_no_style.js'?>"></script>



