<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  
    <!-- PAGE CONTENT BEGINS -->
      <div class="widget-body">
        <div class="widget-main no-padding">

            <div class="col-xs-12">
              <p style="font-size: 14px"><b>SUPPLIER BALANCE</b></p>
              <table id="dynamic-table" base-url="mbl_reseller/C_account" url-detail="mbl_reseller/C_account/show_detail" class="table table-striped table-bordered table-hover">
               <thead>
                <tr style="background-color: #f5810b; color: white">  
                  <th rowspan="2" width="30px" class="center">NO</th>
                  <th rowspan="2">PRINCIPAL</th>
                  <th colspan="3" class="center">TOTAL AMOUNT</th>
                  <th colspan="3" class="center">TOTAL ISSUED</th>
                </tr>
                <tr style="background-color: #f5810b; color: white">
                  <th align="center" width="120px">Amount</th>
                  <th align="center" width="120px">Issued</th>
                  <th align="center" width="120px">Balance</th>
                  <!-- total issued -->
                  <th align="center" width="120px">Weekday</th>
                  <th align="center" width="120px">Weekend</th>
                  <th align="center" width="120px">Highseason</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $no=0; 
                  $arr_amount = array();
                  $arr_issued_amount = array();
                  $arr_balance = array();
                  $arr_weekday = array();
                  $arr_weekend = array();
                  $arr_highseason = array();

                  foreach($balance as $row_balance) : 
                  $no++; 
                  $arr_amount[] = $row_balance->amount;
                  $arr_issued_amount[] = $row_balance->issued_amount;
                  $arr_balance[] = $row_balance->balance;
                  $arr_weekday[] = $row_balance->weekday;
                  $arr_weekend[] = $row_balance->weekend;
                  $arr_highseason[] = $row_balance->highseason;
                ?>
                <tr style="background-color: #f5810b21; color: black">  
                  <td width="30px" class="center"><?php echo $no?></td>
                  <td><?php echo $row_balance->principal_name?></td>
                  <td align="right">Rp. <?php echo number_format( $row_balance->amount )?>,-</td>
                  <td align="right">Rp. <?php echo number_format( $row_balance->issued_amount )?>,-</td>
                  <td align="right">Rp. <?php echo number_format( $row_balance->balance )?>,-</td>
                  <td align="right">Rp. <?php echo number_format( $row_balance->weekday )?>,-</td>
                  <td align="right">Rp. <?php echo number_format( $row_balance->weekend )?>,-</td>
                  <td align="right">Rp. <?php echo number_format( $row_balance->highseason )?>,-</td>
                </tr>
                <?php endforeach;?>
                <tr style="background-color: #f5810b21; color: black">  
                  <td width="30px" class="center"></td>
                  <td align="right"><b>TOTAL</b></td>
                  <td align="right"><b>Rp. <?php echo number_format( array_sum($arr_amount) )?>,-</b></td>
                  <td align="right"><b>Rp. <?php echo number_format( array_sum($arr_issued_amount) )?>,-</b></td>
                  <td align="right"><b>Rp. <?php echo number_format( array_sum($arr_balance) )?>,-</b></td>
                  <td align="right"><b>Rp. <?php echo number_format( array_sum($arr_weekday) )?>,-</b></td>
                  <td align="right"><b>Rp. <?php echo number_format( array_sum($arr_weekend) )?>,-</b></td>
                  <td align="right"><b>Rp. <?php echo number_format( array_sum($arr_highseason) )?>,-</b></td>
                </tr>
              </tbody>
            </table>
          </div>

        </div>
      </div>
    
    <!-- PAGE CONTENT ENDS -->
</div><!-- /.row -->


