<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" /> 
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){

    $('#form_default').ajaxForm({
      beforeSend: function() {
        achtungShowLoader();  
      },
      uploadProgress: function(event, position, total, percentComplete) {
      },
      complete: function(xhr) {     
        var data=xhr.responseText;
        var jsonResponse = JSON.parse(data);

        if(jsonResponse.status === 200){
          $.achtung({message: jsonResponse.message, timeout:5});
          $('#page-area-content').load('mbl_reseller/C_principal/video/'+jsonResponse.principal_id+'?_=' + (new Date()).getTime());
        }else{
          $.achtung({message: jsonResponse.message, timeout:5});
        }
        achtungHideLoader();
      }
    }); 

})


counterfile = <?php $j=2;echo $j.";";?>

function hapus_file(a, b)

{

  if(b != 0){
    /*$.getJSON("<?php echo base_url('posting/delete_file') ?>/" + b, '', function(data) {
        document.getElementById("file"+a).innerHTML = "";
        greatComplate(data);
    });*/
  }else{
    y = a ;
    document.getElementById("file"+a).innerHTML = "";
  }

}

function tambah_file()

{

  counternextfile = counterfile + 1;

  counterIdfile = counterfile + 1;

  var html = "<div id=\"file"+counternextfile+"\" class='clonning_form'><div class='form-group'><label class='control-label col-sm-1' for=''>Link Youtube</label><div class='col-sm-3'><input type='text' class='form-control' id='link_youtube' name='link_youtube[]' ></div><label class='control-label col-sm-1' for=''>Attachment</label><div class='col-sm-3'><input type='file' class='form-control' id='attc' name='attc[]' ></div><div class='col-md-1' style='margin-left: -2%'><input type='button' onclick='hapus_file("+counternextfile+",0)' value=' x ' class='btn btn-xs btn-danger'/></div></div></div><div id=\"input_file"+counternextfile+"\"></div>"

  document.getElementById("input_file"+counterfile).innerHTML = html;

  counterfile++;

}


</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
      <div class="widget-body">
        <div class="widget-main no-padding">

          <form class="form-horizontal" method="post" id="form_default" action="<?php echo site_url('mbl_reseller/C_principal/process_upload_video')?>" enctype="multipart/form-data" autocomplete="off">
            <br>
            <input type="hidden" value="<?php echo isset($value)?$value->id:0?>" name="principal_id" id="principal_id">

            <table class="table table borderd" border="1" width="80%">
              <tr style="background-color: darkorange">
                <th>ID</th>
                <th>Principal Name</th>
                <th>Category</th>
                <th>Address</th>
              </tr>
              <tr>
                <td><?php echo isset($value)?$value->id:0?></td>
                <td><?php echo isset($value)?$value->principal_name:'-'?></td>
                <td><?php echo isset($value)?$value->category:'-'?></td>
                <td><?php echo isset($value)?$value->address:'-'?></td>
              </tr>
            </table>
            
            <p style="margin-top: 8px; font-weight: bold">UPLOAD LINK VIDEO</p>
            
            <div class="form-group">
                <label class="control-label col-sm-1" for="">Link Youtube</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="link_youtube" name="link_youtube[]" >
                </div>

                <label class="control-label col-sm-1" for="">Thumbnail</label>
                <div class="col-sm-3">
                  <input type="file" class="form-control" id="attc" name="attc[]" >
                </div>

                <div class ="col-md-1" style="margin-left: -2%">
                  <input onClick="tambah_file()" value="+" type="button" class="btn btn-xs btn-info" />
                </div>
            </div>

            <div id="clone_form_dokter">
              <div id="input_file<?php echo $j;?>"></div>
            </div>
            <hr>
            <p><b>THUMBNAIL VIDEO</b></p>
            <div>
              <ul class="ace-thumbnails clearfix">
                <?php foreach($image as $row_img) :?>
                <li id="tr_id_<?php echo $row_img->id?>">
                  <a href="<?php echo $row_img->url_link; ?>" data-rel="colorbox" target="_blank">
                    <img width="150" height="150" alt="150x150" src="<?php echo $row_img->thumnail_link; ?>" />
                    <div class="text">
                      <div class="inner">Sample Caption on Hover</div>
                    </div>
                  </a>

                  <div class="tools tools-bottom">
                    <a href="#" id="icon_action_<?php echo $row_img->id?>" redirect-link="mbl_reseller/C_principal/gallery/<?php echo isset($value)?$value->id:0?>" onclick="delete_attachment(<?php echo $row_img->id?>)">
                      <i class="ace-icon fa fa-times red"></i>
                    </a>
                  </div>
                </li>
                <?php endforeach;?>

              </ul>
            </div>
            
            
            <div class="form-actions center">

              <a onclick="getMenu('mbl_reseller/C_principal/process_upload_video')" href="#" class="btn btn-sm btn-success">
                <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                Kembali ke daftar
              </a>
              <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                Submit
              </button>
            </div>

          </form>
          
        </div>
      </div>
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


