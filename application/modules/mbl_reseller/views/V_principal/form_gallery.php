<link rel="stylesheet" href="<?php echo base_url()?>assets/css/dropzone.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/colorbox.css" />
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
      <div class="widget-body">
        <div class="widget-main no-padding">
            <table class="table table borderd" border="1" width="80%">
              <tr style="background-color: darkorange">
                <th>ID</th>
                <th>Principal Name</th>
                <th>Category</th>
                <th>Address</th>
              </tr>
              <tr>
                <td><?php echo isset($value)?$value->id:0?></td>
                <td><?php echo isset($value)?$value->principal_name:'-'?></td>
                <td><?php echo isset($value)?$value->category:'-'?></td>
                <td><?php echo isset($value)?$value->address:'-'?></td>
              </tr>
            </table>
            <br>
            <p><b>IMAGE GALLERY</b></p>
            <div>
              <ul class="ace-thumbnails clearfix">
                <?php foreach($image as $row_img) :?>
                <li id="tr_id_<?php echo $row_img->id?>">
                  <a href="<?php echo $row_img->url_link; ?>" data-rel="colorbox">
                    <img width="150" height="150" alt="150x150" src="<?php echo $row_img->thumnail_link; ?>" />
                    <div class="text">
                      <div class="inner">Sample Caption on Hover</div>
                    </div>
                  </a>

                  <div class="tools tools-bottom">
                    <a href="#" id="icon_action_<?php echo $row_img->id?>" redirect-link="mbl_reseller/C_principal/gallery/<?php echo isset($value)?$value->id:0?>" onclick="delete_attachment(<?php echo $row_img->id?>)">
                      <i class="ace-icon fa fa-times red"></i>
                    </a>
                  </div>
                </li>
                <?php endforeach;?>

              </ul>
            </div>
            <br>
            <p><b>UPLOADING FILES</b></p>
            <div class="center" style="width:100%; margin:12px;">
              <!-- our form -->
              <form id="form-default" method="post" action="mbl_reseller/C_principal/upload">
              <!-- hidden -->
              <input type="hidden" value="<?php echo isset($value)?$value->id:0?>" name="principal_id" id="principal_id">
                <input  type="file" name="avatar[]" multiple />
                <div class="hr hr-12 dotted"></div>
                <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                <button type="reset" class="btn btn-sm">Reset</button>
              </form>

            </div>
            
        </div>
      </div>
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url()?>assets/js/jquery.colorbox.js"></script>

<script type="text/javascript">
			jQuery(function($) {
	var $overflow = '';
	var colorbox_params = {
		rel: 'colorbox',
		reposition:true,
		scalePhotos:true,
		scrolling:false,
		previous:'<i class="ace-icon fa fa-arrow-left"></i>',
		next:'<i class="ace-icon fa fa-arrow-right"></i>',
		close:'&times;',
		current:'{current} of {total}',
		maxWidth:'100%',
		maxHeight:'100%',
		onOpen:function(){
			$overflow = document.body.style.overflow;
			document.body.style.overflow = 'hidden';
		},
		onClosed:function(){
			document.body.style.overflow = $overflow;
		},
		onComplete:function(){
			$.colorbox.resize();
		}
	};

	$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
	$("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon
	
	
	$(document).one('ajaxloadstart.page', function(e) {
		$('#colorbox, #cboxOverlay').remove();
   });
})
    </script>
    
<script type="text/javascript">
    jQuery(function($) {
      var $form = $('#form-default');
      //you can have multiple files, or a file input with "multiple" attribute
      var file_input = $form.find('input[type=file]');
      var upload_in_progress = false;

      file_input.ace_file_input({
        style : 'well',
        btn_choose : 'Select or drop files here',
        btn_change: null,
        droppable: true,
        thumbnail: 'large',
        
        maxSize: 110000,//bytes
        allowExt: ["jpeg", "jpg", "png", "gif"],
        allowMime: ["image/jpg", "image/jpeg", "image/png", "image/gif"],

        before_remove: function() {
          if(upload_in_progress)
            return false;//if we are in the middle of uploading a file, don't allow resetting file input
          return true;
        },

        preview_error: function(filename , code) {
          //code = 1 means file load error
          //code = 2 image load error (possibly file is not an image)
          //code = 3 preview failed
        }
      })
      file_input.on('file.error.ace', function(ev, info) {
        if(info.error_count['ext'] || info.error_count['mime']) alert('Invalid file type! Please select an image!');
        if(info.error_count['size']) alert('Invalid file size! Maximum 100KB');
        
        //you can reset previous selection on error
        //ev.preventDefault();
        //file_input.ace_file_input('reset_input');
      });
      
      
      var ie_timeout = null;//a time for old browsers uploading via iframe
      
      $form.on('submit', function(e) {
        e.preventDefault();
      
        var files = file_input.data('ace_input_files');
        if( !files || files.length == 0 ) return false;//no files selected
                  
        var deferred ;
        if( "FormData" in window ) {
          //for modern browsers that support FormData and uploading files via ajax
          //we can do >>> var formData_object = new FormData($form[0]);
          //but IE10 has a problem with that and throws an exception
          //and also browser adds and uploads all selected files, not the filtered ones.
          //and drag&dropped files won't be uploaded as well
          
          //so we change it to the following to upload only our filtered files
          //and to bypass IE10's error
          //and to include drag&dropped files as well
          formData_object = new FormData();//create empty FormData object
          
          //serialize our form (which excludes file inputs)
          $.each($form.serializeArray(), function(i, item) {
            //add them one by one to our FormData 
            formData_object.append(item.name, item.value);							
          });
          //and then add files
          $form.find('input[type=file]').each(function(){
            var field_name = $(this).attr('name');
            //for fields with "multiple" file support, field name should be something like `myfile[]`

            var files = $(this).data('ace_input_files');
            if(files && files.length > 0) {
              for(var f = 0; f < files.length; f++) {
                formData_object.append(field_name, files[f]);
              }
            }
          });


          upload_in_progress = true;
          file_input.ace_file_input('loading', true);
          
          deferred = $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
            processData: false,//important
            contentType: false,//important
                dataType: 'json',
                    data: formData_object,
            xhr: function() {
              var req = $.ajaxSettings.xhr();
              if (req && req.upload) {
                req.upload.addEventListener('progress', function(e) {
                  if(e.lengthComputable) {	
                    var done = e.loaded || e.position, total = e.total || e.totalSize;
                    var percent = parseInt((done/total)*100) + '%';
                    //percentage of uploaded file
                  }
                }, false);
              }
              return req;
            },
            beforeSend : function() {
              achtungShowLoader();
            },
            success : function() {
              achtungHideLoader();
              getMenu('mbl_reseller/C_principal/gallery/'+$('#principal_id').val()+'');
            }
          })

        }
        else {
          //for older browsers that don't support FormData and uploading files via ajax
          //we use an iframe to upload the form(file) without leaving the page

          deferred = new $.Deferred //create a custom deferred object
          
          var temporary_iframe_id = 'temporary-iframe-'+(new Date()).getTime()+'-'+(parseInt(Math.random()*1000));
          var temp_iframe = 
              $('<iframe id="'+temporary_iframe_id+'" name="'+temporary_iframe_id+'" \
              frameborder="0" width="0" height="0" src="about:blank"\
              style="position:absolute; z-index:-1; visibility: hidden;"></iframe>')
              .insertAfter($form)

          $form.append('<input type="hidden" name="temporary-iframe-id" value="'+temporary_iframe_id+'" />');
          
          temp_iframe.data('deferrer' , deferred);
          //we save the deferred object to the iframe and in our server side response
          //we use "temporary-iframe-id" to access iframe and its deferred object
          
          $form.attr({
                  method: 'POST',
                  enctype: 'multipart/form-data',
                  target: temporary_iframe_id //important
                });

          upload_in_progress = true;
          file_input.ace_file_input('loading', true);//display an overlay with loading icon
          $form.get(0).submit();
          
          
          //if we don't receive a response after 30 seconds, let's declare it as failed!
          ie_timeout = setTimeout(function(){
            ie_timeout = null;
            temp_iframe.attr('src', 'about:blank').remove();
            deferred.reject({'status':'fail', 'message':'Timeout!'});
          } , 30000);
        }


        ////////////////////////////
        //deferred callbacks, triggered by both ajax and iframe solution
        deferred
        .done(function(result) {//success
          //format of `result` is optional and sent by server
          //in this example, it's an array of multiple results for multiple uploaded files
          var message = '';
          for(var i = 0; i < result.length; i++) {
            if(result[i].status == 'OK') {
              message += "File successfully saved. Thumbnail is: " + result[i].url
            }
            else {
              message += "File not saved. " + result.message;
            }
            message += "\n";
          }
          // alert(message);
        })
        .fail(function(result) {//failure
          alert("There was an error");
        })
        .always(function() {//called on both success and failure
          if(ie_timeout) clearTimeout(ie_timeout)
          ie_timeout = null;
          upload_in_progress = false;
          file_input.ace_file_input('loading', false);
        });

        deferred.promise();
      });


      //when "reset" button of form is hit, file field will be reset, but the custom UI won't
      //so you should reset the ui on your own
      $form.on('reset', function() {
        $(this).find('input[type=file]').ace_file_input('reset_input_ui');
      });


      if(location.protocol == 'file:') alert("For uploading to server, you should access this page using 'http' protocal, i.e. via a webserver.");

    });
  </script>
  


