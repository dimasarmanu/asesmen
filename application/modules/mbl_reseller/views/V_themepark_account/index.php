<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->
	<br>
    <div class="clearfix" style="margin-bottom:-5px">
      <?php echo $this->authuser->show_button('mbl_reseller/C_themepark_account','C','',1)?>
      <?php echo $this->authuser->show_button('mbl_reseller/C_themepark_account','D','',5)?>
    </div>
    <hr class="separator">
    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <div style="margin-top:-16px">
      <table id="dynamic-table" base-url="mbl_reseller/C_themepark_account" class="table table-bordered table-hover">
       <thead>
        <tr>  
          <th width="30px" class="center"></th>
          <th width="120px">&nbsp;</th>
          <th width="50px">ID</th>
          <th>Fullname</th>
          <th>Email</th>
          <th>Username</th>
          <th>Status</th>
          <th width="100px">Last Update</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    </div>
  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable.js'?>"></script>

