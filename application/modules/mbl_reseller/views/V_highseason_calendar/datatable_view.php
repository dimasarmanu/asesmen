<div class="row">
  <div class="col-xs-12">

    <table id="dynamic-table" base-url="mbl_reseller/C_highseason_calendar" url-detail="mbl_reseller/C_highseason_calendar/show_detail" class="table table-striped table-bordered table-hover">
       <thead>
        <tr>  
          <th width="30px" class="center"></th>
          <th width="40px" class="center"></th>
          <th width="40px" class="center"></th>
          <th width="40px"></th>
          <th width="70px">ID</th>
          <th>Principal</th>
          <th>Season Date</th>
          <th>Description</th>
          <th>Status</th>
          <th>Last Update</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>

  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_with_detail.js'?>"></script>



