<link rel="stylesheet" href="<?php echo  base_url()?>assets/css/jquery-ui.custom.css" />
<link rel="stylesheet" href="<?php echo  base_url()?>assets/css/fullcalendar.css" />
<!-- div.dataTables_borderWrap -->
<div class="row">
      <div class="col-sm-9">
        <div class="space"></div>

        <!-- #section:plugins/data-time.calendar -->
        <div id="calendar"></div>

        <!-- /section:plugins/data-time.calendar -->
      </div>

      <div class="col-sm-3">
        <div class="widget-box transparent">
          <div class="widget-header">
            <h4>Draggable events</h4>
          </div>

          <div class="widget-body">
            <div class="widget-main no-padding">
              <div id="external-events">
                <div class="external-event label-info" data-id="1" data-class="label label-info">
                  <i class="ace-icon fa fa-arrows"></i>
                  Public Holiday
                </div>

                <div class="external-event label-success" data-id="0" data-class="label label-success">
                  <i class="ace-icon fa fa-arrows"></i>
                  Highseason
                </div>

                <div class="external-event label-danger" data-id="2" data-class="label label-danger">
                  <i class="ace-icon fa fa-arrows"></i>
                  Closed 
                </div>
                <!-- <label>
                  <input type="checkbox" class="ace ace-checkbox" id="drop-remove" />
                  <span class="lbl"> Remove after drop</span>
                </label> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<!-- fullcalendar -->
<script src="<?php echo base_url()?>assets/js/jquery-ui.custom.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/moment.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar.js"></script>
<script src="<?php echo base_url()?>assets/js/bootbox.js"></script>

<script type="text/javascript">

jQuery(function($) {

/* initialize the external events
-----------------------------------------------------------------*/

$('#external-events div.external-event').each(function() {

// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
// it doesn't need to have a start or end
var eventObject = {
title: $.trim($(this).text()) // use the element's text as the event title
};

// store the Event Object in the DOM element so we can get to it later
$(this).data('eventObject', eventObject);

// make the event draggable using jQuery UI
$(this).draggable({
zIndex: 999,
revert: true,      // will cause the event to go back to its
revertDuration: 0  //  original position after the drag
});

});

/* initialize the calendar
-----------------------------------------------------------------*/

var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();


var calendar = $('#calendar').fullCalendar({
//isRTL: true,
buttonHtml: {
prev: '<i class="ace-icon fa fa-chevron-left"></i>',
next: '<i class="ace-icon fa fa-chevron-right"></i>'
},

header: {
left: 'prev,next today',
center: 'title',
right: 'month'
},

// events: getEventData(),
eventSources: [
// your event source
{
    url: 'mbl_reseller/C_highseason_calendar/get_highseason_date',
    method: 'POST',
    extraParams: {
    custom_param1: 'something',
    custom_param2: 'somethingelse'
    },
    failure: function() {
    alert('there was an error while fetching events!');
    },
}
// any other sources...
],
editable: true,
droppable: true, // this allows things to be dropped onto the calendar !!!
drop: function(date, allDay) { 
// retrieve the dropped element's stored Event Object
var originalEventObject = $(this).data('eventObject');
var categoryId = $(this).attr('data-id');
var $extraEventClass = $(this).attr('data-class');
console.log(originalEventObject);
// we need to copy it, so that multiple events don't have a reference to the same object
var copiedEventObject = $.extend({}, originalEventObject);
// assign it the date that was reported
copiedEventObject.start = date;
copiedEventObject.allDay = allDay;
copiedEventObject.categoryId = categoryId;
if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];

// render the event on the calendar
// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            
},
selectable: true,
selectHelper: true,
select: function(start, end, allDay) {
console.log(start);
$.getJSON("mbl_reseller/C_highseason_calendar/show_view_json/", '', function (data) {
    $('.content_modal').html(data.html);
});

//display a modal
var modal = 
'<div class="modal fade">\
    <div class="modal-dialog">\
    <div class="modal-content">\
        <form class="form-horizontal" id="form_highseason" method="POST" action="mbl_reseller/C_highseason_calendar/process" enctype="multipart/form-data">\
        <div class="modal-body">\
        <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
        <div class="content_modal"></div>\
        </div>\
        <div class="modal-footer">\
        <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
        <button type="submit" id="btnSaveModal" name="submit" class="btn btn-sm btn-info">\
            <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>\
            Submit\
        </button>\
        </div>\
        </form>\
    </div>\
    </div>\
</div>';

var modal = $(modal).appendTo('body');

modal.find("input[name=date]").val(start);
modal.find("input[name=end_date]").val(end);

modal.find('form').on('submit', function(ev){
    ev.preventDefault();

    start = $(this).find("input[name=date]").val();
    end = $(this).find("input[name=end_date]").val();

    var post_data = {
        id : $(this).find("input[name=id]").val(),
        title : $(this).find("textarea[name=title]").val(),
        date : $(this).find("input[name=date]").val(),
        end_date : $(this).find("input[name=end_date]").val(),
        category : $(this).find("select[name=category]").val(),
        principal_id : $(this).find("select[name=principal_id]").val(),
        is_active : $(this).find("'input[name=is_active]:checked'").val(),
    }

    $.post( $('#form_highseason').attr('action'), post_data, function(result){
    calendar.fullCalendar('refetchEvents');
    modal.modal("hide");
    });
    
    
});
            
modal.modal('show').on('hidden', function(){
    modal.remove();
});


}
,
eventClick: function(calEvent, jsEvent, view) {
console.log(calEvent);
// default

$.getJSON("mbl_reseller/C_highseason_calendar/show_view_json/"+calEvent.id+"", '', function (data) {
    $('.content_modal').html(data.html);
});

//display a modal
var modal = 
'<div class="modal fade">\
    <div class="modal-dialog">\
    <div class="modal-content">\
        <form class="form-horizontal" id="form_highseason" method="POST" action="mbl_reseller/C_highseason_calendar/process" enctype="multipart/form-data">\
        <div class="modal-body">\
        <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
        <div class="content_modal"></div>\
        </div>\
        <div class="modal-footer">\
        <button type="button" class="btn btn-sm btn-danger" data-action="delete_'+calEvent.id+'"><i class="ace-icon fa fa-trash-o"></i> Delete Event</button>\
        <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
        <button type="submit" id="btnSaveModal" name="submit" class="btn btn-sm btn-info">\
            <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>\
            Submit\
        </button>\
        </div>\
        </form>\
    </div>\
    </div>\
</div>';

var modal = $(modal).appendTo('body');

modal.find("input[name=date]").val(calEvent.start);
modal.find("input[name=end_date]").val(calEvent.end);
modal.find("textarea[name=title]").val(calEvent.title);

modal.find('form').on('submit', function(ev){
    ev.preventDefault();

    calEvent.title = $(this).find("textarea[name=title]").val();
    calEvent.start = $(this).find("input[name=date]").val();
    calEvent.end = $(this).find("input[name=end_date]").val();
    calendar.fullCalendar('updateEvent', calEvent);

    var post_data = {
    id : $(this).find("input[name=id]").val(),
    title : $(this).find("textarea[name=title]").val(),
    date : $(this).find("input[name=date]").val(),
    end_date : $(this).find("input[name=end_date]").val(),
    category : $(this).find("select[name=category]").val(),
    principal_id : $(this).find("select[name=principal_id]").val(),
    }

    $.post( $('#form_highseason').attr('action'), post_data, function(result){
    calendar.fullCalendar('refetchEvents');
    modal.modal("hide");
    });
    
    
});

modal.find('button[data-action=delete_'+calEvent.id+']').on('click', function() {
    calendar.fullCalendar('removeEvents' , function(ev){
    console.log(ev._id);
    // delete data
    return (ev._id == calEvent._id);
    })

    if(confirm('Are you sure?')){
    $.ajax({
        url: 'mbl_reseller/C_highseason_calendar/delete',
        type: "post",
        data: {ID:calEvent.id},
        dataType: "json",
        complete: function(xhr) {  
            modal.modal("hide");
        }

    });
    }
    
    
});

modal.modal('show').on('hidden', function(){
    modal.remove();
});

}

});

})

function delete_data(myid){
if(confirm('Are you sure?')){
$.ajax({
    url: 'mbl_reseller/C_highseason_calendar/delete',
    type: "post",
    data: {ID:myid},
    dataType: "json",
    beforeSend: function() {
    achtungShowLoader();  
    },
    uploadProgress: function(event, position, total, percentComplete) {
    },
    complete: function(xhr) {     
    var data=xhr.responseText;
    var jsonResponse = JSON.parse(data);
    if(jsonResponse.status === 200){
        $.achtung({message: jsonResponse.message, timeout:5});
        return false;
    }else{
        $.achtung({message: jsonResponse.message, timeout:5});
    }
    achtungHideLoader();
    }

});

}else{
return false;
}

}

</script>
