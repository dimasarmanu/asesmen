<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){
  
    $('#form_default').ajaxForm({
      beforeSend: function() {
        achtungShowLoader();  
      },
      uploadProgress: function(event, position, total, percentComplete) {
      },
      complete: function(xhr) {     
        var data=xhr.responseText;
        var jsonResponse = JSON.parse(data);

        if(jsonResponse.status === 200){
          $.achtung({message: jsonResponse.message, timeout:5});
          // show billing transaction
          PopupCenter('mbl_reseller/C_principal_payment/print_billing/'+jsonResponse.id+'', 'PRINT BILLING', 900, 600);
          $('#page-area-content').load('mbl_reseller/C_payment_history?_=' + (new Date()).getTime());

        }else{
          $.achtung({message: jsonResponse.message, timeout:5});
        }
        achtungHideLoader();
      }
    }); 
})



</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<?php 
  foreach($value as $row_val_2){
    $total_amount[] = $row_val_2->total;
  } 
?>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <!-- #section:pages/invoice -->
          <div class="widget-box transparent">
            <div class="widget-header widget-header-large">
              <img src="<?php echo isset($value[0]->logo)?PATH_MBR.$value[0]->logo:''?>" alt="" style="width: 50px">
              <h3 class="widget-title grey lighter">
                <?php echo isset($value[0]->principal_name)?$value[0]->principal_name:''?> <i class="fa fa-angle-double-right"></i>
                <small style="font-size: 11px !important;"><?php echo isset($value[0]->address)?strip_tags(trim($value[0]->address)):''?></small>
              </h3>
              <!-- /section:pages/invoice.info -->
            </div>

            <div class="widget-body">
              <div class="widget-main padding-24">

                <form class="form-horizontal" method="post" id="form_default" action="<?php echo site_url('mbl_reseller/C_principal_payment/process')?>" enctype="multipart/form-data" autocomplete="off">

                  <div class="row">
                      <br>
                      <!-- button -->
                      <div class="pull-right" style="margin-top: -35px">
                          <a onclick="getMenu('mbl_reseller/C_principal_payment')" href="#" class="btn btn-sm btn-success">
                            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                            Kembali ke daftar
                          </a>
                          <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                            <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                            Proses Request
                          </button>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-2">ID</label>
                        <div class="col-md-1">
                          <input name="id" id="id" value="" placeholder="Auto" class="form-control" type="text" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2">No. Billing</label>
                        <div class="col-md-2">
                          <input name="payment_number" id="payment_number" value="<?php echo $bill_num; ?>" placeholder="" class="form-control" type="text" >
                        </div>
                        <label class="control-label col-md-1">Tanggal</label>
                        <div class="col-md-3">
                          <div class="input-group">
                            <input class="form-control date-picker" name="payment_date" id="payment_date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-d')?>"/>
                            <span class="input-group-addon">
                              <i class="fa fa-calendar bigger-110"></i>
                            </span>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2">Principal</label>
                        <div class="col-md-3">
                          <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_principals', 'where'=>array('is_active'=>'Y'), 'id'=>'id', 'name' => 'principal_name'), isset($value[0]->principal_id)?$value[0]->principal_id:'' , 'principal_id' , 'principal_id' ,'chosen-slect form-control',($flag=='read')?'':'','', 'db_mbr');?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2">Nomor Rekening</label>
                        <div class="col-md-2">
                          <input name="account_number" id="account_number" value="" placeholder="" class="form-control" type="text" >
                        </div>
                        <label class="control-label col-md-2">Pembayaran a.n</label>
                        <div class="col-md-3">
                          <input name="receive_name" id="receive_name" value="" placeholder="" class="form-control" type="text" >
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-md-2">Bank</label>
                        <div class="col-md-3">
                          <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_banks', 'where'=>array('is_active'=>'Y'), 'id'=>'id', 'name' => 'bank_name'), '' , 'from_bank_id' , 'from_bank_id' ,'chosen-slect form-control',($flag=='read')?'':'','', 'db_mbr');?>
                        </div>
                        <!-- <label class="control-label col-md-2">Tujuan Bank</label>
                        <div class="col-md-3">
                          <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_banks', 'where'=>array('is_active'=>'Y'), 'id'=>'id', 'name' => 'bank_name'), '' , 'to_bank_id' , 'to_bank_id' ,'chosen-slect form-control',($flag=='read')?'':'','', 'db_mbr');?>
                        </div> -->
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2">Total Pembayaran</label>
                        <div class="col-md-2">
                          <input name="total_amount" id="total_amount" value="<?php echo number_format(array_sum($total_amount))?>" class="form-control" type="text" style="text-align: right">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2">Keterangan</label>
                        <div class="col-md-6">
                          <textarea name="keterangan" id="keterangan" style="height: 80px !important" class="form-control"></textarea>
                        </div>
                      </div>
                  </div>
                  <!-- /.row -->
                  <hr class="seaprator">
                  <center><b>DETAIL ORDER</b></center>
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr style="background-color: #a0a09fe8">
                        <th class="center">#</th>
                        <th>Tanggal</th>
                        <th>Pemesan</th>
                        <th class="hidden-xs">Kode Booking</th>
                        <th class="hidden-480">Produk</th>
                        <th>Total</th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php 
                        $no = 0; foreach($value as $row_val) : $no ++; 
                        $total[] = $row_val->total;
                      ?>
                      <input type="hidden" name="requestticket_id[]" value="<?php echo $row_val->id?>">
                      <tr>
                        <td class="center"><?php echo $no?></td>
                        <td> <a href="#"><?php echo $this->tanggal->formatDate($row_val->trans_time)?></a></td>
                        <td><?php echo ucwords($row_val->name_to)?></td>
                        <td class="hidden-xs"><?php echo $row_val->booking_code?></td>
                        <td class="hidden-480"><?php echo $row_val->partner_productname?></td>
                        <td align="right"><?php echo number_format($row_val->total)?></td>
                      </tr>
                      <?php endforeach; ?>

                    </tbody>
                  </table>
                  
                </form>

                <div class="hr hr8 hr-double hr-dotted"></div>

                <div class="row">
                  <div class="col-sm-5 pull-right">
                    <h4 class="pull-right">
                      Total amount :
                      <span class="red"><?php echo number_format(array_sum($total))?></span>
                    </h4>
                  </div>
                  <div class="col-sm-7 pull-left"> Billing total for this payment </div>
                </div>

                <div class="space-6"></div>

                <div class="well">
                  Pastikan data pembayaran sesuai dengan Detail Order, data yang sudah diproses tidak dapat dikembalikan.
                </div>

              </div>
            </div>
          </div>

          <!-- /section:pages/invoice -->
        </div>
      </div>    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


