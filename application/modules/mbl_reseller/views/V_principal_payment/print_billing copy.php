<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.css" />

<!-- ace styles -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/css_custom.css" />

<div class="row">
    <div class="col-xs-12" style="background-color: #ff7600; height: 55px">
      <img src="<?php echo base_url().PATH_IMG_DEFAULT.$app->app_logo?>" height="50px">
    </div>
</div>

<div class="row">
    <div class="center col-xs-12">
      <h2>PRINCIPAL BILLING</h2>
    </div>

    <div class="col-xs-8">
      <blockquote>
        <p class="lighter line-height-125">
          <?php echo isset($value[0]->principal_name)?$value[0]->principal_name:''?>
        </p>
        <small style="font-size: 12px">
          <?php echo isset($value[0]->address)?$value[0]->address:''?>
        </small>
      </blockquote>      
    </div>

    <div class="col-xs-4">
      <table class="table table-bordered">
        <tr style="background-color: #cecccc">
          <td>No. Billing</td>
          <td>Tanggal</td>
        </tr>
        <tr>
          <td><?php echo isset($value[0]->payment_number) ? '#'.$value[0]->payment_number : '-' ?></td>
          <td><?php echo isset($value[0]->payment_date) ? $this->tanggal->formatDateShort($value[0]->payment_date) : '-' ?></td>
        </tr>
      </table>
    </div>
</div>

<div class="row">
  <div class="center col-xs-12">
    <table class="table table-bordered">
      <tr>
        <td><b>Tujuan Pembayaran</b></td>
      </tr>
      <tr>
        <td>
          <b>"<?php echo isset($value[0]->bank_name) ? $value[0]->bank_name : '-' ?>"</b> a.n <?php echo isset($value[0]->receive_name) ? $value[0]->receive_name : '-' ?><br>
          No. Rekening <b><?php echo isset($value[0]->account_number) ? $value[0]->account_number : '-' ?></b>
        </td>
      </tr>
    </table>
  </div>
</div>
<!-- table detail order -->
<div class="row">
    <div class="center col-xs-12">
      <table width="100%">
        <tr style="background-color: #cecccc">
          <th class="center">#</th>
          <th>Tanggal</th>
          <th>Deskripsi</th>
          <th>Total</th>
        </tr>
        <?php 
          $no = 0; foreach($value as $row_val) : $no ++; 
          $total[] = $row_val->total_priceprincipal;
        ?>
        <input type="hidden" name="requestticket_id[]" value="<?php echo $row_val->id?>">
        <tr>
          <td class="center"><?php echo $no?></td>
          <td> <a href="#"><?php echo $this->tanggal->formatDate($row_val->trans_time)?></a></td>
          <td>
            <?php echo $row_val->booking_code?> - <?php echo ucwords($row_val->name_to)?><br>  
            <?php echo $row_val->partner_productname?>
          </td>
          <td align="right"><?php echo number_format($row_val->total_priceprincipal)?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
          <td colspan="3" align="right"><b>Total</b></td>
          <td align="right"><b><?php echo number_format(array_sum($total))?><b></td>
        </tr>
        <tr>
          <td colspan="4"><b>Terbilang : <i>"Dua Ratus Ribu Rupiah"</i></b></td>
        </tr>

      </table>
    </div>
</div>