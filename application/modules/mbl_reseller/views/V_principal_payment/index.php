<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){

  oTable = $('#dynamic-table').DataTable({ 
          
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "ordering": false,
    "searching": true,
    "bPaginate": true,
    "bInfo": true,
    "scrollY": "500px",
    "scrollX": "500px",
    "pageLength": 50,
    "ajax": {
        "url": $('#dynamic-table').attr('base-url'),
        "type": "POST"
    },

  });

  $('#btn_search_data').click(function (e) {    
      e.preventDefault();
      $.ajax({
      url: 'mbl_reseller/C_principal_payment/find_data',
      type: "post",
      data: $('#form_search').serialize(),
      dataType: "json",
      beforeSend: function() {
        achtungShowLoader();  
      },
      success: function(data) {
        achtungHideLoader();
        find_data_reload(data);
      }
    });
  });

  $('#btn_reset_data').click(function (e) {
      e.preventDefault();
      oTable.ajax.url('mbl_reseller/C_principal_payment/get_data').load();
      $('#form_search')[0].reset();
  });

  $('#btn_export_excel').click(function (e) {
      e.preventDefault();
      $.ajax({
        url: 'mbl_reseller/C_principal_payment/find_data',
        type: "post",
        data: $('#form_search').serialize(),
        dataType: "json",
        beforeSend: function() {
          achtungShowLoader();  
        },
        success: function(data) {
          achtungHideLoader();
          export_excel(data);
        }
      });

    });

    $('#btn_create_payment').click(function (e) {
      var searchIDs = $("#dynamic-table input:checkbox:checked").map(function(){
        return $(this).val();
      }).toArray();

      if( searchIDs != '' ){
        e.preventDefault();
        $.ajax({
          url: 'mbl_reseller/C_principal_payment/select_transaction',
          type: "post",
          data: {data : searchIDs},
          dataType: "json",
          beforeSend: function() {
            achtungShowLoader();  
          },
          success: function(result) {
            achtungHideLoader();
            getMenu('mbl_reseller/C_principal_payment/create_payment?'+result.data);
          }
        });
      }else{
        alert('Tidak ada item yang dipilih !'); return false;
      }

    });


});


function find_data_reload(result){

  oTable.ajax.url('mbl_reseller/C_principal_payment/get_data?'+result.data).load();

}

function export_excel(result){

  window.open('mbl_reseller/C_principal_payment/export_excel?'+result.data+'','_blank');  

}

$('select[name="principal_id"]').change(function () {
    if ($(this).val()) {
        $.getJSON("Templates/References/getProdukByPrincipal/" + $(this).val(), '', function (data) {
            $('#product option').remove();
            $('<option value="">-Pilih Produk-</option>').appendTo($('#product'));
            $.each(data, function (i, o) {
                $('<option value="' + o.id + '">' + o.partner_productname + '</option>').appendTo($('#product'));
            });

        });
    } else {
        $('#product option').remove()
    }
});

function checkAll(elm) {

  if($(elm).prop("checked") == true){
    $('.checked').each(function(){
        $(this).prop("checked", true);
    });
  }else{
    $('.checked').prop("checked", false);
  }

}

</script>

<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

    <form class="form-horizontal" method="post" id="form_search" action="#">

    <div class="row">
      <div class="col-md-12">
        <!-- <center>
          <h4>FORM PENCARIAN DATA TRANSAKSI<br>
            <small style="font-size:12px"> Silahkan lakukan pencarian data dengan parameter dibawah ini </small>
          </h4>
        </center> -->
        
        <p><b>PENCARIAN BERDASARKAN PRINCIPAL</b></p>

        <div class="form-group">
			<label class="control-label col-md-2">Distributor</label>
			<div class="col-md-3">
				<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), ($this->session->userdata('user')->role != 1)?$this->session->userdata('user')->company_id:'' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
				</div>
		</div>
        
        <div class="form-group">
            <label class="control-label col-md-2">Principal</label>
            <div class="col-md-4">
              <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_principals', 'where'=>array(), 'id'=>'id', 'name' => 'principal_name'),'','principal_id','principal_id','chosen-slect form-control','','', 'db_mbr');?>
            </div>
            <label class="control-label col-md-1">Produk</label>
            <div class="col-md-3">
              <select name="product" id="product" class="form-control">
                <option value="0">-Silahkan Pilih</option>
              </select>
            </div>
        </div>

        <p><b>PENCARIAN BERDASARKAN WAKTU TRANSAKSI</b></p>
        <div class="form-group">
            <label class="control-label col-md-2">Transaksi Bulan</label>
            <div class="col-md-2">
              <?php echo $this->master->get_bulan('' , 'from_month', 'from_month', 'form-control', '','') ?>
            </div>
            <label class="control-label col-md-1">s/d Bulan</label>
            <div class="col-md-2">
              <?php echo $this->master->get_bulan('' , 'to_month', 'to_month', 'form-control', '','') ?>
            </div>
            <label class="control-label col-md-1">Tahun</label>
            <div class="col-md-2">
              <?php echo $this->master->get_tahun('' , 'year', 'year', 'form-control', '', '') ?>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-2">Tanggal</label>
            <div class="col-md-2">
              <div class="input-group">
                <input class="form-control date-picker" name="from_tgl" id="from_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                <span class="input-group-addon">
                  <i class="fa fa-calendar bigger-110"></i>
                </span>
              </div>
            </div>

            <label class="control-label col-md-1">s/d Tanggal</label>
            <div class="col-md-2">
              <div class="input-group">
                <input class="form-control date-picker" name="to_tgl" id="to_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                <span class="input-group-addon">
                  <i class="fa fa-calendar bigger-110"></i>
                </span>
              </div>
            </div>
        </div>
        

        <div class="form-group">
          <label class="control-label col-md-2 ">&nbsp;</label>
          <div class="col-md-10" style="margin-left: 6px">
            <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
              <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
              Search
            </a>
            <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
              <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Reset
            </a>
            <a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
              <i class="fa fa-file-word-o bigger-110"></i>
              Export Excel
            </a>
            <a href="#" id="btn_create_payment" class="btn btn-xs btn-primary">
              <i class="fa fa-file-word-o bigger-110"></i>
              Create Request
            </a>
          </div>
        </div>

      </div>
    </div>
    <hr class="separator">
    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <div style="margin-top:-27px">
      <table id="dynamic-table" base-url="mbl_reseller/C_principal_payment/get_data" url-detail="mbl_reseller/C_principal_payment/show_detail" class="table table-striped table-bordered table-hover">
       <thead>
        <tr>  
          <th width="30px" class="center"><label class="pos-rel">
                        <input type="checkbox" class="ace" name="selected_id" onclick="checkAll(this)"/>
                        <span class="lbl"></span>
                    </label></th>
          <th>Kode Booking</th>
          <th>Nama Agent</th>
          <th>Tanggal</th>
          <th>Principal</th>
          <th>Nama Pemesan</th>
          <th>Tanggal Kedatangan</th>
          <th>Produk</th>
          <th>Hari Kunjungan</th>
          <th>Group</th>
          <th>Jml</th>
          <th>Modal</th>
          <th>Total Modal</th>
          <th>Jual</th>
          <th>Total</th>
          <th>Margin</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    </div>

    </form>

  </div><!-- /.col -->
</div><!-- /.row -->





