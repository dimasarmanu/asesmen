<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Editable Invoice</title>
	
	<link rel='stylesheet' type='text/css' href='<?php echo base_url()?>assets/css_billing/css/style.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url()?>assets/css_billing/css/print.css' media="print" />
	

</head>

<body>

	<div id="page-wrap">

		<textarea id="header">REIMBURSMENT</textarea>
		
		<div id="identity">
		
      <textarea id="address">
        PT. PELANGI NUSANTARA
        Jl. Jambore, RT.6/RW.11, 
		Harjamukti, Kec. Cimanggis,
		Kota Depok, Jawa Barat 16454
        Phone: (021) 84341028
      </textarea>

            <div id="logo">
              <div id="logohelp">
                <input id="imageloc" type="text" size="50" value="" /><br />
                (max width: 540px, max height: 100px)
              </div>
              <img src="<?php echo base_url().PATH_IMG_DEFAULT.$app->app_logo?>" height="50px" style="background-color: #ff5e00">
            </div>
		
		</div>
		
		<div style="clear:both"></div>
		
		<div id="customer">
      <div style="width: 30%; float: left">
      <h2><?php echo isset($value[0]->principal_name)?$value[0]->principal_name:''?></h2>
      <?php echo isset($value[0]->address)?$value[0]->address:''?>
      </div>
      <div style="width: 70%; float: right">
      <table id="meta">
          <tr>
              <td class="meta-head">Invoice #</td>
              <td><textarea><?php echo isset($value[0]->payment_number)?'#'.$value[0]->payment_number:''?></textarea></td>
          </tr>
          <tr>

              <td class="meta-head">Tanggal</td>
              <td><textarea id="date"><?php echo isset($value[0]->payment_date)?$this->tanggal->formatDateShort($value[0]->payment_date):''?></textarea></td>
          </tr>
          <tr>
              <td class="meta-head">Total Amount</td>
              <td><div class="due"><?php echo isset($value[0]->total_amount)?number_format($value[0]->total_amount):''?></div></td>
          </tr>

      </table>
      </div>
		</div>
		
		<table id="items">
		
		  <tr>
		      <th>Tanggal</th>
		      <th>Kode Booking</th>
		      <th>Pemesan</th>
		      <th>Produk</th>
		      <th>Total</th>
		  </tr>
		  <?php 
          $no = 0; foreach($value as $row_val) : $no ++; 
          $total[] = $row_val->total;
        ?>
		  <tr class="item-row">
		      <td class="item-name"><?php echo $this->tanggal->formatDateForm($row_val->trans_time)?></td>
		      <td><?php echo $row_val->booking_code?></td>
		      <td><?php echo ucwords($row_val->name_to)?></td>
		      <td><?php echo $row_val->partner_productname?></td>
		      <td><?php echo number_format($row_val->total)?></td>
		  </tr>
      <?php endforeach;?>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Total</td>
		      <td class="total-value"><div id="subtotal"><?php echo number_format(array_sum($total))?></div></td>
		  </tr>		
		</table>
		Terbilang : <i>"<?php $terbilang = new Kuitansi(); echo ucwords($terbilang->terbilang(array_sum($total)))?> Rupiah"</i>
		<div id="terms">
		  <h5>Terms</h5>
		  Pembayaran dilakukan minimal 3 hari setelah billing tagihan ini dibuat.
		</div>
	
	</div>
	
</body>

</html>