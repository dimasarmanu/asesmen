<!doctype html>
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="initial-scale=1.0" />
  <meta name="format-detection" content="telephone=no" />
  <title>e-Tiket Liburania</title>
  <style type="text/css">
 	body {
		width: 100%;
		margin: 0;
		padding: 0;
		-webkit-font-smoothing: antialiased;
	}
	@media only screen and (max-width: 600px) {
		table[class="table-row"] {
			float: none !important;
			width: 98% !important;
			padding-left: 20px !important;
			padding-right: 20px !important;
		}
		table[class="table-row-fixed"] {
			float: none !important;
			width: 98% !important;
		}
		table[class="table-col"], table[class="table-col-border"] {
			float: none !important;
			width: 100% !important;
			padding-left: 0 !important;
			padding-right: 0 !important;
			table-layout: fixed;
		}
		td[class="table-col-td"] {
			width: 100% !important;
		}
		table[class="table-col-border"] + table[class="table-col-border"] {
			padding-top: 12px;
			margin-top: 12px;
			border-top: 1px solid #E8E8E8;
		}
		table[class="table-col"] + table[class="table-col"] {
			margin-top: 15px;
		}
		td[class="table-row-td"] {
			padding-left: 0 !important;
			padding-right: 0 !important;
		}
		table[class="navbar-row"] , td[class="navbar-row-td"] {
			width: 100% !important;
		}
		img {
			max-width: 100% !important;
			display: inline !important;
		}
		img[class="pull-right"] {
			float: right;
			margin-left: 11px;
            max-width: 125px !important;
			padding-bottom: 0 !important;
		}
		img[class="pull-left"] {
			float: left;
			margin-right: 11px;
			max-width: 125px !important;
			padding-bottom: 0 !important;
		}
		table[class="table-space"], table[class="header-row"] {
			float: none !important;
			width: 98% !important;
		}
		td[class="header-row-td"] {
			width: 100% !important;
		}
	}
	@media only screen and (max-width: 480px) {
		table[class="table-row"] {
			padding-left: 16px !important;
			padding-right: 16px !important;
		}
	}
	@media only screen and (max-width: 320px) {
		table[class="table-row"] {
			padding-left: 12px !important;
			padding-right: 12px !important;
		}
	}
	@media only screen and (max-width: 608px) {
		td[class="table-td-wrap"] {
			width: 100% !important;
		}
	}
  </style>
 </head>
 <body style="font-family: Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;" bgcolor="#E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
 <table width="100%" height="100%" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
 <tr><td width="100%" align="center" valign="top" bgcolor="#E4E6E9" style="background-color:#E4E6E9; min-height: 200px;">
<table><tr><td class="table-td-wrap" align="center" width="608"><div style="font-family: Arial, sans-serif; line-height: 32px; color: #444444; font-size: 13px;">
  e-Tiket reservasi anda,
  <a href="#" style="color: #478fca; text-decoration: none; font-size: 14px; background-color: transparent;">
     Buka di halaman website &rarr;
  </a>
</div>


<?php for($i=1;$i<5;$i++) :?>
<!-- header -->
<table class="table-row" style="table-layout: auto; width: 600px; background-image: url('http://apps.liburania.com/assets/images/logo-header-2.png'); background-size:     cover;  background-repeat:   no-repeat; background-position: center center; " bgcolor="#FFFFFF" width="600" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr height="55px" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; height: 55px; ">
		   <td class="table-row-td" style="height: 55px; padding-right: 16px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; vertical-align: middle;" valign="middle" align="left">
		     <img src="http://apps.liburania.com/assets/images/lib-logo-white.png" height="50px">
		   </td>
	 
	   <td class="table-row-td" style="height: 55px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; text-align: right; vertical-align: top;" align="right" valign="middle">
		    <!-- <a href="#" style="color: #fff; text-decoration: none; font-size: 11px; background-color: transparent;">
			   <?php echo $i?>/5 Pcs
			</a> -->
	   </td>
		</tr>
	</tbody>
</table>
<!-- end header -->

<table class="table-space" height="6" style="height: 3px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-space-td" valign="middle" height="6" style="height: 3px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left">&nbsp;
			</td>
		</tr>
	</tbody>
</table>

<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;
			</td>
		</tr>
	</tbody>
</table>

<!-- main content -->
<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">

 				<table class="table-col" align="left" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
 					<tbody>
 						<tr>
 							<td class="table-col-td" width="30%" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="center">	
 								<center>
 									<div style="font-family: Arial, sans-serif; line-height: 15px; color: #444444; font-size: 13px; text-align: center;">
										<img src="http://apps.liburania.com/assets/qrcode/aminlubis23.png" style="border: 0px none #444444; vertical-align: middle; padding-bottom: 9px;width: 100%" hspace="0" vspace="0" border="0"><br>
										<span style="font-size:12px">Kode Tiket</span><br>
										<b style="font-size:18px">LIB0028JGL</b><br>
										
									</div>
 								</center>
								
 							</td>

 							<td class="table-col-td" width="70%" style="font-family: Arial, sans-serif; line-height: 13px; color: #444444; font-size: 11px; font-weight: normal;" valign="top" align="right">	
								
								<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px;width: 100%; padding-top:5px">
									<table class="table-row" style="table-layout: auto; padding-right: 24px; padding-left: 24px; width: 100%; background-color: #ffffff;" bgcolor="#FFFFFF" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											
											<tr style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px;">
												<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 15px; color: #444444; font-size: 12px; font-weight: normal;" valign="top" align="right">
													<img src="http://apps.liburania.com/assets/jungleland.png" width="150px"><br>
													<b>The Jungle Land Waterpark Adventure<br></b>
													<!-- Tickets - Easy Access<br>
													Admission Ticket - Easy Access</b><br> -->
													Jl. Jungleland Boulevard
													Kawasan Sentul Nirwana - Bogor 16810 
													Gedung Satria Buiding 2 
													(Golden Stick) Lt.2 No.204
													Jawa Barat, Indonesia<br>
													Telpon: (021) 29 311 313 <br>
													Fax : (021) 29 311 311 <br>
													Email: customer.service@jungleland.co.id

												</td>
											</tr>

										</tbody>
									</table>	
								</div>
 							</td>
 						</tr>
 					</tbody>
 				</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- end main content -->

<!-- personal info -->
<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">

 				<table class="table-col" align="left" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
 					<tbody>
 						<tr>
 							<td class="table-col-td" width="55%" style="font-family: Arial, sans-serif; line-height: 13px; color: #444444; font-size: 11px; font-weight: normal;" valign="top" align="right">	
								
								<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px;width: 100%; padding-top:5px">
									<table class="table-row" style="table-layout: auto; padding-right: 24px; padding-left: 8px; width: 100%; background-color: #ffffff;" bgcolor="#FFFFFF" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											
											<tr style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px;">
												<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 15px; color: #444444; font-size: 12px; font-weight: normal;" valign="top" align="left">
													Detail Pemesanan<br>
													<b><span style="font-size:16px">Muhammad Amin Lubis</span></b><br>
													als230990@gmail.com / 0858 1965 5296<br>
												</td>
											</tr>

										</tbody>
									</table>	
								</div>
 							</td>
 						</tr>
 					</tbody>
 				</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- end personal info -->

<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="center">&nbsp;
				<table bgcolor="#E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr>
							<td bgcolor="#E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>

<!-- booking detail -->
<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">

 				<table class="table-col" align="left" width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
 					<tbody>
 						<tr style="background-color:#b5b5b52e; text-align: middle;height: 60px">
							<td class="table-col-td" width="25%" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;padding-left:10px" valign="middle" align="left">	
								<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; ">
									<div style="line-height:13px;padding-bottom:8px">
										<span style="font-size:13px;">Kode</span>
										<br><span style="font-size:11px;">Booking</span><br>
									</div>
										
									<b style="font-size:14px">LB000123</b>
								</div>
							</td>

							<td class="table-col-td" width="25%" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;padding-left:10px" valign="middle" align="left">	
								<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; ">
									<div style="line-height:13px;padding-bottom:8px">
										<span style="font-size:13px;">Tanggal</span>
										<br><span style="font-size:11px;">Kunjungan</span><br>
									</div>
									<b style="font-size:14px"><?php echo date('d/m/Y')?></b>
								</div>
							</td>

							<td class="table-col-td" width="25%" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;padding-left:10px" valign="middle" align="left">	
								<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; ">
									<div style="line-height:13px;padding-bottom:8px">
										<span style="font-size:13px;">Masa</span>
										<br><span style="font-size:11px;">Berlaku</span><br>
									</div>
									<b style="font-size:14px"><?php echo date('d/m/Y')?></b>
								</div>
							</td>

							<td class="table-col-td" width="25%" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;padding-left:10px" valign="middle" align="left">	
								<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; ">
									<div style="line-height:13px;padding-bottom:8px">
										<span style="font-size:13px;">Hari</span>
										<br><span style="font-size:11px;">Kunjungan</span><br>
									</div>
									<b style="font-size:14px">Weekdays</b>
								</div>
							</td>

							<td class="table-col-td" width="25%" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;padding-left:10px" valign="middle" align="left">	
								<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; ">
									<div style="line-height:13px;padding-bottom:8px">
										<span style="font-size:13px;">Kategori</span>
										<br><span style="font-size:11px;">Tiket</span><br>
									</div>
									<b style="font-size:14px">Individual</b>
								</div>
							</td>
						</tr>
 					</tbody>
 				</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- end booking detail -->

<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="center">&nbsp;
				<table bgcolor="#E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr>
							<td bgcolor="#E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>

<!-- term and condition -->
<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 13px; color: #444444; font-size: 10px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			<p style="font-style:italic">
				Syarat dan Ketentuan :<br>
				<ul style="margin-left:-25px;font-style:italic">
					<li>e-Tiket ini berlaku untuk 1 x pakai, dan uang tidak dapat dikembalikan</li>
					<li>Harga Normal Pintu Masuk TMII, Tarif Masuk Kendaraan dan Obyek-obyek wisata di area TMI serta informasi umum lainnya mengacu pada website official Taman Mini Indonesia Indah</li>
				</ul>
			</p>
			</td>
		</tr>
	</tbody>
</table>
<!-- end term and condition -->

<!-- kontak kami -->
<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="right">

   				<table class="table-col" align="left" width="273" style="padding-right: 18px; table-layout: fixed;" cellspacing="0" cellpadding="0" border="0">
   					<tbody>
   						<tr>
   							<td class="table-col-td" width="150" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
								<table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
									<tbody>
										<tr>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 12px; padding-top: 10px;" valign="top" align="left">Telepon Kami
											</td>
										</tr>
										<tr>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 13px; padding-bottom: 8px;" valign="top" align="left">
												<img src="http://apps.liburania.com/assets/images/icon-phone.png" width="15px"> <b>+62 858 1965 5296</b>
											</td>
										</tr>
									</tbody>
								</table>
   							</td>

   							<td class="table-col-td" width="150" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
								<table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
									<tbody>
										<tr>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 12px; padding-top: 10px;" valign="top" align="left">Email Kami
											</td>
										</tr>
										<tr>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 13px; padding-bottom: 8px;" valign="top" align="left">
												<img src="http://apps.liburania.com/assets/images/mail-icon.png" width="15px" style="padding-top:2px">
												<b>cs@liburania.com</b>
											</td>
										</tr>
									</tbody>
								</table>
   							</td>

   							<td class="table-col-td" width="150" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="middle" align="center">
								<table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
									<tbody>
										<tr>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 13px; padding-bottom: 8px;" valign="middle" align="center">
												<a href="#" style="color: #ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid #d64727; padding: 4px 5px; font-size: 14px; line-height: 21px; background-color: #d64727;">
													Download e-Tiket »
												</a>
												<p style="font-size:11px;">Silahkan download tiket anda..!</p>
											</td>
										</tr>
									</tbody>
								</table>
   							</td>

					   	</tr>
					</tbody>
				</table>

			</td>
		</tr>
	</tbody>
</table>
<!-- end kontak kami -->

<!-- footer -->
<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>


<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left">&nbsp;</td></tr></tbody></table>

<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #dad7d7;" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #dad7d7; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
 				<table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
 					<tbody>
 						<tr>
   							<td class="table-col-td" width="150" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
								<table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
									<tbody>
										<tr>
											<td class="header-row-td" width="100" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 13px; padding-bottom: 8px;" valign="top" align="left">
												<img src="http://apps.liburania.com/assets/images/hand-2.png" width="80px">
											</td>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 13px; margin: 0px; padding-left:10px" valign="top" align="left">
												<p>
													<b style="font-size:15px;">Tidak Perlu Dicetak!</b><br>
													<span style="font-size:11px">
														Tunjukkan qrcode ini kepada partner kami pada tanggal kunjungan langsung
														dari smartphone Anda! Akses semua qrcode Anda dengan mudah selama
														bepergian. Untuk melihat pesanan dari perangkat lain, masuk ke akun Anda
														dengan email yang digunakan saat pemesanan.
													</span>
												</p>
												
											</td>
											<td class="header-row-td" width="100" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 13px; padding-bottom: 8px;padding-left: 30px" valign="middle" align="center">
												<span style="font-size:15px"><b>Available on</b></span>
												<img src="http://apps.liburania.com/assets/images/get-on-gplay.png" width="120px">
											</td>
										</tr>
									</tbody>
								</table>
   							</td>

   							<!-- <td class="table-col-td" width="150" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
								<table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
									<tbody>
										<tr>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 12px; padding-top: 10px;" valign="top" align="left">Email Kami
											</td>
										</tr>
										<tr>
											<td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; margin: 0px; font-size: 13px; padding-bottom: 8px;" valign="top" align="left">
												<img src="http://apps.liburania.com/assets/images/mail-icon.png" width="15px" style="padding-top:2px">
												<b>cs@liburania.com</b>
											</td>
										</tr>
									</tbody>
								</table>
   							</td> -->

					   	</tr>
 					</tbody>
 				</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- end footer -->

<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left">&nbsp;
			</td>
		</tr>
	</tbody>
</table>

<?php endfor; ?>
	

<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

<!-- header -->
<table class="table-row" style="table-layout: auto; width: 600px; background-image: url('http://apps.liburania.com/assets/images/logo-header-2.png'); background-size:     cover;  background-repeat:   no-repeat; background-position: center center; " bgcolor="#FFFFFF" width="600" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr height="55px" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; height: 55px; ">
		   <td class="table-row-td" style="height: 55px; padding-right: 16px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; vertical-align: middle;" valign="middle" align="left">
		     <img src="http://apps.liburania.com/assets/images/lib-logo-white.png" height="50px">
		   </td>
	 
	   <!-- <td class="table-row-td" style="height: 55px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; text-align: right; vertical-align: top;" align="right" valign="middle">
	     <a href="#" style="color: #fff; text-decoration: none; font-size: 15px; background-color: transparent;">
		   <b>Term and Condition</b>
		 </a>
	   </td> -->
		</tr>
	</tbody>
</table>
<!-- end header -->

<!-- content term and condition -->
<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
			<p style="font-size:15px;"><b>TERMS AND CONDITION</b></p>
			<ol style="text-align:justify"">
				<li>Harga Normal Pintu Masuk TMII, Tarif Masuk Kendaraan dan Obyek-obyek wisata di area TMI serta informasi umum lainnya mengacu pada website official Taman Mini Indonesia Indah</li>
				<li>Jam Operasional Taman Mini Indonesia Indah :<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Pintu Masuk TMII setiap hari 08.00 &ndash; 22.00<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Anjungan, Museum dan Wahana setiap hari 09.00 &ndash; 16.00 &nbsp;</li>
				<li>&nbsp;Tarif Masuk Perorangan :<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Normal Rp. 20.000<br />Tarif Normal Masuk Kendaraan :<br />&nbsp; &nbsp; &nbsp; &nbsp; - Mobil Rp. 15.000<br />&nbsp; &nbsp; &nbsp; &nbsp; - Sepeda Motor Rp. 10.000<br />&nbsp; &nbsp; &nbsp; &nbsp; - Bus / Truk Rp. 35.000&nbsp;<br />&nbsp; &nbsp; &nbsp; &nbsp; - Sepeda Rp. 1.000</li>
				<li>E-Ticket hanya berlaku jika dibeli secara online melalui <a href="https://ticket.tamanmini.com">https://ticket.tamanmini.com</a> ataupun Distributor resmi yang terdaftar dalam website kami berlaku selama 10 (sepuluh) hari terhitung dari tanggal kedatangan yang ditentukan</li>
				<li>Setiap e-Ticket dengan barcode unik yang telah digunakan sebelumnya tidak dapat digunakan lagi, maka pastikan kerahasiaan kode booking dan kode tiket anda sampai dengan tanggal kedatangan.</li>
				<li>E-Ticket ini bersifat non-kertas (paperless), customer hanya perlu menunjukan kode booking berupa QR code kepada petugas di loket wahana/museum/obyek wisata.</li>
				<li>&nbsp;E-Ticket yang berhasil dipesan dikirim melalui email customer berupa lampiran .pdf</li>
				<li>Setiap E-Ticket berlaku untuk masuk ke tempat pertunjukan/wahana untuk 1 (satu) orang</li>
				<li>Pihak E-Ticketing Taman Mini Indonesia Indah berhak untuk memproses secara hukum sesuai dengan ketentuan perundangan yang berlaku di Indonesia baik secara perdata maupun secara pidana terhadap orang-orang yang memperoleh e-Ticket secara tidak sah sesuai dengan butir 4 dan 5 ketentuan ini, serta mengambil tindakan untuk melarang/menolak masuk ke area Taman Mini Indonesia Indah</li>
				<li>Pihak E-Ticketing Taman Mini Indonesia Indah tidak bertanggung jawab atas kelalaian pembeli tiket yang mengakibatkan E-voucher/E-Ticket jatuh ke tangan orang lain (dalam penguasaan orang lain) untuk digunakan sebagai tanda masuk ke &nbsp;area TMII atau wahana yang tercantum didalamnya, sehingga menghilangkan hak pembeli untuk masuk dan menggunakan e-Ticket tersebut</li>
				<li>E-Ticket TIDAK dapat diuangkan (no-refundable) ataupun digunakan kembali diluar tanggal yang berlaku</li>
				<li>Pembeli e-Ticket ini menyatakan melepaskan hak hukum untuk mengajukan tuntutan balik melalui pengadilan atau cara-cara apapun yang diperkenankan secara hukum untuk menuntut pihak e-Ticketing Taman Mini Indonesia Indah dalam hal terjadi pembatalan yang dilakukan secara sepihak oleh pihak artis atau pemerintah atau sebab-sebab lain diluar kemampuan dan kehendak pihak Taman Mini Indonesia Indah</li>
				<li>Jika terjadi pembatalan pelayanan jasa/pertunjukan di wahana, museum dan anjungan di kemudian hari maka pembeli akan mendapatkan penjadwalan ulang kunjungan sesuai dengan kesepakatan pihak wahana, museum dan anjungan tidak termasuk biaya pintu masuk dan kendaraan</li>
				<li>Biaya convenient fee yang dibebankan kepada pembeli dengan kartu kredit tidak akan dikembalikan</li>
				<li>Dilarang membawa dan menggunakan segala jenis obat-obatan terlarang termasuk tapi tidak terbatas dengan narkoba atau sejenisnya</li>
				<li>Dilarang membawa senjata api dan senjata tajam serta benda-benda yang dilarang berdasakan ketentuan perundangan yang berlaku di area rekreasi ke dalam area Taman Mini Indonesia Indah</li>
				<li>Demi kenyamanan rekreasi di area Taman Mini Indonesia Indah, jam efektif penggunaan e-Ticket yang telah dipesan adalah pukul 09.00 &ndash; 16.00</li>
				<li>Tiket individu dengan jumlah pembelian 1-29 pax dapat dilakukan minimal 1 (satu) hari sebelum tanggal kedatangan di loket</li>
				<li>Khusus tiket rombongan dengan jumlah pembelian diatas 30 pax digunakan pada 3 (tiga) hari sebelum tanggal kunjungan dilakukan dan melakukan konfirmasi kunjungan di loket rombongan (jalur antrian sebelah kanan di pintu masuk TMII)</li>
				<li>Seluruh rombongan diatas 30 pax yang telah melakukan pembelian tiket (issued ticket) akan otomatis terdaftar di layar berisi informasi rombongan yang terdapat di loket rombongan di pintu masuk TMII</li>
			</ol>

			</td>
		</tr>
	</tbody>
</table>
<!-- end content term and condition -->

<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left">&nbsp;</td></tr></tbody></table>

<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
 <table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="528" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
	 <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
	 <div style="font-family: Arial, sans-serif; line-height: 19px; color: #777777; font-size: 14px; text-align: center;">&copy; <?php echo date('Y')?> by liburania.com</div>
	 <table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
	 <!-- <div style="font-family: Arial, sans-serif; line-height: 19px; color: #bbbbbb; font-size: 13px; text-align: center;">
		<a href="#" style="color: #428bca; text-decoration: none; background-color: transparent;">Terms</a>
		&nbsp;|&nbsp;
		<a href="#" style="color: #428bca; text-decoration: none; background-color: transparent;">Privacy</a>
		&nbsp;|&nbsp;
		<a href="#" style="color: #428bca; text-decoration: none; background-color: transparent;">Unsubscribe</a>
	 </div> -->
	 <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 528px; background-color: #ffffff;" width="528" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
 </td></tr></tbody></table>
</td></tr></tbody></table>
<table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left">&nbsp;</td></tr></tbody></table></td></tr></table>
</td></tr>
 </table>
 </body>
 </html>