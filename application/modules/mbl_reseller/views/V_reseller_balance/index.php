<style>
.table-custom {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 70%;
}

.table-custom td, .table-custom th {
  border: 1px solid #ddd;
  padding: 5px;
}

.table-custom tr:nth-child(even){background-color: #f2f2f2;}

.table-custom tr:hover {background-color: #ddd;}

.table-custom th {
  padding-top: 8px;
  padding-bottom: 8px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<script>

$(document).ready(function(){

	function find_data_reload(result){
	  oTable.ajax.url('mbl_reseller/C_reseller_balance/get_data?'+result.data).load();
	}
  
	$('#btn_reset_data').click(function (e) {
	  e.preventDefault();
	  oTable.ajax.url('mbl_reseller/C_reseller_balance/get_data').load();

	  $('#form_search')[0].reset();
	});
  
	$('select[name="company_id"]').change(function () {
		if ($(this).val()) {
			$.getJSON("Templates/References/AgentByCompany/" + $(this).val(), '', function (data) {
				$('#account_id option').remove();
				$('<option value="">-Pilih Agent-</option>').appendTo($('#account_id'));
				$.each(data, function (i, o) {
					$('<option value="' + o.id + '">' + o.user_name + '</option>').appendTo($('#account_id'));
				});
			});
		} else {
			$('#account_id option').remove()
		}
	});

});
</script>
<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

    <form class="form-horizontal" method="post" id="form_search" action="mbl_reseller/C_reseller_balance/find_data">

    <div class="row">
      <div class="col-md-12">
        <center>
          <h4>RESELLER BALANCE AND TRANSACTION HISTORY<br>
            <small style="font-size:12px"> Silahkan lakukan pencarian data dengan parameter dibawah ini </small>
          </h4>
        </center>
        <div class="form-group">
            <label class="control-label col-md-2">Distributor</label>
            <div class="col-md-2">
              <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=>array(), 'id'=>'id', 'name' => 'name'), '' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
            </div>
			<!--<label class="control-label col-md-1">Agent</label>
            <div class="col-md-3">
              <select name="account_id" id="account_id" class="form-control">
                <option value="0">-Pilih Agent</option>
              </select>
            </div>
			-->
        </div>
		<div class="form-group">
          <label class="control-label col-md-2 ">&nbsp;</label>
          <div class="col-md-10" style="margin-left: 6px">
            <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
              <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
              Search
            </a>
            <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
              <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Reset
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- div.dataTables_borderWrap -->
    <div style="margin-top:-30x">
      <table id="dynamic-table" base-url="mbl_reseller/C_reseller_balance" url-detail="mbl_reseller/C_reseller_balance/show_detail" class="table table-striped table-bordered table-hover">
       <thead>
        <tr>  
          <th width="30px" class="center"></th>
          <th width="40px"></th>
          <th width="30px" class="center"></th>
          <th width="50px">ID</th>
          <th>RESELLER NAME</th>
		  <th>COMPANY</th>
          <th align="center" width="120px">AMOUNT</th>
          <th align="center" width="120px">ISSUED</th>
          <th align="center" width="120px">BALANCE</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    </div>

    </form>
  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_with_detail.js'?>"></script>



