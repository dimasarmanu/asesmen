<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->
	<form class="form-horizontal" method="post" id="form_search" action="mbl_reseller/C_news/find_data">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Distributor</label>
					<div class="col-md-3">
						<?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=> ($this->session->userdata('user')->role != 1) ? array('id' => $this->session->userdata('user')->company_id) : array(), 'id'=>'id', 'name' => 'name'), '' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
					</div>
				</div>
				<div class="form-group">
				  <label class="control-label col-md-2 ">&nbsp;</label>
				  <div class="col-md-10" style="margin-left: 6px">
					<a href="#" id="btn_search_data" class="btn btn-xs btn-default">
					  <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
					  Search
					</a>
					<a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
					  <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
					  Reset
					</a>
				  </div>
				</div>
			</div>
		</div>
		<div class="clearfix" style="margin-bottom:-5px">
		  <?php echo $this->authuser->show_button('mbl_reseller/C_news','C','',1)?>
		  <?php echo $this->authuser->show_button('mbl_reseller/C_news','D','',5)?>
		  <div class="pull-right tableTools-container"></div>
		</div>
		<hr class="separator">
		<!-- div.table-responsive -->

		<!-- div.dataTables_borderWrap -->
		<div style="margin-top:-16px">
			<table id="dynamic-table" base-url="mbl_reseller/C_news" url-detail="mbl_reseller/C_news/show_detail" class="table table-striped table-bordered table-hover">
			   <thead>
				<tr>  
				  <th width="30px" class="center"></th>
				  <th width="40px" class="center"></th>
				  <th width="40px" class="center"></th>
				  <th width="40px"></th>
				  <th width="70px">ID</th>
				  <th width="100px">Distributor</th>
				  <th width="100px">Image</th>
				  <th>Title</th>
				  <th>Summary</th>
				  <th width="100px">Status</th>
				  <th width="180px">Last Update</th>
				</tr>
			  </thead>
			  <tbody>
			  </tbody>
			</table>
		</div>
	</form>
  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_with_detail.js'?>"></script>



