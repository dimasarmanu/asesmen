<script src="<?php echo base_url()?>assets/js/typeahead.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){

  get_total_mutasi();

  oTable = $('#dynamic-table').DataTable({ 
          
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "ordering": false,
    "searching": true,
    "bPaginate": true,
    "bInfo": true,
    "pageLength": 50,
    "ajax": {
        "url": $('#dynamic-table').attr('base-url'),
        "type": "POST"
    },

  });

  $('#btn_search_data').click(function (e) {
    
      e.preventDefault();
      $.ajax({
      url: 'mbl_reseller/C_mutasi/find_data',
      type: "post",
      data: $('#form_search').serialize(),
      dataType: "json",
      beforeSend: function() {
        achtungShowLoader();  
      },
      success: function(data) {
        achtungHideLoader();
        find_data_reload(data);
      }
    });
  });

  $('#btn_reset_data').click(function (e) {
      e.preventDefault();
      oTable.ajax.url('mbl_reseller/C_mutasi/get_data').load();

      $('#form_search')[0].reset();
  });

  $('#btn_export_excel').click(function (e) {
      e.preventDefault();
      $.ajax({
        url: 'mbl_reseller/C_mutasi/find_data',
        type: "post",
        data: $('#form_search').serialize(),
        dataType: "json",
        beforeSend: function() {
          achtungShowLoader();  
        },
        success: function(data) {
          achtungHideLoader();
          export_excel(data);
        }
      });

    });


});


function find_data_reload(result){
  get_total_mutasi();
  oTable.ajax.url('mbl_reseller/C_mutasi/get_data?'+result.data).load();

}

function export_excel(result){

  window.open('mbl_reseller/C_mutasi/export_excel?'+result.data+'','_blank');  

}


function get_total_mutasi(){

  $.ajax({
    url: 'mbl_reseller/C_mutasi/find_data',
    type: "post",
    data: $('#form_search').serialize(),
    dataType: "json",
    success: function(result) {
      $.getJSON("mbl_reseller/C_mutasi/get_total_mutasi?" +result.data, '', function (response) {
          $('#total_in').text(formatMoney(response.total_in));
          $('#total_out').text(formatMoney(response.total_out));
          $('#total_balance').text(formatMoney(response.total_balance));
      });
    }
  });

}

</script>

<div class="row">
  <div class="col-xs-12">

    <div class="page-header">
      <h1>
        <?php echo $title?>
        <small>
          <i class="ace-icon fa fa-angle-double-right"></i>
          <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
        </small>
      </h1>
    </div><!-- /.page-header -->

    <form class="form-horizontal" method="post" id="form_search" action="#">

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
            <label class="control-label col-md-2">Distributor</label>
            <div class="col-md-2">
              <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_company', 'where'=>array(), 'id'=>'id', 'name' => 'name'), '' ,'company_id','company_id','chosen-slect form-control','','', 'db_mbr');?>
            </div>
        </div>
        <hr>
        <p><b>PENCARIAN BERDASARKAN WAKTU TRANSAKSI</b></p>
        <div class="form-group">
          <label class="control-label col-md-2">Tanggal</label>
            <div class="col-md-2">
              <div class="input-group">
                <input class="form-control date-picker" name="from_tgl" id="from_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                <span class="input-group-addon">
                  <i class="fa fa-calendar bigger-110"></i>
                </span>
              </div>
            </div>

            <label class="control-label col-md-1">s/d Tanggal</label>
            <div class="col-md-2">
              <div class="input-group">
                <input class="form-control date-picker" name="to_tgl" id="to_tgl" type="text" data-date-format="yyyy-mm-dd" value=""/>
                <span class="input-group-addon">
                  <i class="fa fa-calendar bigger-110"></i>
                </span>
              </div>
            </div>
        </div>
        
        <div class="form-group">
          <label class="control-label col-md-2 ">&nbsp;</label>
          <div class="col-md-10" style="margin-left: 6px">
            <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
              <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
              Search
            </a>
            <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
              <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Reset
            </a>
            <a href="#" id="btn_export_excel" class="btn btn-xs btn-success">
              <i class="fa fa-file-word-o bigger-110"></i>
              Export Excel
            </a>
          </div>
        </div>

      </div>
    </div>
    <hr class="separator">
    <!-- div.table-responsive -->
    <div class="row" style="margin-top: -16px;">
      <div class="pull-left"><h3>Riwayat Mutasi</h3></div>

      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: gold">
        <span style="font-size: 14px">Balance</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_balance"></span></h3>
      </div>
      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: darkorange">
        <span style="font-size: 14px">Total Out</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_out"></span></h3>
      </div>
      <div class="pull-right" style="border-left: 1px solid #b2b3b5; padding-left: 10px; padding-right: 10px; background: chartreuse">
        <span style="font-size: 14px">Total In</span>
        <h3 style="font-weight: bold; margin-top : 0px"><span id="total_in"></span></h3>
      </div>
      
    </div>
    <!-- div.dataTables_borderWrap -->
    <div class="row" style="margin-top: -2px">
      <table id="dynamic-table" base-url="mbl_reseller/C_mutasi/get_data" url-detail="mbl_reseller/C_mutasi/show_detail" class="table table-striped table-bordered table-hover">
       <thead>
        <tr>  
          <th width="30px" class="center"></th>
          <th>Name</th>
		  <th>Company</th>
          <th>Date</th>
          <th>In</th>
          <th>Out</th>
          <th>Balance</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    </div>

    </form>

  </div><!-- /.col -->
</div><!-- /.row -->





