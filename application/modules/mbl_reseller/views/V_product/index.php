
<div class="row">
<div class="col-xs-12">

<div class="page-header">
  <h1>
	<?php echo $title?>
	<small>
	  <i class="ace-icon fa fa-angle-double-right"></i>
	  <?php echo isset($breadcrumbs)?$breadcrumbs:''?>
	</small>
  </h1>
</div><!-- /.page-header -->
	<form class="form-horizontal" method="post" id="form_search" action="mbl_reseller/C_product/find_data">
		<!--<b>Pencarian Data Harga</b><br><br>
		<div class="form-group">
			<label class="control-label col-md-2">Product Type</label>
            <div class="col-md-2">
             <?php echo $this->master->custom_selection_no_database(array('B2B', 'B2C'),isset($value)?$value->product_type:'','product_type','product_type','chosen-slect form-control','','', 'db_mbr');?>
            </div>
		</div>
		<div class="form-group">
          <label class="control-label col-md-2 ">&nbsp;</label>
          <div class="col-md-10" style="margin-left: 6px">
            <a href="#" id="btn_search_data" class="btn btn-xs btn-default">
              <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
              Search
            </a>
            <a href="#" id="btn_reset_data" class="btn btn-xs btn-warning">
              <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Reset
            </a>
          </div>
        </div>		
		-->
		<div class="form-group">
		
		</div>
		<div class="clearfix" style="margin-bottom:-5px">
		  <?php echo $this->authuser->show_button('mbl_reseller/C_product','C','',1)?>
		  <?php echo $this->authuser->show_button('mbl_reseller/C_product','D','',5)?>
		  <div class="pull-right tableTools-container"></div>
		</div>
	
		<hr class="separator">
		<div class="clearfix">
		<!-- div.table-responsive -->

		<!-- div.dataTables_borderWrap -->
		<div style="margin-top:-18px;">
		<table id="dynamic-table" base-url="mbl_reseller/C_product" url-detail="mbl_reseller/C_product/show_detail" class="table table-striped table-bordered table-hover">
	   <thead>
		<tr>  
		  <th width="30px" class="center"></th>
		  <th width="40px" class="center"></th>
		  <th width="40px" class="center"></th>
		  <th width="40px"></th>
		  <th width="70px">ID</th>
		  <th>Product Name</th>
		  <th>Principal</th>
		  <th width="80px" style="text-align: center">Min Order</th>
		  <th width="80px" style="text-align: center">Max Order</th>
		  <th width="80px" style="text-align: center">Min Group</th>
		  <th width="100px">Status</th>
		  <th width="180px">Last Update</th>
		</tr>
	  </thead>
	  <tbody>
	  </tbody>
	</table>
		</div>
		</div><!-- /.col -->
	</form>
	
</div><!-- /.row -->


<script src="<?php echo base_url().'assets/js/custom/als_datatable_with_detail.js'?>"></script>



