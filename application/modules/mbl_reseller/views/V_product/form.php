<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<script>
jQuery(function($) {

  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});

$(document).ready(function(){
  
    $('#form_product').ajaxForm({
      beforeSend: function() {
        achtungShowLoader();  
      },
      uploadProgress: function(event, position, total, percentComplete) {
      },
      complete: function(xhr) {     
        var data=xhr.responseText;
        var jsonResponse = JSON.parse(data);

        if(jsonResponse.status === 200){
          $.achtung({message: jsonResponse.message, timeout:5});
          $('#page-area-content').load('mbl_reseller/C_product?_=' + (new Date()).getTime());
        }else{
          $.achtung({message: jsonResponse.message, timeout:5});
        }
        achtungHideLoader();
      }
    }); 
})

</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_product" action="<?php echo site_url('mbl_reseller/C_product/process')?>" enctype="multipart/form-data">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                  
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Principal</label>
                  <div class="col-md-3">
                    <?php echo $this->master->custom_selection_with_db_selected(array('table'=>'m_principals', 'where'=>array('is_active' => 'Y'), 'id'=>'id', 'name' => 'principal_name'), isset($value)?$value->principal_id:'','principal_id','principal_id','chosen-slect form-control',($flag=='read')?'readonly':'','', 'db_mbr');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Product Name</label>
                  <div class="col-md-4">
                    <input name="partner_productname" id="partner_productname" value="<?php echo isset($value)?$value->partner_productname:''?>" placeholder="" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>
				
                <div class="form-group">
                  <label class="control-label col-md-2">Image</label>
                  <div class="col-md-3">
                    <input name="file_image" id="file_image" placeholder="" class="form-control" type="file">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Description</label>
                  <div class="col-md-4">
                    <textarea class="form-control" name="product_desc" id="product_desc" style="height: 100px !important" <?php echo ($flag=='read')?'readonly':''?>><?php echo isset($value)?$value->product_desc:''?></textarea>
                  </div>
                </div>
                <p><b>PERIOD OF VALIDITY</b></p>
                <div class="form-group">
                  <label class="control-label col-md-2">Masa Berlaku Tiket</label>
                  <div class="col-md-1">
                    <input name="day_expired" id="day_expired" value="<?php echo isset($value)?$value->day_expired:1?>" class="form-control" type="text">
                  </div>
                  <span style="padding-top: 8px">Hari</span>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Start date</label>
                    <div class="col-md-2">
                      <div class="input-group">
                        <input class="form-control date-picker" name="start_date" id="start_date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value)?$value->start_date:''?>"/>
                        <span class="input-group-addon">
                          <i class="fa fa-calendar bigger-110"></i>
                        </span>
                      </div>
                    </div>

                    <label class="control-label col-md-1">Expired Date</label>
                    <div class="col-md-2">
                      <div class="input-group">
                        <input class="form-control date-picker" name="expired_date" id="expired_date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value)?$value->expired_date:''?>"/>
                        <span class="input-group-addon">
                          <i class="fa fa-calendar bigger-110"></i>
                        </span>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Min Order</label>
                  <div class="col-md-1">
                    <input name="min_buy" id="min_buy" value="<?php echo isset($value)?$value->min_buy:''?>" placeholder="" style="text-align: center" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                  <label class="control-label col-md-1">Max Order</label>
                  <div class="col-md-1">
                    <input name="max_buy" id="max_buy" value="<?php echo isset($value)?$value->max_buy:''?>" placeholder="" style="text-align: center" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                  <label class="control-label col-md-2">Min Group Order</label>
                  <div class="col-md-1">
                    <input name="group_min" id="group_min" value="<?php echo isset($value)?$value->group_min:''?>" placeholder="" style="text-align: center" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Min Hari H Pemesanan</label>
                  <div class="col-md-1">
                    <input name="visitdate_transdate_min" id="visitdate_transdate_min" value="<?php echo isset($value)?$value->visitdate_transdate_min:''?>" placeholder="" style="text-align: center" class="form-control" type="text" <?php echo ($flag=='read')?'readonly':''?> >
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Is active?</label>
                  <div class="col-md-2">
                    <div class="radio">
                          <label>
                            <input name="is_active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->is_active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?> <?php echo ($flag=='read')?'readonly':''?> />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="is_active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->is_active == 'N') ? 'checked="checked"' : '' : ''; ?> <?php echo ($flag=='read')?'readonly':''?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Last update</label>
                  <div class="col-md-8" style="padding-top:8px;font-size:11px">
                      <i class="fa fa-calendar"></i> <?php echo isset($value->updated_date)?$this->tanggal->formatDateTime($value->updated_date):isset($value)?$this->tanggal->formatDateTime($value->created_date):date('d-M-Y H:i:s')?> - 
                      by : <i class="fa fa-user"></i> <?php echo isset($value->updated_by)?$value->updated_by:isset($value->created_by)?$value->created_by:$this->session->userdata('user')->username?>
                  </div>
                </div>


                <div class="form-actions center">

                  <a onclick="getMenu('mbl_reseller/C_product')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <?php if($flag != 'read'):?>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                <?php endif; ?>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


