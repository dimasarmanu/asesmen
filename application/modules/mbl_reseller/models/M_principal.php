<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_principal extends CI_Model {

	var $table = 'm_principals';
	var $column = array('m_principals.principal_name');
	var $select = 'm_principals.*,m_principalcategories.category_name as category';

	var $order = array('m_principals.id' => 'DESC');

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
	}

	private function _main_query(){
		$this->db_mbr->select($this->select);
		$this->db_mbr->from($this->table);
		$this->db_mbr->join('m_principalcategories','m_principalcategories.id=m_principals.principalcategory_id','left');
	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db_mbr->like($item, $_POST['search']['value']) : $this->db_mbr->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_mbr->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_mbr->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_mbr->limit($_POST['length'], $_POST['start']);
		$query = $this->db_mbr->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_mbr->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_main_query();
		return $this->db_mbr->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->_main_query();
		if(is_array($id)){
			$this->db_mbr->where_in(''.$this->table.'.id',$id);
			$query = $this->db_mbr->get();
			return $query->result();
		}else{
			$this->db_mbr->where(''.$this->table.'.id',$id);
			$query = $this->db_mbr->get();
			return $query->row();
		}
		
	}

	public function save($table, $data)
	{
		$this->db_mbr->insert($table, $data);
		return $this->db_mbr->insert_id();
	}

	public function update($where, $data)
	{
		$this->db_mbr->update($this->table, $data, $where);
		return $this->db_mbr->affected_rows();
	}

	public function delete_by_id($id)
	{
		$get_data = $this->get_by_id($id);
		if( $this->delete_image_default($get_data[0]) ){
			$this->db_mbr->where_in(''.$this->table.'.id', $id);
			return $this->db_mbr->delete($this->table);
		}else{
			return false;
		}
		
	}

	public function delete_image_default($data){
		/*print_r($data);die;*/
		/*if file images exist*/
		if ( file_exists(PATH_MBR.$data->logo) ) {
			if($data->logo != NULL){
				/*delete first logo file*/
	            unlink(PATH_MBR.$data->logo);
			}
        }
        return true;
	}


	public function list_fields(){
		return $this->db_mbr->list_fields( $this->table );
	}

	public function get_gallery($principal_id, $type){
		return $this->db_mbr->get_where('t_gallery', array('principal_id' => $principal_id, 'gallery_type' => $type) )->result();
	}


}
