<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_supplier_balance extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
		/*get data from session*/
		$this->company_id = $this->session->userdata('user')->company_id;
	}

	public function get_balance(){
		// if( $this->session->userdata('user')->user_id != 1 ){
		// 	$this->db_mbr->where( array('company_id' => $this->company_id ) );
		// }

		$this->db_mbr->select('v_principalbalances.*');
		$this->db_mbr->select('(SELECT total FROM v_sum_principal_trans_by_price_type WHERE principal_id=v_principalbalances.principal_id AND datepricetype_id="WEEKDAY") as weekday');
		$this->db_mbr->select('(SELECT total FROM v_sum_principal_trans_by_price_type WHERE principal_id=v_principalbalances.principal_id AND datepricetype_id="WEEKEND") as weekend');
		$this->db_mbr->select('(SELECT total FROM v_sum_principal_trans_by_price_type WHERE principal_id=v_principalbalances.principal_id AND datepricetype_id="HIGHSEASON") as highseason');
		return $this->db_mbr->get('v_principalbalances')->result();
	}


}
