<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_wlb_price_setting extends CI_Model {

	var $table = 'v_priceconcat';
	var $column = array('v_priceconcat.partner_productname','v_priceconcat.principal_name','v_priceconcat.datepricetype_id');
	var $select = 'v_priceconcat.*';

	var $order = array('v_priceconcat.updated_date' => 'DESC', 'v_priceconcat.id' => 'DESC');

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
	}

	private function _main_query(){
		$this->db_mbr->select($this->select);
		$this->db_mbr->from($this->table);
		$this->db_mbr->where('behalf_accounttype_id IS NOT NULL');
	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();
		
		if ( isset($_GET['company_id']) AND $_GET['company_id'] != '' ) {
			$this->db_mbr->where('v_priceconcat.company_id', $_GET['company_id']);
		}
		
		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db_mbr->like($item, $_POST['search']['value']) : $this->db_mbr->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_mbr->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_mbr->order_by(key($order), $order[key($order)]);
		}
		/*search by reseller*/
        if (isset($_GET['company_id']) AND $_GET['company_id'] != 0) {
            $this->db_mbr->where('company_id='.$_GET['company_id'].'');	
		}
		
		/*search by reseller*/
        if (isset($_GET['principal_id']) AND $_GET['principal_id'] != 0) {
            $this->db_mbr->where('principal_id='.$_GET['principal_id'].'');	
		}
		
		/*search by reseller*/
        if (isset($_GET['accounttype_id']) AND $_GET['accounttype_id'] != 0) {
            $this->db_mbr->where('accounttype_id='.$_GET['accounttype_id'].'');	
		}
		
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_mbr->limit($_POST['length'], $_POST['start']);
		$query = $this->db_mbr->get();
		//echo $this->db_mbr->last_query();
		//die;
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_mbr->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_main_query();
		return $this->db_mbr->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db_mbr->select('v_prices.*');
		$this->db_mbr->from('v_prices ');
		$this->db_mbr->where('behalf_accounttype_id IS NOT NULL');
		if(is_array($id)){
			$this->db_mbr->where_in('v_prices.id',$id);
			$query = $this->db_mbr->get();
			return $query->result();
		}else{
			$this->db_mbr->where('v_prices.id',$id);
			$query = $this->db_mbr->get();
			return $query->row();
		}
		
	}

	public function get_by_params($params)
	{
		$this->db_mbr->select('v_prices.*');
		$this->db_mbr->from('v_prices ');
		$this->db_mbr->where('behalf_accounttype_id IS NOT NULL');
		$this->db_mbr->where( $params );
		$query = $this->db_mbr->get();
		$getData = array('dt_header' => $query->row());
		foreach ($query->result() as $key => $value) {
			$getData[$value->datepricetype_id] = $value;
		}
		return $getData;
		
	}

	public function save($data)
	{
		$this->db_mbr->insert('m_prices', $data);
		return $this->db_mbr->insert_id();
	}

	public function update($where, $data)
	{
		$this->db_mbr->update('m_prices', $data, $where);
		return $this->db_mbr->affected_rows();
	}

	public function delete_by_id($id)
	{
		$get_data = $this->get_by_id($id);
		// print_r($get_data);die;
		return $this->db_mbr->delete('m_prices', array('product_id' => $get_data[0]->product_id, 'accounttype_id' => $get_data[0]->accounttype_id) );
		
	}

	public function list_fields(){
		return $this->db_mbr->list_fields( 'v_prices' );
	}

	public function get_price_type(){
		return $this->db_mbr->get_where( 'global_parameter', array('flag' => 'price_type') )->result();
	}

	public function check_existing_by_agenttype_and_product(){
		return $this->db_mbr->get_where( 'm_prices', array('accounttype_id' => $_GET['agent_type'], 'product_id' => $_GET['product'], 'product_type' => $_GET['product_type'], 'price_type' => $_GET['price_type']) );
	}

	public function get_reff_price($params){
		return $this->db_mbr->get_where('v_pricesactivenonactive', $params )->result();
	}


}
