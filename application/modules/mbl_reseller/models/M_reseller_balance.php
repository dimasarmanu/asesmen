<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_reseller_balance extends CI_Model {

	var $table = 'v_deposits';
	var $column = array('v_deposits.user_name' );
	var $select = 'v_deposits.*';
	var $order = array('v_deposits.user_name' => 'ASC');

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
	}

	private function _main_query(){
		$this->db_mbr->select($this->select);
		$this->db_mbr->from($this->table);
	}

	private function _get_datatables_query()
	{
		$this->_main_query();

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db_mbr->like($item, $_POST['search']['value']) : $this->db_mbr->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_mbr->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_mbr->order_by(key($order), $order[key($order)]);
		}
		   /*search by reseller*/
        if (isset($_GET['account_id']) AND $_GET['account_id'] != 0) {
            $this->db_mbr->where('account_id='.$_GET['account_id'].'');	
		}
		// search by company id
		if (isset($_GET['company_id']) AND $_GET['company_id'] != 0) {
            $this->db_mbr->where('company_id='.$_GET['company_id'].'');	
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db_mbr->limit($_POST['length'], $_POST['start']);
		$query = $this->db_mbr->get();
		//print_r($this->db_mbr->last_query());die;
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_mbr->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_main_query();
		return $this->db_mbr->count_all_results();
	}

	public function list_fields(){
		return $this->db_mbr->list_fields( $this->table );
	}

	public function get_by_id($id)
	{
		$this->_main_query();
		if(is_array($id)){
			$this->db_mbr->where_in(''.$this->table.'.account_id',$id);
			$query = $this->db_mbr->get();
			return $query->result();
		}else{
			$this->db_mbr->where(''.$this->table.'.account_id',$id);
			$query = $this->db_mbr->get();
			return $query->row();
		}
		
	}

	public function show_detail_row_table( $id ){
		// get data transaksi
		$result = $this->db_mbr->get_where('v_requestticket', array('account_id' => $id) )->result();
		$result_deposit = $this->db_mbr->get_where('v_deposithistories', array('account_id' => $id) )->result();
		$html = '';
		// riwayat transaksi
		$table = '';
		$table .= '<b>RIWAYAT TRANSAKSI</b><br>';
		$table .= '<table width="80%" class="table-custom">';
		$table .= '<thead>';
		$table .= '<tr>';
		$table .= '<th style="text-align: center">NO</th>';
		$table .= '<th>BOOKING CODE</th>';
		$table .= '<th>TRANS TIME</th>';
		$table .= '<th>PRINCIPAL</th>';
		$table .= '<th>PRODUCT</th>';
		$table .= '<th>PRICE</th>';
		$table .= '<tr>';
		$table .= '</thead>';
		$no=0; 
		$arr_total = [];
		foreach( $result as $row_dt ){ $no++;
			$table .= '<tr>';
			$table .= '<td align="center">'.$no.'</td>';
			$table .= '<td>'.$row_dt->booking_code.'</td>';
			$table .= '<td>'.$row_dt->trans_time.'</td>';
			$table .= '<td>'.$row_dt->principal_name.'</td>';
			$table .= '<td>'.$row_dt->partner_productname.'</td>';
			$table .= '<td align="right">'.number_format($row_dt->total).'</td>';
			$table .= '<tr>';
			$arr_total[] = $row_dt->total;
		}
			$table .= '<tr>';
			$table .= '<td colspan="5" align="right"><b>Issued Total</b></td>';
			$table .= '<td align="right">'.number_format(array_sum($arr_total)).'</td>';
			$table .= '<tr>';
		$table .= '<table>';

		// riwayat deposit
		$table_depo = '';
		$table_depo .= '<b>RIWAYAT DEPOSIT</b><br>';
		$table_depo .= '<table width="80%" class="table-custom">';
		$table_depo .= '<thead>';
		$table_depo .= '<tr>';
		$table_depo .= '<th style="text-align: center">NO</th>';
		$table_depo .= '<th>Payment Ref</th>';
		$table_depo .= '<th>Date</th>';
		$table_depo .= '<th>Bank</th>';
		$table_depo .= '<th>Currency</th>';
		$table_depo .= '<th>Amount</th>';
		$table_depo .= '<tr>';
		$table_depo .= '</thead>';
		$no_dep=0; 
		$arr_total_depo = [];
		foreach( $result_deposit as $row_dt_depo ){ $no_dep++;
			$table_depo .= '<tr>';
			$table_depo .= '<td align="center">'.$no_dep.'</td>';
			$table_depo .= '<td>'.$row_dt_depo->payment_ref.'</td>';
			$table_depo .= '<td>'.$row_dt_depo->payment_datetime.'</td>';
			$table_depo .= '<td>'.$row_dt_depo->credit_to_name.'</td>';
			$table_depo .= '<td>'.$row_dt_depo->ccy.'</td>';
			$table_depo .= '<td align="right">'.number_format($row_dt_depo->amount).'</td>';
			$table_depo .= '<tr>';
			$arr_total_depo[] = $row_dt_depo->amount;
		}
			$table_depo .= '<tr>';
			$table_depo .= '<td colspan="5" align="right"><b>Amount Total</b></td>';
			$table_depo .= '<td align="right">'.number_format(array_sum($arr_total_depo)).'</td>';
			$table_depo .= '<tr>';
		$table_depo .= '<table>';

		$html .= $table;
		$html .= '<br>';
		$html .= $table_depo;

		return $html;

	}


}
