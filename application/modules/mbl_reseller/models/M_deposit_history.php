<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_deposit_history extends CI_Model {

	var $table = 'v_principalwithdraws';
	var $column = array('v_principalwithdraws.principal_name');
	var $select = 'v_principalwithdraws.*';

	var $order = array('v_principalwithdraws.id' => 'ASC');

	public function __construct()
	{
		parent::__construct();
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
		/*get data from session login*/
        $this->company_id = ( $this->session->userdata('user')->role != 1 ) ? $this->session->userdata('user')->company_id : 0;
	}

	private function _main_query(){
		$this->db_mbr->select($this->select);
		$this->db_mbr->from($this->table);
		if( $this->session->userdata('user')->role != 1 ){
			$this->db_mbr->where( array('v_principalwithdraws.company_id' => $this->company_id ) );
		}
	}

	private function _get_datatables_query()
	{
		
		$this->_main_query();
		if ( isset($_GET['company_id']) AND $_GET['company_id'] != '' ) {
			$this->db_mbr->where('v_principalwithdraws.company_id', $_GET['company_id']);
		}
		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if(isset($_POST['search'])){
				if($_POST['search']['value'])
					($i===0) ? $this->db_mbr->like($item, $_POST['search']['value']) : $this->db_mbr->or_like($item, $_POST['search']['value']);
			}
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db_mbr->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db_mbr->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables()
	{
		$this->_get_datatables_query();
		if(isset($_POST['length'])){
			if($_POST['length'] != -1)
			$this->db_mbr->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db_mbr->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db_mbr->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_main_query();
		return $this->db_mbr->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->_main_query();
		if(is_array($id)){
			$this->db_mbr->where_in(''.$this->table.'.deposit_id',$id);
			$query = $this->db_mbr->get();
			return $query->result();
		}else{
			$this->db_mbr->where(''.$this->table.'.deposit_id',$id);
			$query = $this->db_mbr->get();
			return $query->row();
		}
		
	}

	public function save($table, $data)
	{
		$this->db_mbr->insert($table, $data);
		return $this->db_mbr->insert_id();
	}

	public function update($table, $where, $data)
	{
		$this->db_mbr->update($table, $data, $where);
		return $this->db_mbr->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db_mbr->where_in('t_supplierdeposits.id', $id);
		return $this->db_mbr->delete( 't_supplierdeposits' );
	}

	public function list_fields(){
		return $this->db_mbr->list_fields( $this->table );
	}

	public function get_status_topup($obj){
		switch ($obj->status_topup) {
			case '0':
				$label = '<label class="label label-warning">Principal Verification</label>';
				break;
			
			case '1':
				$label = '<label class="label label-success"><i class="fa fa-eye"></i> Reviewed </label>';
				break;
			
			case '2':
				$label = '<label class="label label-primary"><i class="fa fa-check-circle bigger-120"></i> Approved </label><br>By. '.$obj->approved_by.'<br>Date. '.$this->tanggal->formatDateFormDmy($obj->approved_date).'';
				break;
			
			case '2':
				$label = '<label class="label label-danger"><i class="fa fa-times-circle bigger-120"></i> Rejected </label>';
				break;
			
			default:
				$label = '<label class="label label-warning">Principal Verification</label>';
				break;
		}
		return $label;
	}


}
