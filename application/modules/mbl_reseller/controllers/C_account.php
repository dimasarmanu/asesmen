<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_account extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'mbl_reseller/C_account');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('mbl_reseller/M_account', 'm_account');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
        $this->db_mbr = $this->load->database('db_mbr', TRUE);

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        // echo '<pre>';print_r($this->session->all_userdata());die;
        /*load view index*/
        $this->load->view('V_account/index', $data);
    }

    public function form($id='')
    {
        /*if id is not null then will show form edit*/
        if( $id != '' ){
            /*breadcrumbs for edit*/
            $this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'mbl_reseller/C_account/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
            /*get value by id*/
            $data['value'] = $this->m_account->get_by_id($id);
            /*initialize flag for form*/
            $data['flag'] = "update";
        }else{
            /*breadcrumbs for create or add row*/
            $this->breadcrumbs->push('Add '.strtolower($this->title).'', 'mbl_reseller/C_account/'.strtolower(get_class($this)).'/form');
            /*initialize flag for form add*/
            $data['flag'] = "create";
        }
        /*title header*/
        $data['title'] = $this->title;
        /*show breadcrumbs*/
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_account/form', $data);
    }

    /*function for view data only*/
    public function show($id)
    {
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_account/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
        /*define data variabel*/
        $data['value'] = $this->m_account->get_by_id($id);
        $data['title'] = $this->title;
        $data['flag'] = "read";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_account/form', $data);
    }

    public function verifikasi($id)
    {
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('Verifikasi '.strtolower($this->title).'', 'mbl_reseller/C_account/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
        /*define data variabel*/
        $data['value'] = $this->m_account->get_by_id($id);
        $data['title'] = $this->title;
        $data['flag'] = "verify";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_account/form_verifikasi', $data);
    }

    public function show_detail( $id )
    {
        $fields = $this->m_account->list_fields();
        $data = $this->m_account->get_by_id( $id );
        $html = $this->master->show_detail_row_table( $fields, $data );      

        echo json_encode( array('html' => $html) );
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->m_account->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            //$row[] = '<div class="center"><label class="pos-rel">
             //           <input type="checkbox" class="ace" name="selected_id[]" value="'.$row_list->id.'"/>
             //           <span class="lbl"></span>
            //        </label></div>';
            $row[] = '';
            //$row[] = $row_list->id;
            // $row[] = '<div class="center"><div class="btn-group">
            //             <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
            //                 <span class="ace-icon fa fa-caret-down icon-on-right"></span>
            //             </button>
            //             <ul class="dropdown-menu dropdown-inverse">
            //                 '.$this->authuser->show_button_dropdown('mbl_reseller/C_account', array('R','U','D') ,$row_list->id).'   
            //             </ul>
            //           </div></div>';
            
            $row[] = '<div class="center">'.$row_list->id.'</div>';
            $row[] = $row_list->email;
            $row[] = $row_list->user_name;
            $row[] = $row_list->phone;
            $row[] = $row_list->accounttype_name;
            $row[] = $row_list->company_name;
            // $row[] = $row_list->gmail_token;
            $row[] = ($row_list->is_active == 'Y') ? '<div class="center"><span class="label label-sm label-success">Active</span></div>' : '<div class="center"><span class="label label-sm label-danger">Not active</span></div>';
            $row[] = '<div class="center"><a href="#" class="btn btn-xs btn-primary" onclick="getMenu('."'mbl_reseller/C_account/verifikasi/".$row_list->id."'".')"> Verifikasi </a></div>';
            $row[] = $this->logs->show_logs_record_datatable($row_list);
                   
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_account->count_all(),
                        "recordsFiltered" => $this->m_account->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        // print_r($_POST);die;
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('email', 'Email', 'trim|required|valid_email');
        $val->set_rules('user_name', 'User Name', 'trim|required');
        $val->set_rules('company_id', 'Company', 'trim|required');
        $val->set_rules('accounttype_id', 'Agent Type', 'trim|required');
        $val->set_rules('password_hidden', 'Password', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = $this->input->post('user_id_temp');

            $dataexc = array(
                'email' => $val->set_value('email'),
                'fullname' => $val->set_value('user_name'),
                'username' => $val->set_value('email'),
                'password' => $val->set_value('password_hidden'),
                'flag_user' => 'Publik',
                'is_active' => $this->input->post('is_active'),
            );
            
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*save post data*/
                $this->db->insert('tmp_user',$dataexc);
                $newId = $this->db->insert_id();
                /*save logs*/
                $this->logs->save('tmp_user', $newId, 'insert new record on '.$this->title.' module', json_encode($dataexc),'user_id');

                $user_group = array(
                    'user_id' => $newId,
                    'role_id' => 5,
                    'updated_date' => date('Y-m-d H:i:s'),
                    'created_by' => json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL'))),
                );
                $this->db->insert('tmp_user_has_role',$user_group);
                /*save logs*/
                $this->logs->save('tmp_user_has_role', $newId, 'insert new record on '.$this->title.' module', json_encode($user_group),'user_id');
                
                /*email creating*/
                $mail_creating = array(
                    'code_id' => $this->input->post('id'),
                    'creating_type' => 2,
                    'status_creating' => 0,
                    'create_atemp' => 0,
                );
                $this->db_mbr->insert('t_mailcreating', $mail_creating);

            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*update record*/
                $this->db->update('tmp_user', $dataexc, array('user_id' => $id) );
                $newId = $id;
                /*save logs*/
                $this->logs->save('tmp_user', $newId, 'update record on '.$this->title.' module', json_encode($dataexc),'user_id');
            }

            // update accounttype_id
            $dataaccount = array();
            $dataaccount['company_id'] = $val->set_value('company_id');
            $dataaccount['accounttype_id'] = $val->set_value('accounttype_id');
            $dataaccount['user_id_temp'] = $newId;
            // NPWP
            if(isset($_FILES['npwp']['name']) AND $_FILES['npwp']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->npwp != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->npwp.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->npwp.'');
                        }    
                    }
                }
                $dataaccount['npwp'] = $this->upload_file->doUpload('npwp', PATH_MBR.'attachment/');
            }

            // SIUP
            if(isset($_FILES['siup']['name']) AND $_FILES['siup']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->siup != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->siup.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->siup.'');
                        }    
                    }
                }
                $dataaccount['siup'] = $this->upload_file->doUpload('siup', PATH_MBR.'attachment/');
            }
            // NIB
            if(isset($_FILES['nib']['name']) AND $_FILES['nib']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->nib != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->nib.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->nib.'');
                        }    
                    }
                }
                $dataaccount['nib'] = $this->upload_file->doUpload('nib', PATH_MBR.'attachment/');
            }
            // OFFICE PHOTO
            if(isset($_FILES['photo_office']['name']) AND $_FILES['photo_office']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->photo_office != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->photo_office.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->photo_office.'');
                        }    
                    }
                }
                $dataaccount['photo_office'] = $this->upload_file->doUpload('photo_office', PATH_MBR.'attachment/');
            }

            $dataaccount['is_active'] = 'Y';
            $this->m_account->update( array('id' => $this->input->post('id') ), $dataaccount );
            
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
            }
        }
    }

    public function process_verifikasi()
    {
        // print_r($_POST);die;
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('email', 'Email', 'trim|required|valid_email');
        $val->set_rules('user_name', 'User Name', 'trim|required');
        $val->set_rules('company_id', 'Company', 'trim|required');
        $val->set_rules('accounttype_id', 'Agent Type', 'trim|required');
        $val->set_rules('password_hidden', 'Password', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = $this->input->post('user_id_temp');

            $dataexc = array(
                'email' => $val->set_value('email'),
                'fullname' => $val->set_value('user_name'),
                'username' => $val->set_value('email'),
                'password' => $val->set_value('password_hidden'),
                'flag_user' => 'Publik',
                'is_active' => $this->input->post('is_active'),
            );
            
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*save post data*/
                $this->db->insert('tmp_user',$dataexc);
                $newId = $this->db->insert_id();
                /*save logs*/
                $this->logs->save('tmp_user', $newId, 'insert new record on '.$this->title.' module', json_encode($dataexc),'user_id');

                $user_group = array(
                    'user_id' => $newId,
                    'role_id' => 5,
                    'updated_date' => date('Y-m-d H:i:s'),
                    'created_by' => json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL'))),
                );
                $this->db->insert('tmp_user_has_role',$user_group);
                /*save logs*/
                $this->logs->save('tmp_user_has_role', $newId, 'insert new record on '.$this->title.' module', json_encode($user_group),'user_id');
                
                /*email creating*/
                $mail_creating = array(
                    'code_id' => $this->input->post('id'),
                    'creating_type' => 2,
                    'status_creating' => 0,
                    'create_atemp' => 0,
                );
                $this->db_mbr->insert('t_mailcreating', $mail_creating);

            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*update record*/
                $this->db->update('tmp_user', $dataexc, array('user_id' => $id) );
                $newId = $id;
                /*save logs*/
                $this->logs->save('tmp_user', $newId, 'update record on '.$this->title.' module', json_encode($dataexc),'user_id');
            }

            // update accounttype_id
            $dataaccount = array();
            $dataaccount['company_id'] = $val->set_value('company_id');
            $dataaccount['accounttype_id'] = $val->set_value('accounttype_id');
            $dataaccount['user_id_temp'] = $newId;
            // NPWP
            if(isset($_FILES['npwp']['name']) AND $_FILES['npwp']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->npwp != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->npwp.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->npwp.'');
                        }    
                    }
                }
                $dataaccount['npwp'] = $this->upload_file->doUpload('npwp', PATH_MBR.'attachment/');
            }

            // SIUP
            if(isset($_FILES['siup']['name']) AND $_FILES['siup']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->siup != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->siup.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->siup.'');
                        }    
                    }
                }
                $dataaccount['siup'] = $this->upload_file->doUpload('siup', PATH_MBR.'attachment/');
            }
            // NIB
            if(isset($_FILES['nib']['name']) AND $_FILES['nib']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->nib != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->nib.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->nib.'');
                        }    
                    }
                }
                $dataaccount['nib'] = $this->upload_file->doUpload('nib', PATH_MBR.'attachment/');
            }
            // OFFICE PHOTO
            if(isset($_FILES['photo_office']['name']) AND $_FILES['photo_office']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_account->get_by_id( $this->input->post('id') );
                    if($res_dt->photo_office != NULL){
                        if (file_exists(PATH_MBR.'attachment/'.$res_dt->photo_office.'')) {
                            unlink(PATH_MBR.'attachment/'.$res_dt->photo_office.'');
                        }    
                    }
                }
                $dataaccount['photo_office'] = $this->upload_file->doUpload('photo_office', PATH_MBR.'attachment/');
            }

            $this->m_account->update( array('id' => $this->input->post('id') ), $dataaccount );
            
            

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                /*update status*/
                $this->m_account->update( array('id' => $_POST['id']), array('is_active' => 'Y') );
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
            }
        }
    }

    public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }

    public function delete()
    {
        $id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
        $toArray = explode(',',$id);
        if($id!=null){
            if($this->m_account->delete_by_id($toArray)){
                $this->logs->save('cms_m_product_type', $id, 'delete record', '', 'id');
                echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
            }else{
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
            }
        }else{
            echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
        }
        
    }

    public function export_excel(){
        $list = $this->m_account->get_data(); 
        $data = array(
			'title' 	=> 'Member Account',
            'fields' 	=> array('id','principal_id','accounttype_id','email','user_name','address','province','city','phone','postal_code','is_active','npwp','accounttype_name'),
            'data' 		=> $list->result(),
		);
        // echo '<pre>';print_r($data);die;
        $this->load->view('Templates/excel_view', $data);

    }


}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
