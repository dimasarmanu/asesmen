<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_deposit_history extends MX_Controller {

	/*function constructor*/
	function __construct() {

		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'mbl_reseller/C_deposit_history');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('mbl_reseller/M_deposit_history', 'm_deposit_history');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';
		
	}

	public function index() {
		/*define variable data*/
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show()
		);
		/*load view index*/
		$this->load->view('V_deposit_history/index', $data);
	}

	public function form($id='')
	{
		/*if id is not null then will show form edit*/
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'mbl_reseller/C_deposit_history/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_deposit_history->get_by_id($id);
			/*initialize flag for form*/
			$data['flag'] = "update";
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'mbl_reseller/C_deposit_history/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";
		}
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_deposit_history/form', $data);
	}

	/*function for view data only*/
	public function show($id)
	{
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_deposit_history/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_deposit_history->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "read";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_deposit_history/form', $data);
	}

	public function verifikasi($id)
	{
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('Verifikasi '.strtolower($this->title).'', 'mbl_reseller/C_deposit_history/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_deposit_history->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "verify";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_deposit_history/form_verifikasi', $data);
	}

	public function show_detail( $id )
	{
		$fields = $this->m_deposit_history->list_fields();
		$data = $this->m_deposit_history->get_by_id( $id );
		$html = $this->master->show_detail_row_table( $fields, $data );      

		echo json_encode( array('html' => $html) );
	}

	public function get_data()
	{
		/*get data from model*/
		$list = $this->m_deposit_history->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = $row_list->topup_number;
			$row[] = $this->tanggal->formatDateFormDmy($row_list->deposit_date);
			$row[] = "Nama : " . $row_list->sender_name . "<br>No Rek : ". $row_list->sender_acc_no . "<br>Bank : " . $row_list->bank_name;
			$row[] = "Nama : " . $row_list->acc_name . "<br>No Rek : ". $row_list->acc_no . "<br>Bank : " . $row_list->acc_bank_name;
			//$row[] = $row_list->acc_no;
			//$row[] = $row_list->bank_name;
			$row[] = $row_list->principal_name;
			$row[] = '<div align="right">Rp. '.number_format($row_list->amount).',-</div>';
			$status_topup = $this->m_deposit_history->get_status_topup( $row_list );

			$row[] = '<div align="center">'.$status_topup.'</div>';

			//$row[] = '<div class="center">
			//			'.$this->authuser->show_button('mbl_reseller/C_deposit_history','R',$row_list->deposit_id, 2).'
			//			'.$this->authuser->show_button('mbl_reseller/C_deposit_history','U',$row_list->deposit_id, 2).'
			//			'.$this->authuser->show_button('mbl_reseller/C_deposit_history','D',$row_list->deposit_id, 2).'
			//		  </div>'; 
				   
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m_deposit_history->count_all(),
						"recordsFiltered" => $this->m_deposit_history->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function process()
	{
		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('company_id', 'Company', 'trim|required');
		$val->set_rules('amount', 'Name', 'trim|required');
		$val->set_rules('deposit_date', 'Tanggal', 'trim|required');
		$val->set_rules('principal_id', 'Principal', 'trim|required');
		$val->set_rules('acc_bank_id', 'Bank', 'trim|required');
		$val->set_rules('acc_name', 'Nama Tujuan', 'trim|required');
		$val->set_rules('acc_no', 'Nomor Rekening Penerima', 'trim|required');
		$val->set_rules('sender_name', 'Nama Pengirim', 'trim|required');
		$val->set_rules('sender_acc_no', 'Nomor Rekening Pengirim', 'trim|required');
		$val->set_rules('bank_id', 'Bank Asal', 'trim|required');


		$val->set_message('required', "Silahkan isi field \"%s\"");

		if ($val->run() == FALSE)
		{
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}
		else
		{                       
			$this->db->trans_begin();
			$id = ($this->input->post('id'))?$this->input->post('id'):0;
			// format app num
			$app_num = $this->master->get_max_number_per_month('t_withdrawprincipals','id','deposit_date', 'db_mbr');
			$format_app_num = date('my').'-'.$app_num;
			$dataexc = array(
				'topup_number' => $format_app_num,
				'company_id' => $val->set_value('company_id'),
				'amount' => $val->set_value('amount'),
				'deposit_date' => $this->tanggal->sqlDateForm( $val->set_value('deposit_date') ),
				'principal_id' => $val->set_value('principal_id'),
				'acc_bank_id' => $val->set_value('acc_bank_id'),
				'acc_name' => $val->set_value('acc_name'),
				'acc_no' => $val->set_value('acc_no'),
				'sender_name' => $val->set_value('sender_name'),
				'sender_acc_no' => $val->set_value('sender_acc_no'),
				'bank_id' => $val->set_value('bank_id'),
				'status_topup' => 0,
			);
			
			

			if(isset($_FILES['attachment']['name']) AND $_FILES['attachment']['name'] != ''){
				/*hapus dulu file yang lama*/
				if( $id != 0 ){
					$res_dt = $this->m_deposit_history->get_by_id($id);
					if($res_dt->attachment != NULL){
						if (file_exists(PATH_MBR.$res_dt->attachment.'')) {
							unlink(PATH_MBR.$res_dt->attachment.'');
						}    
					}
					
				}

				$dataexc['attachment'] = 'attachment/'.$this->upload_file->doUpload('attachment', PATH_MBR.'attachment/');
			}

			// print_r($dataexc);die;
			if($id==0){
				$dataexc['created_date'] = date('Y-m-d H:i:s');
				$dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
				/*save post data*/
				$this->m_deposit_history->save('t_withdrawprincipals', $dataexc);
				$newId = $this->db->insert_id();
			}else{
				$dataexc['updated_date'] = date('Y-m-d H:i:s');
				$dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
				/*update record*/
				$this->m_deposit_history->update('t_withdrawprincipals', array('t_withdrawprincipals.id' => $id), $dataexc);
				$newId = $id;
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}
			else
			{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}

	public function find_data()
	{   
		$output = array( "data" => http_build_query($_POST) . "\n" );
		echo json_encode($output);
	}
	
	public function delete()
	{
		$id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
		$toArray = explode(',',$id);
		if($id!=null){
			if($this->m_deposit_history->delete_by_id($toArray)){
				echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
			}else{
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
			}
		}else{
			echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
		}
		
	}


}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
