<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cronjob extends MX_Controller {

	var $address_invoice = "";
	var $address_tiket = "";

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
	}

	public function index() {
		
		//$file_handler = fopen(C_ASSETS_FOLDER . 'images/file_lock_mailcreating.lock', 'w');
		//$is_lock = flock($file_handler, LOCK_EX | LOCK_NB);
		//if (!$is_lock)
		//	die("Ada cronjob mail creating yang belum selesai berjalan!");
		
		$this->cron_mailcreating();
		//$this->cron_mailer();
		//echo "Done!";
		//flock($file_handler, LOCK_UN);
		//fclose($file_handler);
	}

	public function unlockfile() {
		$file_handler = fopen(C_ASSETS_FOLDER . 'images/file_lock_mailcreating.lock', 'w');
		$is_lock = flock($file_handler, LOCK_EX | LOCK_NB);
		if (!$is_lock) {
			flock($file_handler, LOCK_UN);
			fclose($file_handler);
		}
		echo "Done!";
		
	}

	public function cron_mailcreating() {
		$q_mailcreating = $this->db_mbr->query("SELECT * FROM t_mailcreating WHERE status_creating = 0 AND create_atemp < 2 LIMIT 1");
		if ($q_mailcreating->num_rows() == 0) {
			echo "No cronjob found!";
			return;
		}
		$row = $q_mailcreating->row();

		//$this->db_mbr->query("UPDATE t_mailcreating SET create_atemp = 1 WHERE id = ?", array($row->id));
		$message_info = "";

		//Create Invoice send eticket;
		if ($row->creating_type == 3) {
			$message_info =  "<br>Mail creating running! send eticket :" . $row->code_id;
			$this->createemail_ticket($row->code_id);
			//$this->db_mbr->query("UPDATE t_mailcreating SET status_creating = 1 WHERE id = ?", array($row->id));
		}
		echo $message_info;
	}

	public function generate_qr_code_ticket($dt_barcode){
		$this->load->library('ciqrcode');

		foreach ($dt_barcode->result() as $key => $value) {
			$config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = 'assets/qrcode/'; //string, the default is application/cache/
	        $config['errorlog']     = 'assets/qrcode/'; //string, the default is application/logs/
	        $config['imagedir']     = 'assets/qrcode/'; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $this->ciqrcode->initialize($config);
	        $qr_name = $value->barcode.'.png';
	        $params['data'] = $value->barcode; //data yang akan di jadikan QR CODE
	        $params['level'] = 'H'; //H=High
	        $params['size'] = 10;
	        $params['savename'] = FCPATH.$config['imagedir'].$qr_name; //simpan image QR CODE ke folder assets/images/
	        $this->ciqrcode->generate($params);
		}
		
        return true;
	}

	public function generate_qr_code_booking_code($booking_code){
		$this->load->library('ciqrcode');
		$config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = 'assets/qrcode/'; //string, the default is application/cache/
        $config['errorlog']     = 'assets/qrcode/'; //string, the default is application/logs/
        $config['imagedir']     = 'assets/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
        $qr_name = $booking_code.'.png';
        $params['data'] = $booking_code; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$qr_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params);

        return true;
	}

	public function createemail_ticket($id) {
		
		$this->load->library('tanggal');
		
		$q_barcoderequest = $this->db_mbr->query("SELECT * FROM v_requestticket where id LIKE ?", array($id));

		if ($q_barcoderequest->num_rows() == 0) {
			return;
		}

		$data_header = $q_barcoderequest->row();
		
		/*updated by aminlubis23*/
		$dt_barcode = $this->db_mbr->get_where('v_barcodes', array('requestticket_id' => $id) );

		/*generate qrcode*/
	    $qr_code_booking = $this->generate_qr_code_booking_code($data_header->booking_code);
	    $qr_code = $this->generate_qr_code_ticket($dt_barcode);

		/*body message*/
		$arr_data = array(
			'qr_code_booking' => $qr_code_booking,
			'dt_header' => $data_header,
			'dt_barcodeticket' => $dt_barcode,
		);

		$this->load->view('temp_email', $arr_data); // send html template email
		//$template_email = $this->load->view('temp_email', $arr_data, true); // send html template email

		/*end body message*/
		/*end updated by aminlubis23*/

		//$debug_export = var_export($data_header , true);
		
		//data detail ada di view v_barcodes
		
		
		//$this->sendmail($data_header->email_to, "Etiket", $template_email);
	}

	public function sendmail($email_to, $email_subject, $email_message) {
		date_default_timezone_set('Etc/UTC');
		$mail = $this->set_email_object();
		
		$mail->AddAddress($email_to);
		$mail->Subject = $email_subject;
		$mail->Body = $email_message;

		//ob_start();
		if (!$mail->send()) {
			return 'Mailer Error: ' . $mail->ErrorInfo;
		}
		return 'Message has been sent';
	}
	
	function set_email_object() {
        $this->load->library('PHPMailerAutoload');
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 3;
        $mail->Debugoutput = "text/plain";
        $mail->IsHTML(true);
        $mail->Host = "mail.smtp2go.com";
        $mail->Port = 2525;
        $mail->SMTPSecure = "tls";
		$mail->SMTPAuth = true;
		$mail->Username = 'aries.liburania@gmail.com';
        $mail->Password = 'liburania#7654321';
        $mail->From = "resellerapss@liburania.com";
        $mail->FromName = "Liburania.com [Distributor]";
        return $mail;
    }

}
