<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_product extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'mbl_reseller/C_product');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('mbl_reseller/M_product', 'm_product');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        /*load view index*/
        $this->load->view('V_product/index', $data);
    }

    public function form($id='')
    {
        /*if id is not null then will show form edit*/
        if( $id != '' ){
            /*breadcrumbs for edit*/
            $this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'mbl_reseller/C_product/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
            /*get value by id*/
            $data['value'] = $this->m_product->get_by_id($id);
            /*initialize flag for form*/
            $data['flag'] = "update";
        }else{
            /*breadcrumbs for create or add row*/
            $this->breadcrumbs->push('Add '.strtolower($this->title).'', 'mbl_reseller/C_product/'.strtolower(get_class($this)).'/form');
            /*initialize flag for form add*/
            $data['flag'] = "create";
        }
        // echo '<pre>';print_r($data);die;
        /*title header*/
        $data['title'] = $this->title;
        /*show breadcrumbs*/
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_product/form', $data);
    }

    /*function for view data only*/
    public function show($id)
    {
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_product/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
        /*define data variabel*/
        $data['value'] = $this->m_product->get_by_id($id);
        $data['title'] = $this->title;
        $data['flag'] = "read";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_product/form', $data);
    }

    public function show_detail( $id )
    {
        $fields = $this->m_product->list_fields();
        $data = $this->m_product->get_by_id( $id );
        $html = $this->master->show_detail_row_table( $fields, $data );      

        echo json_encode( array('html' => $html) );
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->m_product->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center"><label class="pos-rel">
                        <input type="checkbox" class="ace" name="selected_id[]" value="'.$row_list->id.'"/>
                        <span class="lbl"></span>
                    </label></div>';
            $row[] = '';
            $row[] = $row_list->id;
            $row[] = '<div class="center"><div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
                            <span class="ace-icon fa fa-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-inverse">
                            '.$this->authuser->show_button_dropdown('mbl_reseller/C_product', array('R','U','D') ,$row_list->id).'   
                        </ul>
                      </div></div>';
            
            $row[] = '<div class="center">'.$row_list->id.'</div>';
            $row[] = $row_list->partner_productname;
            $row[] = $row_list->principal_name;
            $row[] = '<div class="center">'.$row_list->min_buy.'</div>';
            $row[] = '<div class="center">'.$row_list->max_buy.'</div>';
            $row[] = '<div class="center">'.$row_list->group_min.'</div>';
            $row[] = ($row_list->is_active == 'Y') ? '<div class="center"><span class="label label-sm label-success">Active</span></div>' : '<div class="center"><span class="label label-sm label-danger">Not active</span></div>';
            $row[] = $this->logs->show_logs_record_datatable($row_list);
                   
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_product->count_all(),
                        "recordsFiltered" => $this->m_product->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }
	
    public function process()
    {
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('partner_productname', 'Product Name', 'trim|required');
        $val->set_rules('principal_id', 'Principal', 'trim|required');
        $val->set_rules('product_desc', 'Product Desccription', 'trim|required');
        $val->set_rules('start_date', 'Start Date', 'trim');
        $val->set_rules('expired_date', 'Expired Date', 'trim');
        $val->set_rules('min_buy', 'Min Order', 'trim|required|integer');
        $val->set_rules('max_buy', 'Max Order', 'trim|required|integer');
        $val->set_rules('group_min', 'Min Group Order', 'trim|required|integer');
        $val->set_rules('day_expired', 'Masa Berlaku', 'trim|required|integer');
        $val->set_rules('visitdate_transdate_min', 'Min Hari H Pemesanan', 'trim|required|integer');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'partner_productname' => $val->set_value('partner_productname'),
                'principal_id' => $val->set_value('principal_id'),
                'product_desc' => $val->set_value('product_desc'),
                'start_date' => $val->set_value('start_date'),
                'expired_date' => $val->set_value('expired_date'),
                'min_buy' => $val->set_value('min_buy'),
                'max_buy' => $val->set_value('max_buy'),
                'group_min' => $val->set_value('group_min'),
                'day_expired' => $val->set_value('day_expired'),
                'visitdate_transdate_min' => $val->set_value('visitdate_transdate_min'),
                'is_active' => $this->input->post('is_active'),
            );
            //print_r($dataexc);die;
            if(isset($_FILES['file_image']['name']) AND $_FILES['file_image']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_product->get_by_id($id);
                    if($res_dt->product_image != NULL){
                        if (file_exists(PATH_MBR.'products/'.$res_dt->product_image.'')) {
                            unlink(PATH_MBR.'products/'.$res_dt->product_image.'');
                        }    
                    }
                    
                }

                $dataexc['product_image'] = 'products/'.$this->upload_file->doUpload('file_image', PATH_MBR.'products/');
            }

            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*save post data*/
                $this->m_product->save($dataexc);
                $newId = $this->db->insert_id();
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*update record*/
                $this->m_product->update(array('id' => $id), $dataexc);
                $newId = $id;
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
            }
        }
    }

    public function delete()
    {
        $id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
        $toArray = explode(',',$id);
        if($id!=null){
            if($this->m_product->delete_by_id($toArray)){
                echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
            }else{
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
            }
        }else{
            echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
        }
        
    }


}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
