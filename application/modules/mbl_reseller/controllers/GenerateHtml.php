<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class GenerateHtml extends MX_Controller {

	var $address_invoice = "";
	var $address_tiket = "";

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->db_mbr = $this->load->database('db_mbr', TRUE);
		$this->load->library('pdf');
	}

    public function generate(){
        
        $code = $_GET['code'];
		
        $q_barcoderequest = $this->db_mbr->get_where('v_requestticket', array('booking_code' => $code));
		
        if ($q_barcoderequest->num_rows() == 0) {
			return;
        }
		
        $data_header = $q_barcoderequest->row();
        /*updated by aminlubis23*/
        $dt_barcode = $this->db_mbr->get_where('v_barcodes', array('requestticket_id' => $data_header->id) );
		
        /*generate qrcode*/
        $qr_code_booking = $this->generate_qr_code_booking_code($data_header->booking_code);
        $qr_code = $this->generate_qr_code_ticket($dt_barcode);
		
		
        $arr_data = array(
			'qr_code_booking' => $qr_code_booking,
            'dt_header' => $data_header,
            'dt_barcodeticket' => $dt_barcode,
        );
		// echo '<pre>';print_r($arr_data);die;
        
        // $template_html = $this->load->view('temp_pdf', $arr_data, false);

        $this->generatePDF($arr_data);
    }

    public function generate_qr_code_ticket($dt_barcode){
        $this->load->library('ciqrcode');

        foreach ($dt_barcode->result() as $key => $value) {
            $config['cacheable']    = true; //boolean, the default is true
            $config['cachedir']     = 'assets/qrcode/'; //string, the default is application/cache/
            $config['errorlog']     = 'assets/qrcode/'; //string, the default is application/logs/
            $config['imagedir']     = 'assets/qrcode/'; //direktori penyimpanan qr code
            $config['quality']      = true; //boolean, the default is true
            $config['size']         = '1024'; //interger, the default is 1024
            $config['black']        = array(224,255,255); // array, default is array(255,255,255)
            $config['white']        = array(70,130,180); // array, default is array(0,0,0)
            $this->ciqrcode->initialize($config);
            $qr_name = $value->barcode.'.png';
            $params['data'] = $value->barcode; //data yang akan di jadikan QR CODE
            $params['level'] = 'H'; //H=High
            $params['size'] = 10;
            $params['savename'] = FCPATH.$config['imagedir'].$qr_name; //simpan image QR CODE ke folder assets/images/
            $this->ciqrcode->generate($params);
        }
        
        return true;
    }

    public function generate_qr_code_booking_code($booking_code){
        $this->load->library('ciqrcode');
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = 'assets/qrcode/'; //string, the default is application/cache/
        $config['errorlog']     = 'assets/qrcode/'; //string, the default is application/logs/
        $config['imagedir']     = 'assets/qrcode/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
        $qr_name = $booking_code.'.png';
        $params['data'] = $booking_code; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$qr_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params);

        return true;
    }

	function generatePDF($data) { 

        $pdf = new TCPDF('L', PDF_UNIT, array(180,120), true, 'UTF-8', false);
        $pdf->SetCreator('Liburania Agent');
        
        $pdf->SetAuthor('Liburania Redeem System');
        $pdf->SetTitle('E-Tiket '.$data['dt_header']->principal_name.'');

        // set margins
        $pdf->SetHeaderMargin(5);
        $pdf->SetFooterMargin(5);

    // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

    // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 5);

    // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    
    // auto page break //
        $pdf->SetAutoPageBreak(TRUE, 5);

    //set margin
        
    // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->setJPEGQuality(75);

        //add page
        $pdf->AddPage();

        $pdf->SetFont('dejavusans', '', 11);
        $pdf->ln();
        define('ENVIRONTMENT', 'local');
        $image_url = PATH_IMG_MBR ;

        $page = 0; 
        $html = '';
		$data_arr = $data['dt_barcodeticket']->result();
		
        if( $data['dt_header']->is_group == 'ROMBONGAN' ){
			$html .= '<table width="100%" border="0">';
			$html .= '<tr>';
			$html .= '<td width="20%" align="center">';
			$html .= '<img align="center" src="'.$image_url.'/assets/images/'.$data['dt_header']->logo.'" width="60px">';
			$html .= '</td>';
			$html .= '<td width="80%">';
			$html .= '<b style="font-size:48px">'.strtoupper($data['dt_header']->principal_name).'<br></b>';
			$html .= '<span style="font-size: 30px">'.$data['dt_header']->address.'</span>';
			$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td colspan="2">';
			$html .= '<hr>';
			$html .= '</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<table width="100%" border="0">';
			$html .= '<tr>';
			/*image qr code*/
			$html .= '<td width="30%" align="center">';
			$html .= '<img align="left" src="'.$image_url.'/assets/qrcode/'.$data['dt_header']->booking_code.'.png" width="100px"><br>';
			$html .= '<span >Kode Booking :</span><br>';
			$html .= '<b>'.$data['dt_header']->booking_code.'</b><br>';
			$html .= '<span >( '.ucfirst(strtolower($data['dt_header']->is_group)).' )</span>';
			$html .= '</td>';
			/*end image qr code*/
			$html .= '<td width="70%" align="center">';
			$html .= '<b>'.strtoupper(strtolower($data['dt_header']->principal_name)).'</b><br><br>';
			$html .= '<table style="font-size:36px" border="0">';
			$html .='<tr>';
			$html .='<td align="left" width="130px">';
			$html .='<span>Nama</span>';
			$html .='</td>';
			$html .='<td align="right" width="263px">';
			$html .='<b>'.strtoupper($data['dt_header']->name_to).'</b>';
			$html .='</td>';
			$html .='</tr>';
			$html .='<tr>';
			$html .='<td align="left">';
			$html .='<span>Email</span>';
			$html .='</td>';
			$html .='<td align="right">';
			$html .='<b>'.$data['dt_header']->email_to.'</b>';
			$html .='</td>';
			$html .='</tr>';
			$html .='<tr>';
			$html .='<td align="left">';
			$html .='<span>No.Telp / HP</span>';
			$html .='</td>';
			$html .='<td align="right">';
			$html .='<b>'.$data['dt_header']->phone_to.'</b>';
			$html .='</td>';
			$html .='</tr>';
			//$html .='<tr>';
			//$html .='<td align="left">';
			//$html .='<span>Distributor</span>';
			//$html .='</td>';
			//$html .='<td align="right">';
			//$html .='<b>'.$data['dt_header']->user_name.'</b>';
			//$html .='</td>';
			//$html .='</tr>';
			$html .='<tr>';
			$html .='<td align="left">';
			$html .='<span>Jumlah</span>';
			$html .='</td>';
			$html .='<td align="right">';
			$html .='<b>'.$data['dt_header']->qty.'</b>';
			$html .='</td>';
			$html .='</tr>';
			$html .='<tr>';
			$html .='<td align="left">';
			$html .='<span>Tanggal Kunjungan</span>';
			$html .='</td>';
			$html .='<td align="right">';
			$html .='<b>'.$this->tanggal->formatDateFormDmy($data['dt_header']->visit_date).'</b>';
			$html .='</td>';
			$html .='</tr>';
			$html .='<tr>';
			$html .='<td align="left">';
			$html .='<span>Masa Berlaku</span>';
			$html .='</td>';
			$html .='<td align="right">';
			$html .='<b>'.$this->tanggal->formatDateFormDmy($data['dt_header']->expired_date).'</b>';
			$html .='</td>';
			$html .='</tr>';
			$html .='<tr>';
			$html .='<td align="left">';
			$html .='<span>Hari Kunjungan</span>';
			$html .='</td>';
			$html .='<td align="right">';
			$html .='<b>'.$data['dt_header']->datepricetype_id.'</b><br><br>';
			$html .='</td>';
			$html .='</tr>';
			$html .='</table>';
			$html .= '</td>';
			$html .= '</tr>';
			$html .= '</table>';
			$html .= '<table>';
			$html .= '<tr>';
			$html .= '<td valign="top" align="right" style="font-size:28px">';
			$html .= '<br><br><br><br><i>Issued by '.$data['dt_header']->company_name.'<br>'.date('D, d/m/Y').'</i><br><br>';
			$html .= '</td>';
			$html .= '</tr>';
			$html .= '</table>';
        }else{
            foreach($data_arr as $key=>$row_dt) : 
				$page++;
				$html .= '<table width="100%" border="0">';
				$html .= '<tr>';
				$html .= '<td width="20%" align="center">';
				$html .= '<img align="center" src="'.$image_url.'/assets/images/'.$data['dt_header']->logo.'" width="60px">';
				$html .= '</td>';
				$html .= '<td width="80%" valign="center">';
				$html .= '<b style="font-size:48px">'.strtoupper($data['dt_header']->principal_name).'<br></b>';
				$html .= '<span style="font-size: 30px">'.$data['dt_header']->address.'</span>';
				$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
				$html .= '<td colspan="2">';
				$html .= '<br><hr>';
				$html .= '</td>';
				$html .= '</tr>';
				$html .= '</table>';
				$html .= '<table width="100%" border="0">';
				$html .= '<tr>';
				/*image qr code*/
				$html .= '<td width="30%" align="center">';
				$html .= '<img align="left" src="'.$image_url.'/assets/qrcode/'.$row_dt->barcode.'.png" width="100px"><br>';
				$html .= '<span >Kode Tiket :</span><br>';
				$html .= '<b>'.$row_dt->barcode.'</b><br>';
				$html .= '<span >( '.ucfirst(strtolower($row_dt->is_group)).' )</span>';
				$html .= '</td>';
				/*end image qr code*/
				$html .= '<td width="70%" align="center">';
				$html .= '<b>'.strtoupper(strtolower($row_dt->partner_productname)).'</b><br><br>';
				$html .= '<table style="font-size:35px" border="0">';
				$html .='<tr>';
				$html .='<td align="left" width="130px">';
				$html .='<span>Nama</span>';
				$html .='</td>';
				$html .='<td align="right" width="263px">';
				$html .='<b>'.strtoupper($data['dt_header']->name_to).'</b>';
				$html .='</td>';
				$html .='</tr>';
				$html .='<tr>';
				$html .='<td align="left">';
				$html .='<span>Email</span>';
				$html .='</td>';
				$html .='<td align="right">';
				$html .='<b>'.$data['dt_header']->email_to.'</b>';
				$html .='</td>';
				$html .='</tr>';
				$html .='<tr>';
				$html .='<td align="left">';
				$html .='<span>No.Telp / HP</span>';
				$html .='</td>';
				$html .='<td align="right">';
				$html .='<b>'.$data['dt_header']->phone_to.'</b>';
				$html .='</td>';
				$html .='</tr>';
				//$html .='<tr>';
				//$html .='<td align="left">';
				//$html .='<span>Distributor</span>';
				//$html .='</td>';
				//$html .='<td align="right">';
				//$html .='<b>'.$data['dt_header']->user_name.'</b>';
				//$html .='</td>';
				//$html .='</tr>';
				$html .='<tr>';
				$html .='<td align="left">';
				$html .='<span>Jumlah</span>';
				$html .='</td>';
				$html .='<td align="right">';
				$html .='<b>'.$data['dt_header']->qty.'</b>';
				$html .='</td>';
				$html .='</tr>';
				$html .='<tr>';
				$html .='<td align="left">';
				$html .='<span>Tanggal Kunjungan</span>';
				$html .='</td>';
				$html .='<td align="right">';
				$html .='<b>'.$this->tanggal->formatDateFormDmy($row_dt->visit_date).'</b>';
				$html .='</td>';
				$html .='</tr>';
				$html .='<tr>';
				$html .='<td align="left">';
				$html .='<span>Masa Berlaku</span>';
				$html .='</td>';
				$html .='<td align="right">';
				$html .='<b>'.$this->tanggal->formatDateFormDmy($row_dt->expired_date).'</b>';
				$html .='</td>';
				$html .='</tr>';
				$html .='<tr>';
				$html .='<td align="left">';
				$html .='<span>Hari Kunjungan</span>';
				$html .='</td>';
				$html .='<td align="right">';
				$html .='<b>'.$row_dt->datepricetype_id.'</b><br><br>';
				$html .='</td>';
				$html .='</tr>';
				$html .='</table>';
				$html .= '</td>';
				$html .= '</tr>';
				$html .= '</table>';
				$html .= '<table>';
				$html .= '<tr>';
				$html .= '<td valign="top" align="right" style="font-size:28px">';
				$html .= '<br><br><br><br><i>Issued by '.$data['dt_header']->company_name.'<br>'.date('D, d/m/Y').'</i><br><br>';
				$html .= '</td>';
				$html .= '</tr>';
				$html .= '</table><br/><br/>';
            endforeach;
		}
		$pdf->SetFont('dejavusans', '', 8);
		
		$html .= '<br pagebreak="true"/>' ;
		$html .= '<b>Terms and Condition</b>';
		$html .= '<hr>';
		$html .= '<p>';
		$html .= $data['dt_header']->tc;
		$html .= '</p>';

        $result = $html;
        // output the HTML content
        $pdf->writeHTML($result, true, false, true, false, '');
        $pdf->lastPage();
        
        ob_end_clean();
        $filename = 'E-tiket_'.$data['dt_header']->principal_name.'_'.$data['dt_header']->booking_code.'';
        $pdf->Output(''.$filename.'.pdf', 'I'); 
        

    }


}
