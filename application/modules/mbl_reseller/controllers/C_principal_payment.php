<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_principal_payment extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'mbl_reseller/C_principal_payment');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('mbl_reseller/M_principal_payment', 'm_principal_payment');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        /*load view index*/
        $this->load->view('V_principal_payment/index', $data);
    }

    public function form($id='')
    {
        /*if id is not null then will show form edit*/
        if( $id != '' ){
            /*breadcrumbs for edit*/
            $this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'mbl_reseller/C_principal_payment/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
            /*get value by id*/
            $data['value'] = $this->m_principal_payment->get_by_id($id);
            /*initialize flag for form*/
            $data['flag'] = "update";
        }else{
            /*breadcrumbs for create or add row*/
            $this->breadcrumbs->push('Add '.strtolower($this->title).'', 'mbl_reseller/C_principal_payment/'.strtolower(get_class($this)).'/form');
            /*initialize flag for form add*/
            $data['flag'] = "create";
        }
        /*title header*/
        $data['title'] = $this->title;
        /*show breadcrumbs*/
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_principal_payment/form', $data);
    }

    /*function for view data only*/
    public function show($id)
    {
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_principal_payment/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
        /*define data variabel*/
        $data['value'] = $this->m_principal_payment->get_by_id($id);
        $data['title'] = $this->title;
        $data['flag'] = "read";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_principal_payment/form', $data);
    }

    public function create_payment()
    {
        
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_principal_payment/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/');
        /*define data variabel*/
        $data['value'] = $this->m_principal_payment->get_selected_transaction($_GET);
        $data['bill_num'] = $this->master->get_max_number_per_month('t_payment','id','payment_date', 'db_mbr');
        $data['title'] = $this->title;
        $data['flag'] = "read";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_principal_payment/form', $data);
    }

    public function show_detail( $id )
    {
        $fields = $this->m_principal_payment->list_fields();
        $data = $this->m_principal_payment->get_by_id( $id );
        $html = $this->master->show_detail_row_table( $fields, $data );      

        echo json_encode( array('html' => $html) );
    }

    public function print_billing($id)
    {
        $this->load->library('Kuitansi');
        /*define data variabel*/
        $data['app'] = $this->db->get_where('tmp_profile_app', array('id' => 1))->row();
        $data['value'] = $this->m_principal_payment->get_detail_payment($id);
        /*load form view*/
        $this->load->view('V_principal_payment/print_billing', $data);
    }
    
    public function get_data()
    {
        /*get data from model*/
        $list = $this->m_principal_payment->get_datatables();
        $data = array();
        $no = $_POST['start'];
        if(isset($_GET['principal_id'])){
            foreach ($list as $row_list) {
                $no++;
                $row = array();
                if(is_null($row_list->payment_detail_id)){
                    $row[] = '<div class="center"><label class="pos-rel">
                            <input type="checkbox" class="checked ace" name="selected_id[]" value="'.$row_list->id.'"/>
                            <span class="lbl"></span>
                        </label></div>';  
                }else{
                    $row[] = '<div class="center"><i class="fa fa-check-circle green bigger-120"></i></div>';  
                }
                       
                $row[] = $row_list->booking_code;
                $row[] = ucfirst($row_list->user_name);
                $row[] = $this->tanggal->formatDateFormDmy($row_list->trans_time);
                $row[] = $row_list->principal_name;
                $row[] = $row_list->name_to;
                $row[] = $this->tanggal->formatDateFormDmy($row_list->visit_date);
                $row[] = $row_list->partner_productname;
                $row[] = $row_list->datepricetype_id;
                $row[] = $row_list->is_group;
                $row[] = '<div class="center">'.$row_list->qty.'</div>';
                $row[] = '<div align="right">'.number_format($row_list->price_principal).',-</div>';
                $row[] = '<div align="right">'.number_format($row_list->total_priceprincipal).',-</div>';
                $row[] = '<div align="right">'.number_format($row_list->price).',-</div>';
                $row[] = '<div align="right">'.number_format($row_list->total).',-</div>';
                $margin = $row_list->total - $row_list->total_priceprincipal;
                $row[] = '<div align="right">'.number_format($margin).',-</div>';
                $data[] = $row;
            }
        }        

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_principal_payment->count_all(),
                        "recordsFiltered" => $this->m_principal_payment->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('payment_number', 'Nomor Billing', 'trim|required');
        $val->set_rules('payment_date', 'Tanggal', 'trim|required');
        $val->set_rules('principal_id', 'Principal', 'trim|required');
        $val->set_rules('account_number', 'Nomor Rekening', 'trim|required');
        $val->set_rules('receive_name', 'Nama Penerima', 'trim|required');
        $val->set_rules('from_bank_id', 'Dari Bank', 'trim|required');
        // $val->set_rules('to_bank_id', 'Tujuan Bank', 'trim|required');
        $val->set_rules('total_amount', 'Total Pembayaran', 'trim|required');
        $val->set_rules('keterangan', 'Keterangan', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'payment_number' => $val->set_value('payment_number'),
                'payment_date' => $val->set_value('payment_date'),
                'principal_id' => $val->set_value('principal_id'),
                'account_number' => $val->set_value('account_number'),
                'receive_name' => $val->set_value('receive_name'),
                'from_bank_id' => $val->set_value('from_bank_id'),
                // 'to_bank_id' => $val->set_value('to_bank_id'),
                'keterangan' => $val->set_value('keterangan'),
                'payment_status' => '0',
                'total_amount' => str_replace(',','',$val->set_value('total_amount')),
            );
            
            // print_r($dataexc);die;
            
            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*save post data*/
                $newId = $this->m_principal_payment->save('t_payment',$dataexc);
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*update record*/
                $this->m_principal_payment->update('t_payment', array('id' => $id), $dataexc);
                $newId = $id;
            }

            // insert detail
            $datapayment = array();
            foreach($_POST['requestticket_id'] as $row){
                $datapayment['requestticket_id'] = $row;
                $datapayment['payment_id'] = $newId;
                $datapayment['created_date'] = date('Y-m-d H:i:s');
                $datapayment['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*save post data*/
                $this->m_principal_payment->save('t_payment_detail',$datapayment);
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan', 'id' => $newId));
            }
        }
    }

    public function delete()
    {
        $id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
        $toArray = explode(',',$id);
        if($id!=null){
            if($this->m_principal_payment->delete_by_id($toArray)){
                echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
            }else{
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
            }
        }else{
            echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
        }
        
    }

    public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }

    public function select_transaction()
    {   
        $output = array( "data" => http_build_query($_POST['data']) . "\n" );
        echo json_encode($output);
    }

    public function export_excel(){

        $result = $this->m_principal_payment->get_data();

        $data = array(
            'value' => $result,
        );
        // echo '<pre>'; print_r($data);die;
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-type:   application/x-msexcel; charset=utf-8");
        header("Content-Disposition: attachment; filename=export_data_".date('d/m/Y').".xls"); 
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        $html = '';
        $html .= '<p><h3><b>Hasil Pencarian Data</b></h3></p>';
        $html .= $this->load->view('V_principal_payment/view_excel', $data, true);
        echo $html;
    }

    public function ProsesPembayaran()
    {
        $id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
        $status=$this->input->post('status')?$this->input->post('status',TRUE):null;
        if($id!=null){
            if($this->m_principal_payment->ProsesPembayaran($id, $status)){
                echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
            }else{
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
            }
        }else{
            echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
        }
        
    }


}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
