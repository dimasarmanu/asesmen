<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class C_wlb_price_setting extends MX_Controller {

	/*function constructor*/
	function __construct() {

		parent::__construct();
		/*breadcrumb default*/
		$this->breadcrumbs->push('Index', 'mbl_reseller/C_wlb_price_setting');
		/*session redirect login if not login*/
		if($this->session->userdata('logged')!=TRUE){
			echo 'Session Expired !'; exit;
		}
		/*load model*/
		$this->load->model('mbl_reseller/M_wlb_price_setting', 'm_wlb_price_setting');
		/*enable profiler*/
		$this->output->enable_profiler(false);
		/*profile class*/
		$this->title = 'Price Setting For White Label';

	}

	public function index() {
		/*define variable data*/
		$data = array(
			'title' => $this->title,
			'breadcrumbs' => $this->breadcrumbs->show()
		);
		/*load view index*/
		$this->load->view('V_wlb_price_setting/index', $data);
	}

	public function form($id='')
	{
		/*params*/
		$data['price_type'] = $this->m_wlb_price_setting->get_price_type();
		/*if id is not null then will show form edit*/
		if( $id != '' ){
			/*breadcrumbs for edit*/
			$this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'mbl_reseller/C_wlb_price_setting/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			/*get value by id*/
			$data['value'] = $this->m_wlb_price_setting->get_by_id($id);
			/*initialize flag for form*/
			$data['flag'] = "update";
		}else{
			/*breadcrumbs for create or add row*/
			$this->breadcrumbs->push('Add '.strtolower($this->title).'', 'mbl_reseller/C_wlb_price_setting/'.strtolower(get_class($this)).'/form');
			/*initialize flag for form add*/
			$data['flag'] = "create";
		}
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		
		/*load form view*/
		$this->load->view('V_wlb_price_setting/form', $data);
	}

	public function form_setting()
	{
		/*params*/
		$data['price_type'] = $this->m_wlb_price_setting->get_price_type();

		/*breadcrumbs for edit*/
		$this->breadcrumbs->push('Price Update'.strtolower($this->title).'', 'mbl_reseller/C_wlb_price_setting/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/');
		/*get value by id*/
		$params = array(
			'product_id' => $_GET['product'],
			'accounttype_id' => $_GET['agent'],
		);
		$data['value'] = $this->m_wlb_price_setting->get_by_params($params);
		/*initialize flag for form*/
		$data['flag'] = "update";
		/*title header*/
		$data['title'] = $this->title;
		/*show breadcrumbs*/
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		// echo '<pre>'; print_r($data);die;
		$this->load->view('V_wlb_price_setting/form', $data);
	}

	/*function for view data only*/
	public function show($id)
	{
		/*breadcrumbs for view*/
		$this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_wlb_price_setting/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
		/*define data variabel*/
		$data['value'] = $this->m_wlb_price_setting->get_by_id($id);
		$data['title'] = $this->title;
		$data['flag'] = "read";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*load form view*/
		$this->load->view('V_wlb_price_setting/form', $data);
	}

	public function show_detail( $id )
	{
		$fields = $this->m_wlb_price_setting->list_fields();
		$dt_price = $this->m_wlb_price_setting->get_by_id( $id );
		$data = array(
			'dt_price' => $dt_price,
			'reff_price' => $this->m_wlb_price_setting->get_reff_price( array('product_id' => $dt_price->product_id, 'accounttype_id' => $dt_price->accounttype_id, 'behalf_accounttype_id' => $dt_price->behalf_accounttype_id, 'is_active' => 'Y') ),
		);
		
		$html = $this->load->view('V_wlb_price_setting/detail_data_view', $data, TRUE);
		$html .= $this->master->show_detail_row_table( $fields, $dt_price );      

		echo json_encode( array('html' => $html) );
	}

	public function get_data()
	{
		/*get data from model*/
		$list = $this->m_wlb_price_setting->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $row_list) {
			$no++;
			$row = array();
			$row[] = '<div class="center"><label class="pos-rel">
						<input type="checkbox" class="ace" name="selected_id[]" value="'.$row_list->id.'"/>
						<span class="lbl"></span>
					</label></div>';
			$row[] = '';
			$row[] = $row_list->id;
			//$row[] = '<div class="center"><div class="btn-group">
			//            <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
			//                <span class="ace-icon fa fa-caret-down icon-on-right"></span>
			//            </button>
			//            <ul class="dropdown-menu dropdown-inverse">
			//                <li><a href="#" onclick="getMenu('."'mbl_reseller/C_wlb_price_setting/form_setting?principal=".$row_list->principal_id."&agent=".$row_list->accounttype_id."&price_type=".$row_list->datepricetype_id."&product=".$row_list->product_id."'".')">Update</a>
			//                </li>
			//                '.$this->authuser->show_button_dropdown('mbl_reseller/C_wlb_price_setting', array('D') ,$row_list->id).'   
			//            </ul>
			//          </div></div>';
			
			// $row[] = '<div class="center">'.$row_list->id.'</div>';
			$row[] = $row_list->company_name .'<br>'.$row_list->accounttype_name.'<br>'.$row_list->partner_productname.'<br><b>'.$row_list->principal_name.'</b><br>'.$row_list->price_type.'<br>'.$row_list->product_type;
			$row[] = $row_list->HargaBasic;
			$row[] = $row_list->Harga;
			$row[] = $row_list->HargaPublish;
			$row[] = '<small><b>Start</b></small><br>'.$this->tanggal->formatDateFormDmy($row_list->start_date).'<br><small><b>Expired</b><br></small>'.$this->tanggal->formatDateFormDmy($row_list->expired_date);
			 $row[] = ($row_list->is_active == 'Y') ? '<div class="center"><span class="label label-sm label-success">Active</span></div>' : '<div class="center"><span class="label label-sm label-danger">Not active</span></div>';
			 
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m_wlb_price_setting->count_all(),
						"recordsFiltered" => $this->m_wlb_price_setting->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function process()
	{
		// print_r($_POST);die;

		$this->load->library('form_validation');
		$val = $this->form_validation;
		$val->set_rules('behalf_accounttype_id', 'Behalf Account Type', 'trim|required');
		$val->set_rules('accounttype_id', 'Agent Type', 'trim|required');
		$val->set_rules('principal_id', 'Principal', 'trim|required');
		$val->set_rules('product_id', 'Product', 'trim|required');
		$val->set_rules('price_type', 'Price Type', 'trim|required');
		$val->set_rules('product_type', 'Product Type', 'trim|required');

		foreach ($_POST['datepricetype_id'] as $key => $value) {
			$val->set_rules('price_principal['.$value.']', 'Basic Price on '.$value.'', 'trim|integer|required');
			$val->set_rules('price['.$value.']', 'Selling Price on '.$value.'', 'trim|integer|required');
			$val->set_rules('publish['.$value.']', 'Publish Price on '.$value.'', 'trim|integer|required');
		}

		$val->set_message('required', "Silahkan isi field \"%s\"");

		if ($val->run() == FALSE)
		{
			$val->set_error_delimiters('<div style="color:white">', '</div>');
			echo json_encode(array('status' => 301, 'message' => validation_errors()));
		}
		else
		{           
					 
			$this->db->trans_begin();
			// 'behalf_accounttype_id' => $val->set_value('behalf_accounttype_id'),
			foreach ($_POST['datepricetype_id'] as $row_post) {
				// if( $_POST['is_active'][$row_post] == 'Y' ){
					$id = ($_POST['price_id'][$row_post])?$_POST['price_id'][$row_post]:0;
					$dataexc = array(
						'id' => $id,
						'behalf_accounttype_id' => $val->set_value('behalf_accounttype_id'),
						'datepricetype_id' => $row_post,
						'product_id' => $val->set_value('product_id'),
						'product_type' => $val->set_value('product_type'),
						'price_type' => $val->set_value('price_type'),
						'accounttype_id' => $val->set_value('accounttype_id'),
						'price_principal' => $val->set_value('price_principal['.$row_post.']'),
						'price' => $val->set_value('price['.$row_post.']'),
						'price_publish' => $val->set_value('publish['.$row_post.']'),
						'is_active' => 'N',
					);
					
					// print_r($dataexc);die;  
					/*save post data*/
					/*update all non active*/
					$dataexcupdate = array();
					$dataexcupdate['is_active'] = 'N';
					$dataexcupdate['updated_date'] = date('Y-m-d H:i:s');
					$dataexcupdate['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
						
					$this->m_wlb_price_setting->update(array(
						'datepricetype_id' => $dataexc['datepricetype_id'],
						'product_id' => $dataexc['product_id'],
						'product_type' => $dataexc['product_type'],
						'price_type' => $dataexc['price_type'],
						'accounttype_id' => $dataexc['accounttype_id'],
						'behalf_accounttype_id' => $dataexc['behalf_accounttype_id']
					), $dataexcupdate);
					
					/*add new record*/
					$dataexc['created_date'] = date('Y-m-d H:i:s');
					$dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
					$dataexc["id"] = 0;
					$dataexc["is_active"] = 'Y';
					$this->m_wlb_price_setting->save($dataexc);
					$newId = $this->db->insert_id();
			} 

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
			}
			else
			{
				$this->db->trans_commit();
				echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
			}
		}
	}
	
	public function find_data()
	{   
		$output = array( "data" => http_build_query($_POST) . "\n" );
		echo json_encode($output);
	}

	public function delete()
	{
		$id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
		$toArray = explode(',',$id);
		if($id!=null){
			if($this->m_wlb_price_setting->delete_by_id($toArray)){
				echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
			}else{
				echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
			}
		}else{
			echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
		}
		
	}

	public function check_existing(){
		/*cek existing data*/
		$count = $this->m_wlb_price_setting->check_existing_by_agenttype_and_product();
		echo json_encode( array('count' => $count->num_rows(), 'data' => $count->result()) );
	}


}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
