<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_transaction_report extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'mbl_reseller/C_transaction_report');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('mbl_reseller/M_transaction_report', 'm_transaction_report');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        // echo '<pre>';print_r($this->session->all_userdata());die;
        /*load view index*/
        $this->load->view('V_transaction_report/index', $data);
    }

    public function form($id='')
    {
        /*if id is not null then will show form edit*/
        if( $id != '' ){
            /*breadcrumbs for edit*/
            $this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'mbl_reseller/C_transaction_report/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
            /*get value by id*/
            $data['value'] = $this->m_transaction_report->get_by_id($id);
            /*initialize flag for form*/
            $data['flag'] = "update";
        }else{
            /*breadcrumbs for create or add row*/
            $this->breadcrumbs->push('Add '.strtolower($this->title).'', 'mbl_reseller/C_transaction_report/'.strtolower(get_class($this)).'/form');
            /*initialize flag for form add*/
            $data['flag'] = "create";
        }
        /*title header*/
        $data['title'] = $this->title;
        /*show breadcrumbs*/
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_transaction_report/form', $data);
    }

    /*function for view data only*/
    public function show($id)
    {
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_transaction_report/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
        /*define data variabel*/
        $data['value'] = $this->m_transaction_report->get_by_id($id);
        $data['title'] = $this->title;
        $data['flag'] = "read";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_transaction_report/form', $data);
    }

    public function show_detail( $id )
    {
        $fields = $this->m_transaction_report->list_fields();
        $data = $this->m_transaction_report->get_by_id( $id );
        $html = $this->master->show_detail_row_table( $fields, $data );      

        echo json_encode( array('html' => $html) );
    }

    public function get_summary_transaction(){
        $result = $this->m_transaction_report->get_data();
        foreach($result as $row_list){
            $margin = $row_list->total - $row_list->total_priceprincipal;
            $arr_total_modal[] = $row_list->total_priceprincipal;
            $arr_total_jual[] = $row_list->total;
            $arr_total_margin[] = $margin;
        }
        $total_modal = array_sum($arr_total_modal);
        $total_jual = array_sum($arr_total_jual);
        $total_margin = array_sum($arr_total_margin);
        echo json_encode(array('total_modal' => $total_modal, 'total_jual' => $total_jual, 'total_margin' => $total_margin));
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->m_transaction_report->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center"><label class="pos-rel">
                        <input type="checkbox" class="ace" name="selected_id[]" value="'.$row_list->id.'"/>
                        <span class="lbl"></span>
                    </label></div>';         
            $row[] = '<b>'.$row_list->booking_code.'</b><br>'.$row_list->user_name.'<br>'.$this->tanggal->formatDateFormDmy($row_list->trans_time);
            // $row[] = ucfirst($row_list->user_name);
            // $row[] = $this->tanggal->formatDateFormDmy($row_list->trans_time);
            $row[] = $row_list->partner_productname.'<br><span style="font-size:11px">'.$row_list->principal_name.'</span>';
            // $row[] = $row_list->principal_name;
            $row[] = $row_list->name_to.'<br>('.strtolower($row_list->is_group).')';
            $row[] = $this->tanggal->formatDateFormDmy($row_list->visit_date).'<br>'.$row_list->datepricetype_id;
            // $row[] = $row_list->datepricetype_id;
            //$row[] = "";//$row_list->company_name;
            $row[] = '<div class="center">'.$row_list->qty.'</div>';
            $row[] = '<div align="right">'.number_format($row_list->price_principal).',-</div>';
            $row[] = '<div align="right">'.number_format($row_list->total_priceprincipal).',-</div>';
            $row[] = '<div align="right">'.number_format($row_list->price).',-</div>';
            $row[] = '<div align="right">'.number_format($row_list->total).',-</div>';
            $margin = $row_list->total - $row_list->total_priceprincipal;
            $row[] = '<div align="right">'.number_format($margin).',-</div>';
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_transaction_report->count_all(),
                        "recordsFiltered" => $this->m_transaction_report->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('bank_name', 'Bank Name', 'trim|required');
        $val->set_rules('bank_code', 'Bank Code', 'trim|required');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'bank_name' => $val->set_value('bank_name'),
                'is_active' => $this->input->post('is_active'),
            );

            if(isset($_FILES['icon_image']['name']) AND $_FILES['icon_image']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_transaction_report->get_by_id($id);
                    if($res_dt->icon_image != NULL){
                        if (file_exists(PATH_MBR.$res_dt->icon_image.'')) {
                            unlink(PATH_MBR.$res_dt->icon_image.'');
                        }    
                    }
                    
                }

                $dataexc['icon_image'] = $this->upload_file->doUpload('icon_image', PATH_MBR);
            }


            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*save post data*/
                $this->m_transaction_report->save($dataexc);
                $newId = $this->db->insert_id();
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*update record*/
                $this->m_transaction_report->update(array('id' => $id), $dataexc);
                $newId = $id;
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
            }
        }
    }

    public function delete()
    {
        $id=$this->input->post('ID')?$this->input->post('ID',TRUE):null;
        $toArray = explode(',',$id);
        if($id!=null){
            if($this->m_transaction_report->delete_by_id($toArray)){
                echo json_encode(array('status' => 200, 'message' => 'Proses Hapus Data Berhasil Dilakukan'));
            }else{
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Hapus Data Gagal Dilakukan'));
            }
        }else{
            echo json_encode(array('status' => 301, 'message' => 'Tidak ada item yang dipilih'));
        }
        
    }

    public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }

    public function export_excel(){

        $result = $this->m_transaction_report->get_data();

        $data = array(
            'value' => $result,
        );
        // echo '<pre>'; print_r($data);die;
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-type:   application/x-msexcel; charset=utf-8");
        header("Content-Disposition: attachment; filename=export_data_".date('d/m/Y').".xls"); 
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        $html = '';
        $html .= '<p><h3><b>Hasil Pencarian Data</b></h3></p>';
        $html .= $this->load->view('V_transaction_report/view_excel', $data, true);
        echo $html;
    }


}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
