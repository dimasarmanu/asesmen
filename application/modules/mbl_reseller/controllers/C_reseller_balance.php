<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_reseller_balance extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'mbl_reseller/C_reseller_balance');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('mbl_reseller/M_reseller_balance', 'm_reseller_balance');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show(),
        );
        //echo '<pre>';print_r($this->session->all_userdata());die;
        /*load view index*/
        $this->load->view('V_reseller_balance/index', $data);
    }

    public function show_detail( $id )
    {
        $fields = $this->m_reseller_balance->list_fields();
        $data = $this->m_reseller_balance->get_by_id( $id );
        $html = $this->m_reseller_balance->show_detail_row_table( $id );      

        echo json_encode( array('html' => $html) );
    }

    public function get_data()
    {
        /*get data from model*/
        $list = $this->m_reseller_balance->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center"><label class="pos-rel">
                        <input type="checkbox" class="ace" name="selected_id[]" value="'.$row_list->account_id.'"/>
                        <span class="lbl"></span>
                    </label></div>';
            $row[] = '';
            $row[] = $row_list->account_id;
            $row[] = '<div class="center">'.$row_list->account_id.'</div>';            
            $row[] = ucwords($row_list->user_name);
			$row[] = $row_list->company_name;
            $row[] = '<div style="text-align: right">'.number_format($row_list->amount).',-</div>';
            $row[] = '<div style="text-align: right">'.number_format($row_list->issued).',-</div>';
            $row[] = '<div style="text-align: right">'.number_format($row_list->balance).',-</div>';
                   
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_reseller_balance->count_all(),
                        "recordsFiltered" => $this->m_reseller_balance->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
	
	public function find_data()
    {   
        $output = array( "data" => http_build_query($_POST) . "\n" );
        echo json_encode($output);
    }
}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
