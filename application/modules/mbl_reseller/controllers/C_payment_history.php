<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_payment_history extends MX_Controller {

    /*function constructor*/
    function __construct() {

        parent::__construct();
        /*breadcrumb default*/
        $this->breadcrumbs->push('Index', 'mbl_reseller/C_payment_history');
        /*session redirect login if not login*/
        if($this->session->userdata('logged')!=TRUE){
            echo 'Session Expired !'; exit;
        }
        /*load model*/
        $this->load->model('mbl_reseller/M_payment_history', 'm_payment_history');
        /*enable profiler*/
        $this->output->enable_profiler(false);
        /*profile class*/
        $this->title = ($this->lib_menus->get_menu_by_class(get_class($this)))?$this->lib_menus->get_menu_by_class(get_class($this))->name : 'Title';

    }

    public function index() {
        /*define variable data*/
        $data = array(
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs->show()
        );
        /*load view index*/
        $this->load->view('V_payment_history/index', $data);
    }

    public function form($id='')
    {
        /*if id is not null then will show form edit*/
        if( $id != '' ){
            /*breadcrumbs for edit*/
            $this->breadcrumbs->push('Edit '.strtolower($this->title).'', 'mbl_reseller/C_payment_history/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
            /*get value by id*/
            $data['value'] = $this->m_payment_history->get_by_id($id);
            /*initialize flag for form*/
            $data['flag'] = "update";
        }else{
            /*breadcrumbs for create or add row*/
            $this->breadcrumbs->push('Add '.strtolower($this->title).'', 'mbl_reseller/C_payment_history/'.strtolower(get_class($this)).'/form');
            /*initialize flag for form add*/
            $data['flag'] = "create";
        }
        /*title header*/
        $data['title'] = $this->title;
        /*show breadcrumbs*/
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_payment_history/form', $data);
    }

    /*function for view data only*/
    public function show($id)
    {
        /*breadcrumbs for view*/
        $this->breadcrumbs->push('View '.strtolower($this->title).'', 'mbl_reseller/C_payment_history/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
        /*define data variabel*/
        $data['value'] = $this->m_payment_history->get_by_id($id);
        $data['title'] = $this->title;
        $data['flag'] = "read";
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*load form view*/
        $this->load->view('V_payment_history/form', $data);
    }

    public function show_detail( $id )
    {
        $fields = $this->m_payment_history->list_fields();
        $data = $this->m_payment_history->get_by_id( $id );
        $html = $this->master->show_detail_row_table( $fields, $data );      

        echo json_encode( array('html' => $html) );
    }

    public function get_data()
    {
        $template = new Template();
        /*get data from model*/
        $list = $this->m_payment_history->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_list) {
            $no++;
            $row = array();
            $row[] = '<div class="center"><label class="pos-rel">
                        <input type="checkbox" class="ace" name="selected_id[]" value="'.$row_list->id.'"/>
                        <span class="lbl"></span>
                    </label></div>';
            $row[] = '';
            $row[] = $row_list->id;
            if( !in_array($row_list->payment_status, array(1,2,3))  ){
                $btn_pembayaran = '<li><a href="#" onclick="ProsesPembayaran('.$row_list->id.',1)">Kirim Request</a></li>';
            }else{
                $btn_pembayaran = '';
            }

            if( !in_array($row_list->payment_status, array('1','2') ) ){
                $btn_batal = '<li><a href="#" onclick="ProsesPembayaran('.$row_list->id.',3)">Batalkan Request</a></li>';
            }else{
                $btn_batal = '';
            }

            $row[] = '<div class="center"><div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle">
                            <span class="ace-icon fa fa-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-inverse">
                            <li><a href="#" onclick="PopupCenter('."'mbl_reseller/C_principal_payment/print_billing/".$row_list->id."'".', '."'PRINT BILLING'".', 900, 600);">Cetak Invoice</a></li>
                            '.$btn_pembayaran.'
                            '.$btn_batal.'
                        </ul>
                      </div></div>';
            
            $row[] = '<div class="center">'.$row_list->id.'</div>';
            $row[] = $row_list->payment_number;
            $row[] = $this->tanggal->formatDate($row_list->payment_date);
            $row[] = $row_list->principal_name;
            $row[] = '<div style="text-align: right">'.number_format($row_list->total_amount).'</div>';
            $status = $template->show_status_payment(  $row_list );  
            if( $row_list->payment_status == 2 ){
                $log = json_decode($row_list->receive_payment_by);
                $txt_ket = 'Payment Request telah diterima oleh '.$log->fullname.' Tanggal '.$this->tanggal->formatDateTime($row_list->receive_payment_date).'';
            }else{
                $txt_ket = '' ;
            }

            $row[] = $status.'<br>'.$txt_ket;
            $row[] = $row_list->keterangan;
            $row[] = $this->logs->show_logs_record_datatable($row_list);
                   
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->m_payment_history->count_all(),
                        "recordsFiltered" => $this->m_payment_history->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function process()
    {
        $this->load->library('form_validation');
        $val = $this->form_validation;
        $val->set_rules('title', 'Title', 'trim|required');
        $val->set_rules('summary', 'Summary', 'trim|required');
        $val->set_rules('desc', 'Description', 'trim');
        $val->set_rules('news_type', 'News Type', 'trim');

        $val->set_message('required', "Silahkan isi field \"%s\"");

        if ($val->run() == FALSE)
        {
            $val->set_error_delimiters('<div style="color:white">', '</div>');
            echo json_encode(array('status' => 301, 'message' => validation_errors()));
        }
        else
        {                       
            $this->db->trans_begin();
            $id = ($this->input->post('id'))?$this->input->post('id'):0;

            $dataexc = array(
                'title' => $val->set_value('title'),
                'summary' => $val->set_value('summary'),
                'desc' => $val->set_value('desc'),
                'news_type' => $val->set_value('news_type'),
                'is_active' => $this->input->post('is_active'),
            );

            if(isset($_FILES['icon_image']['name']) AND $_FILES['icon_image']['name'] != ''){
                /*hapus dulu file yang lama*/
                if( $id != 0 ){
                    $res_dt = $this->m_payment_history->get_by_id($id);
                    if($res_dt->summary_image != NULL){
                        if (file_exists(PATH_MBR.'news/'.$res_dt->summary_image.'')) {
                            unlink(PATH_MBR.'news/'.$res_dt->summary_image.'');
                        }    
                    }
                    
                }

                $dataexc['summary_image'] = 'news/'.$this->upload_file->doUpload('icon_image', PATH_MBR.'news/');
            }


            if($id==0){
                $dataexc['created_date'] = date('Y-m-d H:i:s');
                $dataexc['created_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*save post data*/
                $this->m_payment_history->save($dataexc);
                $newId = $this->db->insert_id();
            }else{
                $dataexc['updated_date'] = date('Y-m-d H:i:s');
                $dataexc['updated_by'] = json_encode(array('user_id' =>$this->regex->_genRegex($this->session->userdata('user')->user_id,'RGXINT'), 'fullname' => $this->regex->_genRegex($this->session->userdata('user')->fullname,'RGXQSL')));
                /*update record*/
                $this->m_payment_history->update(array('id' => $id), $dataexc);
                $newId = $id;
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                echo json_encode(array('status' => 301, 'message' => 'Maaf Proses Gagal Dilakukan'));
            }
            else
            {
                $this->db->trans_commit();
                echo json_encode(array('status' => 200, 'message' => 'Proses Berhasil Dilakukan'));
            }
        }
    }

    

}


/* End of file Gender.php */
/* Location: ./application/modules/product_type/controllers/product_type.php */
